<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentPlatform extends Model
{
	protected $table = "plataformas";
    protected $fillable =[
    	'name','image'
    ];
}
