<?php

namespace App\Exports;



use Illuminate\Contracts\View\View;

use Maatwebsite\Excel\Concerns\FromView;
use App\producto;
use App\marca;
use App\categoria;
use App\proveedor;
use App\modelo;

 
use DB;
class ProductExportt implements FromView
{


     public function view(): View
     {
     	return view('export.producto', [
            'producto' => DB::SELECT("SELECT * FROM producto_barras")
        ]);
     }
}
