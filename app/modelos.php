<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class modelos extends Model
{
     protected $table = "modelo";

    protected $fillable =['id','nombre_modelo','id_marca'];	

    public static function model($id){
    	return modelos::where('id_marca', '=',$id)
    	->get(); 

    }
}
