<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DataTables;
use Image;
use App\eventos;
use alert;

use Session;
use Redirect;
use Illuminate\Support\Facades\Storage;

class EventosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $even = eventos::all();
        return view ('eventos.index',compact('even'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //valido si en el request se manda un archivo en el input llamado chooseFile    
       $file=$request->file('chooseFile');// chooseFile es el name que tiene mi input de tipo file
       //guardo el archivo en la carpeta imag con el nombre original del archivo
       $file->move('eventos',$file->getClientOriginalName());

        \App\eventos::create([
        'nombre'=>($request['nombre']),         
        'decripcion'=>($request['decripcion']),
        //el el campo imagen de la bd guardo el nombre original del archivo
        'img'=>$file->getClientOriginalName()
        ]);
        return redirect('evento')->with('success','Registro con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\c  $c
     * @return \Illuminate\Http\Response
     */
    public function show(eventos $eventos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\c  $c
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $even = eventos::find($id);
        return response()->json(
            $even->toArray()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\c  $c
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,eventos $eventos)
    {
        //dd($request);
        if ($file=$request->file('chooseFile')) {
        $file->move('eventos',$file->getClientOriginalName());

        $id             = $request['id'];
        $nombre         = $request['nombre'];
        $decripcion    = $request['decripcion'];
        $img            = $request['chooseFile']->getClientOriginalName();

        DB::SELECT("CALL modifica_eventos('$id','$nombre','$decripcion','$img')");
        return redirect('evento')->with('success','Se ha modificado la promoción con exito.');
        }else{
            $id             = $request['id'];
        $nombre         = $request['nombre'];
        $decripcion    = $request['decripcion'];
        $img            = $request['img'];

        DB::SELECT("CALL modifica_eventos('$id','$nombre','$decripcion','$img')");
        return redirect('evento')->with('success','Se ha modificado la promoción con exito.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\c  $c
     * @return \Illuminate\Http\Response
     */
    public function destroy(eventos $eventos)
    {
        //
    }
}
