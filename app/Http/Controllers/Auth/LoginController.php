<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Socialite;
use App\User;
use alert;

class LoginController extends Controller
{



    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    protected $maxAttempts = 3; //maximos intntos
    protected $decayMinutes  = 5; //en minutos

    

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback($provider)
    {
        $user = Socialite::driver($provider)->user();
          
        $authUser = $this->findOrCreateUser($user, $provider);
        Auth::login($authUser, true);
        return redirect($this->redirectTo);

    }
 
    public function findOrCreateUser($user, $provider)
    {
        $avatar=$user -> getAvatar();

        $authUser = User::where('email', $user->email)->first();
        // Comprobamos si el usuario ya existe
        if ($authUser) { 
            return $authUser; // Login y redirección
        } 
        
            // En caso de que no exista creamos un nuevo usuario con sus datos.
            
            return User::create([
                'name' => $user->name,
                'email' => $user->email,
                'foto' => $avatar,
             ]);
 
        }

        
}
