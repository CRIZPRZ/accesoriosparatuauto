<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\marca;
use DB;
use Auth;

class CardController extends Controller
{
    public function index()
    {
    	$user =  Auth::user();
        $check= $user->id;
        $carrito = DB::select("call contar('$check')");
        $id_user= $user->id;
        

        $marca = marca::pluck('nombre_marca','id');
        $categoria = DB::select("SELECT * FROM categorias");
        $user = DB::select("SELECT * FROM users");

        $res= $id_user;
        $carro = DB::select("call venta('$res')");
        
        $total = DB::select("call total('$res')");
        


        return view('card',compact('marca','categoria','user','carro','total','carrito'));
    }
}


  