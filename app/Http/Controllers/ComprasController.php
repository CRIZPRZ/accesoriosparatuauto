<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Alert;
use Auth;
use DB;
use App\marca;


class ComprasController extends Controller
{
    public function index()
    {
		if (Auth::check()==true) {
        $categoria = DB::select("SELECT * FROM categorias");
        $marca = marca::pluck('nombre_marca','id');
       //conteo
        $user =  Auth::user();
        $check= $user->id;
        $carrito = DB::select("call contar('$check')");
        $notifi = DB::select("CALL notificaciones('$check')");
	    $notifications = DB::select("CALL vernotificaciones('$check')");

        return view('compras.index', compact('categoria','marca','user','carrito','notifi','notifications'));
        }

        else{
	     return back()->with('warning','Debes Iniciar Sesion');
        }

    }
}
