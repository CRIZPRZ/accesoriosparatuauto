<?php

namespace App\Http\Controllers;

use App\archivo;
use App\producto;
use App\marca;
use Illuminate\Http\Request;
use DB;
use DataTables;
use Image;

use alert;

use Session;
use Redirect;

class ArchivosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function index()
    {      
        
        $produ = producto::pluck('name','id');
        $archivo = DB::select("SELECT * FROM consulta_archivos");
        
        $prod=DB::SELECT("SELECT * FROM productos");
        

        return view ('pdf.index',compact('archivo','produ','prod'));
            
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //valido si en el request se manda un archivo en el input llamado chooseFile    
        $file=$request->file('chooseFile');// chooseFile es el name que tiene mi input de tipo file
       //guardo el archivo en la carpeta imag con el nombre original del archivo
       $file->move('archivos',$file->getClientOriginalName());

        \App\archivo::create([
        
        'id_prod'=>($request['prod']),
        
        //el el campo imagen de la bd guardo el nombre original del archivo
        'nombre'=>$file->getClientOriginalName()
        ]);
        return redirect('archivo')->with('success','Registro con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\archivo  $archivo
     * @return \Illuminate\Http\Response
     */
    public function show(archivo $archivo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\archivo  $archivo
     * @return \Illuminate\Http\Response
     */
    public function edit($id,archivo $archivo)
    {
        $archivo = archivo::find($id);

        return response()->json(
            $archivo->toArray()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\archivo  $archivo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    { 
    	 if($request->hasFile('prod2')){
        $avatar = $request->file('documentos');
        $filename = time() . '.' . $avatar->getClientOriginalExtension();
        $id                     = $request['id'];
        $nombre_archivo         = $request['archivo'];
        $id_producto            = $request['prod2'];
      
       DB::SELECT("CALL modifica_archivo('$id','$nombre_archivo','$id_producto')");
       return redirect('archivo')->with('success','Registro modificado con exito');
        }else{
        $id                     = $request['id'];
        $nombre_archivo         = $request['archivo'];
        $id_producto            = $request['prod'];
      
       DB::SELECT("CALL modifica_archivo('$id','$nombre_archivo','$id_producto')");
       return redirect('archivo')->with('success','Registro modificado con exito');
        }
       
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\archivo  $archivo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        
         $archivo = archivo::findOrFail( $id );

    if ( $request->ajax() ) {
        $archivo = DB::SELECT("UPDATE documentos set documentos.activo=0 where $id=id");

        return response(['msg' => 'Producto Eliminado', 'status' => 'success']);
    }
     return redirect('archivo')->with('errors','Opps ocurrio algun error');
}
    }

