<?php

namespace App\Http\Controllers;

use App\estado;
use Illuminate\Http\Request;
use DB;
use Alert;
class EstadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {      
       
       $estado = DB::select("SELECT * FROM estado where activo=1");

        return view('estado.index',compact('estado'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       // 
         }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       \App\estado::create([
        
        'nombre_estado'=>($request['nombre']),
    
        ]);
        return redirect('estado')->with('success','Registro con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\estado  $estado
     * @return \Illuminate\Http\Response
     */
    public function show(estado $estado)
    {
          
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\estado  $estado
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $check = $id;
        $estado = DB::select("call estado('$check')");
        
        return view('estado.edit',['estado'=>$estado]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\estado  $estado
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
       $id                 = $request['id'];
       $nombre_estado      = $request['nombre'];
    
       
       DB::SELECT("CALL modifica_estados('$id','$nombre_estado')");
       return redirect('estado')->with('success','Registro Modificado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\estado  $estado
     * @return \Illuminate\Http\Response
     */
   /* public function destroy( $id, Request $request ) {
    $estado = estado::findorfail( $id );

    if ( $request->ajax() ) {
        

        return redirect('estado')->with('success','exitoso');
    }
    return redirect('estado')->with('errors','cancelado');
  }*/

  public function destroy( $id, Request $request ) {
    $estado = estado::findOrFail( $id );

    if ( $request->ajax() ) {
        $estado = DB::SELECT("UPDATE estado set estado.activo=0 where $id=id");

        return response(['msg' => 'Producto Eliminado', 'status' => 'success']);
    }
     return redirect('estado')->with('errors','Opps ocurrio algun error');
}
}

    

    

