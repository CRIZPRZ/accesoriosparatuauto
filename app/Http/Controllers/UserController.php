<?php

namespace App\Http\Controllers;

use App\marca;
use App\User;
use Illuminate\Http\Request;
use DB;
use Alert;
use Image;
use Hash;

use Auth;
class UserController extends Controller
{
    /**
     
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $user=DB::SELECT("SELECT * FROM usuarios where activo=1 AND tipo <> 4");
         $users = DB::SELECT("SELECT * FROM tipo_user WHERE id <= 3");
        
        return view('usuarios.index',compact('user','users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rol = $request->rol;
        //dd($rol);
        // crear usuario
        $user = User::create([
          'name' => ($request['nombre']),
          'email' => ($request['correo']),
          'password' => bcrypt('12345678'),
          'id_tipo' => ($request['id_tipo']),
        ]);
        $user->assignRole($rol);

        return redirect('usuarios')->with('success','Usuario registrado con exito'); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updatepas(Request $request)
    {   
       
         $request->validate([
       
        'email' => 'required|email',
        'password' => 'required|confirmed|min:8',
        'captcha' => 'required:captcha'
    ]);

       $email      = $request['email'];
     
       
       $password2 = bcrypt($request->password);
       //dd($password2);
       DB::SELECT("CALL reset_pass('$email','$password2')");
       return redirect('/')->with('success', 'Tu contraseña se ha cambiado, Puedes Iniciar Sesion');
      
}


    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function actualiza(){
        if (Auth::check()==true) {
        $marca = marca::pluck('nombre_marca','id');
        $producto = DB::select("SELECT * FROM producto_barras" );
        $categoria = DB::select("SELECT * FROM categorias");
        $user =  Auth::user();
        $check= $user->id;
        $carrito = DB::select("call contar('$check')");
        return view('perfil.actualizar', compact('categoria','marca','producto','carrito'), array('user' => Auth::user()));
        }else{
        $marca = marca::pluck('nombre_marca','id');
        $producto = DB::select("SELECT * FROM producto_barras" );
        $categoria = DB::select("SELECT * FROM categorias");
        return view('perfil.actualizar', compact('categoria','marca','producto'), array('user' => Auth::user()));   
        }
        
    }

    // metodo actualizar imagen de usuario
    public function update_avatar(Request $request){
        if($request->hasFile('avatar')){
            $avatar = $request->file('avatar');
            $filename = time() . '.' . $avatar->getClientOriginalExtension();
            Image::make($avatar)->resize(300, 300)->save( 'avatar/' . $filename  );
            $user = Auth::user();
            $user->foto = $filename;
            $user->image_facebook = "0";
            $user->save();
        }
        
        return redirect('perfiluser')->with('success','Imgen Actualizada'); 
    }



     

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function registrar(Request $request)
    {
        
        return User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
        ]);
    }


    public function reestablecer(request $request)
    {
     
        return view('auth.passwords.reestablecer',compact('request'));
    }
}
