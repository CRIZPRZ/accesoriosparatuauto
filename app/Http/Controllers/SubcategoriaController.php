<?php

namespace App\Http\Controllers;

use App\subcategoria;
use Illuminate\Http\Request;
use App\Http\Requests\SubStoreRequest;
use App\Http\Requests\SubUpdateRequest;
use alert;
use DB;
use Session;
use Redirect;

class SubcategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subcategoria=DB::SELECT("SELECT * FROM consulta_subcategoria  WHERE activo=1");
        $categoria=DB::SELECT("SELECT * FROM categorias");
        
        return view('subcat.index')
        ->with('categoria',$categoria)
        ->with('subcategoria',$subcategoria);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SubStoreRequest $request)
    {
        \App\subcategoria::create([
        
        'item'=>($request['item']),
        'nombre_sub'=>($request['nombre']),
        'descripcion_sub'=>($request['desc']),
        'id_categoria'=>($request['categoria']),
    
        ]);
        return redirect('subcat')->with('success','Registro con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\subcategoria  $subcategoria
     * @return \Illuminate\Http\Response
     */
    public function show(subcategoria $subcategoria)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\subcategoria  $subcategoria
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subcategoria = subcategoria::find($id);
        
        return response()->json(
            $subcategoria->toArray()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\subcategoria  $subcategoria
     * @return \Illuminate\Http\Response
     */
    public function update(request $request)
    {
        //dd($request);
        $id = $request['id'];
        $nombre_sub = $request['nombre'];
        $descripcion_sub = $request['desc'];
        $id_categoria = $request['cat'];
        
       
        DB::SELECT("CALL modifica_subcategorias('$id','$nombre_sub','$descripcion_sub','$id_categoria')");
        return redirect("subcat")->with('success','Registro Modificado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\subcategoria  $subcategoria
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $sub = subcategoria::findOrFail( $id );
            if ( $request->ajax() ) {
                $sub= DB::SELECT("UPDATE subcategorias set subcategorias.activo=0 where $id=id");
                return response(['msg' => 'Subcategoria Eliminada', 'status' => 'success']);
            }
                return redirect('subcat')->with('errors','Opps ocurrio algun error');
    }
}
