<?php
 
namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use DB;
use App\Services\PayPalService;
use App\Resolvers\PaymentPlatformResolver;

class PaymentController extends Controller
{
    protected $paymentPlatformResolver;

    public function __construct(PaymentPlatformResolver $paymentPlatformResolver)
    {
        $this->middleware('auth');

        $this->paymentPlatformResolver = $paymentPlatformResolver;
    }

     public function pay(Request $request)
    {
        //dd($request);
       
    	$request->validate([
            'value' => ['required', 'numeric'],
            'currency' => ['required', 'exists:moneda,iso'],
            'payment_platform' => ['required', 'exists:plataformas,id'],
        ]);
 
        $paymentPlatform = $this->paymentPlatformResolver
            ->resolveService($request->payment_platform);

        session()->put('paymentPlatformId', $request->payment_platform);
        

        return $paymentPlatform->handlePayment($request);
       
    }
 
     public function approval()
    {   
        
        if (session()->has('paymentPlatformId')) 
        {

         $paymentPlatform = $this->paymentPlatformResolver 
            ->resolveService(session()->get('paymentPlatformId'));
            
        
         return $paymentPlatform->handleApproval();
        }

          return redirect('metodospago')->with('errors','plataforma sin configurar');
    }


    public function cancelled()
    {
        return redirect('carrito')->with('errors','Tu pago se ha cancelado');
    }
}
