<?php

namespace App\Http\Controllers;

use App\marca;
use Illuminate\Http\Request;
use DB;
use Alert;

class MarcaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $marca=DB::SELECT("SELECT * FROM marca where activo=1");

        return view('marcas.index',compact('marca'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        \App\marca::create([

        'nombre_marca'=>($request['nombre']),

        ]);
        return redirect('marca')->with('success','Registro Guardado Existosamente');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\marca  $marca
     * @return \Illuminate\Http\Response
     */
    public function show(marca $marca)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\marca  $marca
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

       /* $check = $id;
        $marca = DB::select("call marca('$check')");*/

        $marca = marca::find($id);
        return response()->json(
            $marca->toArray()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\marca  $marca
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
       $id                 = $request['id'];
       $nombre_marca       = $request['nombre'];


       DB::SELECT("CALL modifica_marca('$id','$nombre_marca')");
       return redirect('marca')->with('success','Registro Modificado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\marca  $marca
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
         $producto = marca::findOrFail( $id );

        if ( $request->ajax() ) {
            $producto = DB::SELECT("UPDATE marca set marca.activo=0 where $id=id");

            return response(['msg' => 'Producto Eliminado', 'status' => 'success']);
        }
         return redirect('productos')->with('errors','Opps ocurrio algun error');
    }
}
