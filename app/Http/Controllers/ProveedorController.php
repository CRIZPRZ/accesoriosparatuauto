<?php
 
namespace App\Http\Controllers;

use App\proveedor;
use Illuminate\Http\Request;
use DB;
use alert;

class ProveedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
   public function index()
    {
        $proveedor = DB::SELECT("SELECT * FROM prov");
        $prov=DB::SELECT("SELECT CONCAT(0,MAX(id)+1) AS id FROM proveedors");
        $estado=DB::SELECT("SELECT * FROM estado");
        return view('proveedor.index')
        ->with('estado',$estado)
        ->with('prov',$prov)
        ->with('proveedor',$proveedor);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
 
          \App\proveedor::create([ 
         'id'=>($request['id']),
         'razon_social'=>($request['razon']),
         'correo'=>($request['correo']),
         'telefono'=>($request['tel']),
         'id_estado'=>($request['estado']),
         
         ]);
         return redirect('proveedor')->with('success','Registro con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\proveedor  $proveedor
     * @return \Illuminate\Http\Response
     */
    public function show(proveedor $proveedor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\proveedor  $proveedor
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $prov = proveedor::find($id);

        return response()->json(
            $prov->toArray()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\proveedor  $proveedor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, proveedor $proveedor)
    {
        
        $id                      = $request['id'];
        $razon_social            = $request['razon'];
        $correo                 = $request['correo'];
        $telefono               = $request['tel'];
        $estado               = $request['id_estado'];
 
        DB::SELECT("CALL modifica_proveedor('$id','$razon_social','$correo',
        '$telefono','$estado')");
        return redirect('proveedor')->with('success','Registro modificado con exito');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\proveedor  $proveedor
     * @return \Illuminate\Http\Response
     */
      public function destroy( $id, Request $request ) 
  {
        $estado = proveedor::findOrFail( $id );

        if ( $request->ajax() ) {
            $estado = DB::SELECT("UPDATE proveedors set proveedors.activo=0 where $id=id");

            return response(['msg' => 'Producto Eliminado', 'status' => 'success']);
        }
         return redirect('estado')->with('errors','Opps ocurrio algun error');
    }
}
