<?php 

namespace App\Http\Controllers;

use App\producto;
use App\marca;
use Illuminate\Http\Request;
use DB;
use DataTables;
use Image;
use App\modelo;
use App\proveedor;
use App\categoria;
use App\subcategoria;
use alert;

use Session;
use Redirect;


class ProductoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     */

    public function getmodel(Request $request, $id){
        if($request->ajax()){
            $modelo = modelo::modelos($id);
            return response()->json($modelo);
        }
    }

    public function getsub(Request $request, $id){
        if($request->ajax()){
            $sub = subcategoria::subcategorias($id);
            return response()->json($sub);
        }
    }

    public function index()
    {      
        $marcas=DB::SELECT("SELECT * FROM marca");
        $modelo=DB::SELECT("SELECT * FROM modelo");
        $marca = marca::pluck('nombre_marca','id');
        $cat = categoria::pluck('nombre_categoria','id');
        $cate = DB::SELECT("SELECT * FROM categorias");
        $sub = DB::select("SELECT * FROM subcategorias");
        $provs = proveedor::pluck('razon_social','id');
        $provee = DB::SELECT("SELECT * FROM proveedors");
        $producto = DB::select("SELECT * FROM producto_barras");
        

        return view ('productos.index',compact('marca','producto','cat','sub','provs','marcas','modelo','cate','provee'));
            
    }

    public function layoud()
    {   
        $marca = marca::pluck('nombre_marca','id');
        $cat = categoria::pluck('nombre_categoria','id');
        $producto = DB::select("SELECT * FROM productos");
        

        return view ('layouts.nav',compact('marca','producto','cat'));
            
    }



   /* public function consulta()
    {   
        $marca = marca::pluck('nombre_marca','id');
        $producto = DB::select("SELECT * FROM producto_barras");
        return view('productos.consulta', compact('producto','marca'));
    }*/

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       //valido si en el request se manda un archivo en el input llamado chooseFile	
       $file=$request->file('chooseFile');// chooseFile es el name que tiene mi input de tipo file
       //guardo el archivo en la carpeta imag con el nombre original del archivo
       $file->move('imag',$file->getClientOriginalName());

        \App\producto::create([
        'no_publicacion'=>($request['publicacion']),         
        'name'=>($request['nombre']),
        'cantidad'=>($request['cantidad']),
        'precio_venta'=>($request['venta']),
        'ano'=>($request['ano']), 
        'descripcion'=>($request['desc']),
        'id_categoria'=>($request['cat']),
        'id_sub'=>($request['sub']),
        'id_marca'=>($request['marca']),
        'id_modelo'=>($request['modelo']),
        'id_prov'=>($request['prov']),
        'empleado'=>($request['empleado']),
        //el el campo imagen de la bd guardo el nombre original del archivo
        'imagen'=>$file->getClientOriginalName()
        ]);
        return redirect('productos')->with('success','Registro con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function show(producto $producto)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
       $producto = producto::find($id);
        return response()->json(
            $producto->toArray()
        );
    }

    public function consultaspr(producto $producto)
    {
        $categoria = DB::select("SELECT * FROM categorias");
        dd($categoria);
        return view('productos.consultaproductos',['categoria'=>$categoria]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //dd($request);
        if ($file=$request->file('img')) {
            $file->move('imag',$file->getClientOriginalName());
            $id                      = $request['id'];
            $no_publicacion       = $request['numerop'];
            $name      = $request['nombre'];
            $cantidad = $request['cantidad'];
            $id_marca               = $request['marca'];
            $id_modelo           = $request['modelo'];
            $ano           = $request['ano'];
            $id_categoria   = $request['cate'];
            $id_sub     = $request['subca'];
            $id_prov   = $request['provee'];
            $precio_venta     = $request['venta'];
            $descripcion            = $request['descripcion'];
            $img            = $request['img']->getClientOriginalName();
            DB::SELECT("CALL modifica_producto('$id',' $no_publicacion','$name','$cantidad','$id_marca','$id_modelo','$ano',
            '$id_categoria','$id_sub','$id_prov','$precio_venta','$descripcion','$img')");
            return redirect('productos');
        }
        $id                      = $request['id'];
        $no_publicacion       = $request['numerop'];
        $name      = $request['nombre'];
        $cantidad = $request['cantidad'];
        $id_marca               = $request['marca'];
        $id_modelo           = $request['modelo'];
        $ano           = $request['ano'];
        $id_categoria   = $request['cate'];
        $id_sub     = $request['subca'];
        $id_prov   = $request['provee'];
        $precio_venta     = $request['venta'];
        $descripcion            = $request['descripcion'];
        $img            = $request['img2'];
        DB::SELECT("CALL modifica_producto('$id','$no_publicacion','$name','$cantidad','$id_marca','$id_modelo','$ano',
        '$id_categoria','$id_sub','$id_prov','$precio_venta','$descripcion','$img')");
        return redirect('productos');
      
    }
    /**

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\producto  $producto
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id, Request $request ) {
    $producto = producto::findOrFail( $id );

    if ( $request->ajax() ) {
        $producto = DB::SELECT("UPDATE productos set productos.activo=0 where $id=id");

        return response(['msg' => 'Producto Eliminado', 'status' => 'success']);
    }
     return redirect('productos')->with('errors','Opps ocurrio algun error');
}

public function filtro2(Request $request)
    {   
       
    
       $bus  = $request['marcas']; 
       $marcas=DB::SELECT("SELECT * FROM marca");
        $modelo=DB::SELECT("SELECT * FROM modelo");
        $marca = marca::pluck('nombre_marca','id');
        $cat = categoria::pluck('nombre_categoria','id');
        
        $provs = proveedor::pluck('razon_social','id');
       
       $producto = DB::select("call filtrado2('$bus')");
       

        return view ('productos.index',compact('marca','producto','cat','provs','marcas','modelo'));
    }
}
