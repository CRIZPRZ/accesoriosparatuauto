<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $rol = Auth::user()->id_tipo;
            

        switch ($rol) 
        {
            case '1':
                return redirect('productos');
            break;

            case '2':
                return redirect('productos');
            break;

            case '3':
                return redirect('productos');
            break;

            case '4':
                return redirect('/');
            break;
            
           
        }
       
    }
}
