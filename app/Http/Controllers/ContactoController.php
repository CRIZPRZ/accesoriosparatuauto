<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\categoria;
use App\marca;
use App\modelos;
use DB;
use Auth;

use alert;

class ContactoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check()==true){
            $marca = marca::pluck('nombre_marca','id');
            $producto = DB::select("SELECT * FROM producto_barras" );
            $categoria = DB::select("SELECT * FROM categorias");
             //conteo
            $user =  Auth::user();
            $check= $user->id;
            $carrito = DB::select("call contar('$check')");

            return view('contacto.mayoreo', compact('categoria','marca','producto','carrito'));
        }
        else{
            $marca = marca::pluck('nombre_marca','id');
            $producto = DB::select("SELECT * FROM producto_barras" );
            $categoria = DB::select("SELECT * FROM categorias");
            return view('contacto.mayoreo', compact('categoria','marca','producto'));
            }
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    //pruebas
    /*public function store(Request $request)
    {
        //dd($request);
        $subject = $request['subject'];
        $name = $request['name'];
        $correo = $request['correo'];
        $msg = $request['msg'];
        //dd($subject);
     
         /*Mail::send('email',$request->all(), function($msj)use($subject){

            $msj->subject($subject);
            $msj->to('al221710891@gmail.com');
         });
*/
         // return redirect('contacto')->with('success','Correo enviado con exito, espere una repuesta');
         /*return view('email',compact('subject','name','correo','msg')); 
    }*/



//el chingon
    public function store(Request $request)
    {
        
        $subject = $request['subject'];
        Mail::send('email',$request->all(), function($msj)use($subject){

            $msj->subject($subject);
            $msj->to('gerencia@accesoriosparatuauto.com');
         });

          return redirect('contacto')->with('success','Correo enviado con exito, espere una repuesta');
     
    }
 
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    
}
