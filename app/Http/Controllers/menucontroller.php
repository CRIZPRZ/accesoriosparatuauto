<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\categoria;
use App\marca;
use App\modelos;
use App\producto;
use Auth;
use Alert;


use DB;


class menucontroller extends Controller
{
  public function index()
  {

  }
    //busqueda por categorias
    public function busquedacat($request)
      {
          //dd($request);
          if (Auth::check()==true) {
              //dd($id);
              $check = $request;
              $producto = DB::select("call busqueda_cat('$check')");
              $subcategorias = DB::select("call subcat('$check')");
              //dd($subcategorias);
              $sub = DB::select("SELECT nombre_categoria FROM categorias WHERE id=$check");
              $subcat = $sub[0]->nombre_categoria;
              $marca = marca::pluck('nombre_marca','id');
              $categoria = DB::select("SELECT * FROM categorias");
              $cat = DB::select("call cat('$check')");
              //conteo
              $user =  Auth::user();
              $check= $user->id;
              $carrito = DB::select("call contar('$check')");
              $relacion = DB::select("call sub_relacionados('$subcat')");

              return view('productos.opcion2cat', compact('marca','producto','categoria','subcategorias','carrito','cat','relacion'));}
          else{
              $check = $request;
              $producto = DB::select("call busqueda_cat('$check')");
              $subcategorias = DB::select("call subcat('$check')");
              $sub = DB::select("SELECT nombre_categoria FROM categorias WHERE id=$check");
              $subcat = $sub[0]->nombre_categoria;
              //dd($sub);
              $marca = marca::pluck('nombre_marca','id');
              $categoria = DB::select("SELECT * FROM categorias");
              $cat = DB::select("call cat('$check')");

              $relacion = DB::select("call sub_relacionados('$subcat')");
              //dd($relacion);


              return view('productos.opcion2cat', compact('marca','producto','categoria','subcategorias','cat','relacion'));
          }
      }

    public function busquedasub(Request $request)
      {
          
          if (Auth::check()==true) {
              //dd($request);
              $id_sub = $request->subcat;
              
              $id_cat = $request->categoria;
              //dd($id_cat);
              $producto = DB::select("call busqueda_sub('$id_sub')");
              $subcategorias = DB::select("call subcat('$id_cat')");

              $sub = DB::select("SELECT nombre_categoria FROM categorias WHERE id=$id_cat");
              $subcat = $sub[0]->nombre_categoria;
              $marca = marca::pluck('nombre_marca','id');
              $categoria = DB::select("SELECT * FROM categorias");
              $cat = DB::select("call cat('$id_cat')");
              //conteo
              $user =  Auth::user();
              $check= $user->id;
              $carrito = DB::select("call contar('$check')");
              $relacion = DB::select("call sub_relacionados('$subcat')");

              return view('productos.buscasub', compact('marca','producto','categoria','subcategorias','carrito','cat','relacion'));}
          else{
            
              $id_sub = $request->subcat;
              
              $id_cat = $request->categoria;
              $producto = DB::select("call busqueda_sub('$id_sub')");
              $subcategorias = DB::select("call subcat('$id_cat')");

              $sub = DB::select("SELECT nombre_categoria FROM categorias WHERE id=$id_cat");
              $subcat = $sub[0]->nombre_categoria;
              $marca = marca::pluck('nombre_marca','id');
              $categoria = DB::select("SELECT * FROM categorias");
              $cat = DB::select("call cat('$id_cat')");
              //dd($sub);
              $marca = marca::pluck('nombre_marca','id');
              $categoria = DB::select("SELECT * FROM categorias");
              $cat = DB::select("call cat('$id_cat')");

              $relacion = DB::select("call sub_relacionados('$subcat')");
              //dd($relacion);


              return view('productos.buscasub', compact('marca','producto','categoria','subcategorias','cat','relacion'));
          }
      }    //metodo para filtrar datos
public function filtrado(Request $request)
{
    if (Auth::check()==true)
    {
       $marca = marca::pluck('nombre_marca','id');
       $categoria = DB::select("SELECT * FROM categorias");
       //$producto = DB::select("SELECT * FROM producto_barras");

       $ano     = $request['ano'];
       $marcas   = $request['marca'];
       $modelos = $request['modelos'];

       $producto = DB::select("call filtrado('$ano','$marcas','$modelos')");

       $user =  Auth::user();
       $check= $user->id;
       $carrito = DB::select("call contar('$check')");

       return view('productos.consulta', compact('marca','producto','categoria','carrito'));
    }
    else
    {
       $marca = marca::pluck('nombre_marca','id');
       $categoria = DB::select("SELECT * FROM categorias");
       //$producto = DB::select("SELECT * FROM producto_barras");
       $ano     = $request['ano'];
       $marcas   = $request['marca'];
       $modelos = $request['modelos'];
       $producto = DB::select("call filtrado('$ano','$marcas','$modelos')");

        return view('productos.consulta', compact('marca','producto','categoria'));
    }
}

//metodo para busqueda de datos
    public function bus(Request $request)
    {
       if (Auth::check()==true) {
       $marca = marca::pluck('nombre_marca','id');
       $categoria = DB::select("SELECT * FROM categorias");
       //$producto = DB::select("SELECT * FROM producto_barras");
       $busqueda = $request->bus;

       $resultado = DB::select("call busqueda('$busqueda')");
       $producto = $resultado;

       //conteo
        $user =  Auth::user();
        $check= $user->id;
        $carrito = DB::select("call contar('$check')");
        return view('productos.bus', compact('marca','producto','categoria','user','carrito'));
       }else {
           $marca = marca::pluck('nombre_marca','id');
           $categoria = DB::select("SELECT * FROM categorias");
           //$producto = DB::select("SELECT * FROM producto_barras");
           $busqueda = $request->bus;
           $resultado = DB::select("call busqueda('$busqueda')");

            $producto = $resultado;
           //conteo
            $user =  Auth::user();

            return view('productos.bus', compact('marca','producto','categoria','user'));
       }


    }
 
    public function getmod(Request $request, $id){
        if($request->ajax()){
            $modelos = modelos::model($id);
            return response()->json($modelos);
        }
    }


public function nav()
  {
    $marca = marca::pluck('nombre_marca','id');
    $categoria = DB::select("SELECT * FROM categorias");
    //conteo
    $user =  Auth::user();
    $check= $user->id;
    $carrito = DB::select("call contar('$check')");
    $car = $carrito[0]->car;
    $notifi = DB::select("CALL notificaciones('$check')");

    $notifications = DB::select("CALL vernotificaciones('$check')");
    return response()->json(
        $car
    );

    return view('layouts.nav', compact('categoria','marca','user','carrito','notifi','notifications'));
  }

     public function menu()
    {
      if (Auth::check()==true) {
      $categoria = DB::select("SELECT * FROM categorias");
      $marca = marca::pluck('nombre_marca','id');
     //conteo
      $user =  Auth::user();
      $check= $user->id;
      $carrito = DB::select("call contar('$check')");

      //promociones
      $promo = DB::select("SELECT * FROM promociones WHERE id='1'");
      $promo2 = DB::select("SELECT * FROM promociones WHERE id='2'");
      $promo3 = DB::select("SELECT * FROM promociones WHERE id='3'");
      $even = DB::select("SELECT * FROM eventos");
      
      $avi = DB::select("SELECT * FROM avisos");
      $notifi = DB::select("CALL notificaciones('$check')");
      
      $notifications = DB::select("CALL vernotificaciones('$check')");
      //dd($promo);
      return view('index', compact('categoria','marca','user','carrito','promo','promo2','promo3','even','avi','notifi','notifications'));
    }

        else{
          $categoria = DB::select("SELECT * FROM categorias");
          $marca = marca::pluck('nombre_marca','id');

         //conteo
          //promociones
          $promo = DB::select("SELECT * FROM promociones WHERE id='1'");
          $promo2 = DB::select("SELECT * FROM promociones WHERE id='2'");
          $promo3 = DB::select("SELECT * FROM promociones WHERE id='3'");
          $even = DB::select("SELECT * FROM eventos");
          $avi = DB::select("SELECT * FROM avisos");

          return view('index', compact('categoria','marca','promo','promo2','promo3','even','avi'));
        }

    }



 
    public function menu3()

    {
        if (Auth::check()==true)
            {
                $marca = marca::pluck('nombre_marca','id');
                $producto = DB::select("SELECT * FROM producto_barras" );
                $categoria = DB::select("SELECT * FROM categorias");
  
                $user =  Auth::user();
                $check= $user->id;
                $carrito = DB::select("call contar('$check')");
                return view('pagina.nosotros', compact('categoria','marca','producto','carrito'));
            }
            else{
                $marca = marca::pluck('nombre_marca','id');
                $producto = DB::select("SELECT * FROM producto_barras" );
                $categoria = DB::select("SELECT * FROM categorias");


                return view('pagina.nosotros', compact('categoria','marca','producto'));
            }

    }
    public function menu4()
    {
        if (Auth::check()==true) {
        $marca = marca::pluck('nombre_marca','id');
        $producto = DB::select("SELECT * FROM producto_barras" );
        $categoria = DB::select("SELECT * FROM categorias");
        $user =  Auth::user();
        $check= $user->id;
        $carrito = DB::select("call contar('$check')");
        return view('pagina.proteccion', compact('categoria','marca','producto','carrito'));
        }else{
            $marca = marca::pluck('nombre_marca','id');
        $producto = DB::select("SELECT * FROM producto_barras" );
        $categoria = DB::select("SELECT * FROM categorias");

        return view('pagina.proteccion', compact('categoria','marca','producto'));
        }


    }

    public function menu5()
    {   $marca = marca::pluck('nombre_marca','id');
        $producto = DB::select("SELECT * FROM producto_barras" );
        $categoria = DB::select("SELECT * FROM categorias");
        return view('pagina.formaspago', compact('categoria','marca','producto'));
    }
    public function menu6()
    {   $marca = marca::pluck('nombre_marca','id');
        $producto = DB::select("SELECT * FROM producto_barras" );
        $categoria = DB::select("SELECT * FROM categorias");
        return view('pagina.envios', compact('categoria','marca','producto'));
    }
    public function menu7()
    {
    if (Auth::check()==true) {
        $marca = marca::pluck('nombre_marca','id');
        $producto = DB::select("SELECT * FROM producto_barras" );
        $categoria = DB::select("SELECT * FROM categorias");
        $user =  Auth::user();
        $check= $user->id;
        $carrito = DB::select("call contar('$check')");
        return view('pagina.devoluciones', compact('categoria','marca','producto','carrito'));
    }else{
        $marca = marca::pluck('nombre_marca','id');
        $producto = DB::select("SELECT * FROM producto_barras" );
        $categoria = DB::select("SELECT * FROM categorias");

        return view('pagina.devoluciones', compact('categoria','marca','producto'));
    }

    }
    public function menu8()
    {
        if (Auth::check()==true) {
           $marca = marca::pluck('nombre_marca','id');
        $producto = DB::select("SELECT * FROM producto_barras" );
        $categoria = DB::select("SELECT * FROM categorias");
        $user =  Auth::user();
        $check= $user->id;
        $carrito = DB::select("call contar('$check')");
        return view('pagina.garantias', compact('categoria','marca','producto','carrito'));
    }else{
        $marca = marca::pluck('nombre_marca','id');
        $producto = DB::select("SELECT * FROM producto_barras" );
        $categoria = DB::select("SELECT * FROM categorias");

        return view('pagina.garantias', compact('categoria','marca','producto'));
    }

    }
     public function menu9()
    {   $marca = marca::pluck('nombre_marca','id');
        $producto = DB::select("SELECT * FROM producto_barras" );
        $categoria = DB::select("SELECT * FROM categorias");
        return view('pagina.ayuda', compact('categoria','marca','producto'));
    }
    public function menu10()
    {   $marca = marca::pluck('nombre_marca','id');
        $producto = DB::select("SELECT * FROM producto_barras" );
        $categoria = DB::select("SELECT * FROM categorias");
        return view('pagina.cuenta', compact('categoria','marca','producto'));
    }
     public function menu11()
    {   $marca = marca::pluck('nombre_marca','id');
        $producto = DB::select("SELECT * FROM producto_barras" );
        $categoria = DB::select("SELECT * FROM categorias");
        return view('pagina.cuponera', compact('categoria','marca','producto'));
    }
    public function menu12()
    {   $marca = marca::pluck('nombre_marca','id');
        $producto = DB::select("SELECT * FROM producto_barras" );
        $categoria = DB::select("SELECT * FROM categorias");
        return view('pagina.descuento', compact('categoria','marca','producto'));
    }
    public function menu13()
    {   $marca = marca::pluck('nombre_marca','id');
        $producto = DB::select("SELECT * FROM producto_barras" );
        $categoria = DB::select("SELECT * FROM categorias");
        return view('pagina.experiencia', compact('categoria','marca','producto'));
    }

    public function menu14()
    {   $marca = marca::pluck('nombre_marca','id');
        $producto = DB::select("SELECT * FROM producto_barras" );
        $categoria = DB::select("SELECT * FROM categorias");
        return view('pagina.mayoreo', compact('categoria','marca','producto'));
    }

    public function menu15()
    {   $marca = marca::pluck('nombre_marca','id');
        $producto = DB::select("SELECT * FROM producto_barras" );
        $categoria = DB::select("SELECT * FROM categorias");
        return view('pagina.fidelidad', compact('categoria','marca','producto'));
    }

    //perfil WebMater
     public function perfiluser()
    {
        if (Auth::check()==true) {
        $marca = marca::pluck('nombre_marca','id');
        $producto = DB::select("SELECT * FROM producto_barras" );
        $categoria = DB::select("SELECT * FROM categorias");

        $user =  Auth::user();
        //dd($user);
        $tipo = $user->id_tipo;
        $check= $user->id;
        $carrito = DB::select("call contar('$check')");

        $notifi = DB::select("CALL notificaciones('$check')");
        $notifications = DB::select("CALL vernotificaciones('$check')");

        $tipou = DB::SELECT("SELECT nombre FROM tipo_user WHERE id=('$tipo')");

        $foto = DB::SELECT("SELECT * FROM users where id=('$check')");

        return view('perfil', compact('categoria','marca','producto','user','carrito','notifi','notifications','tipou','foto'));

        }elseif (Auth::check()==false) {
            $marca = marca::pluck('nombre_marca','id');
        $producto = DB::select("SELECT * FROM producto_barras" );
        $categoria = DB::select("SELECT * FROM categorias");
        $user =  Auth::user();

           return view('index', compact('categoria','marca','producto','user'))->with('success','Usuario registrado con exito');
        }
}



public function relacion(Request $request)
{
    //dd($request);
    if (Auth::check()==true)
    {

    $codigo=$request->codigo;
    $pro = DB::select("SELECT * FROM producto_barras where codigo='$codigo'");
    $idprod = $pro[0]->id;
    
    $relacionados=$request->categoria;
    $marca = marca::pluck('nombre_marca','id');
    $categoria = DB::select("SELECT * FROM categorias");
    $check= $codigo;
    $producto = DB::select("call checker('$check')");
    $check2= $relacionados;
    $related = DB::select("call rela('$check2')");
    $user =  Auth::user();
    $id_user= $user->id;
    $carrito = DB::select("call contar('$id_user')");
    $pdf = DB::select("SELECT nombre FROM documentos where id='$idprod'");
   if ($pdf ==[]) {
           $pdf1 = 'false';
        }else{
            $pdf1 = $pdf[0]->nombre;
        }

        return view('productos.producto',compact('producto','marca','categoria','related','carrito','pdf1','pdf'));
    }else{

        $codigo=$request->codigo;
        $pro = DB::select("SELECT * FROM producto_barras where codigo='$codigo'");
        $idprod = $pro[0]->id;
        $relacionados=$request->categoria;
        $marca = marca::pluck('nombre_marca','id');
        $categoria = DB::select("SELECT * FROM categorias");
        $check= $codigo;
        $producto = DB::select("call checker('$check')");

        $check2= $relacionados;
        $related = DB::select("call rela('$check2')");

        $pdf = DB::select("SELECT nombre FROM documentos where id='$idprod'");

        if ($pdf ==[]) {
           $pdf1 = 'false';
        }else{
            $pdf1 = $pdf[0]->nombre;
        }



        return view('productos.producto',compact('producto','marca','categoria','related','pdf1','pdf'));
    }
}

public function search(Request $request)
{
    $posts = DB::SELECT("SELECT nombre_pro FROM producto_barras WHERE nombre_pro
        LIKE '%$request->bus%';");
    return \response()->json($posts);
}

    public function byFoundation($id){
        return  DB::select("select * from productos where id='$id'");
    }
}
