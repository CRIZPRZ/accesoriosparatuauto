<?php

namespace App\Http\Controllers;

use App\carrito;
use App\marca;
use App\venta;
use Illuminate\Http\Request;
use DB;
use Auth;
use Alert;
use PDF;

class CarritoController extends Controller
{ 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       if (Auth::check()==true) {
           //conteo
        $user =  Auth::user();
        $check= $user->id;
        $carrito = DB::select("call contar('$check')");
        $id_user= $user->id;


        $marca = marca::pluck('nombre_marca','id');
        $categoria = DB::select("SELECT * FROM categorias");
        $user = DB::select("SELECT * FROM users");

        $res= $id_user;
        $carro = DB::select("call carrito('$res')");
        //dd($carro);
        $total = DB::select("call total('$res')");



        return view('carrito',compact('marca','categoria','user','carro','total','carrito'));
        }else {
           return back()->with('warning','Debes Iniciar Sesion');
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check()==true)
        {


             if ($request->ajax())
             {

                 $result = carrito::create($request->all());

                 if ($result)
                    {
                    return response()->json(['success'=>'true']);
                    }
            }

        }else
            {
                return response()->json(['success'=>'false']);
            }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\carrito  $carrito
     * @return \Illuminate\Http\Response
     */
    public function show(carrito $carrito)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\carrito  $carrito
     * @return \Illuminate\Http\Response
     */
    public function edit(carrito $carrito)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\carrito  $carrito
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, carrito $carrito)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\carrito  $carrito
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        
        //dd($nombre_producto);
        if ($request->ajax())
             {

                  $result = DB::table('carrito')->where('id','=',$id)->delete();


                 if ($result)
                    {
                    return response()->json(['success'=>'true']);
                    }
            }

    }

    public function eliminar($nombre_producto, Request $request)
    {
        //dd($nombre_producto);
        if ($request->ajax())
             {

                  $result = DB::table('carrito')->where('nombre_producto','=',$nombre_producto)->delete();

                 if ($result)
                    {
                    return response()->json(['success'=>'true']);
                    }
            }
         
    }


    public function tarjeta(Request $request)
    {
        //dd($request);
        if (Auth::check()===true) {
           $user = Auth::user()->id;
        $n1 = $request->n1;
        $n2 = $request->n2;
        $n3 = $request->n3;
        $n4 = $request->n4;

        $tarjeta = "$n1-$n2-$n3-$n4";
        $mes = $request->mes;
        $año = $request->año;
        $titular = $request->titular;
        $total = $request->total;
        $fecha = "$mes/$año";
        $compra = $request->fcompra;
        $hora = $request->hcompra;;


        //insertar la tarjeta
        DB::SELECT("INSERT INTO tarjeta VALUES (NULL,'$tarjeta','$titular','$fecha','$user','$total',NULL,NULL)");

        //insertar en tabla venta
        DB::select("call tabla_venta('$user','$compra','$hora')");

        //eliminar en carrito
        DB::table('carrito')->where('id_user','=',$user)->delete();

        //pasamos los datos de compra en la factura;
        $total = DB::SELECT("SELECT SUM(total)AS total FROM venta WHERE id_user = '$user' AND fecha= '$compra' AND hora= '$hora'");
        $venta = DB::SELECT("SELECT * FROM venta WHERE id_user = '$user' AND fecha= '$compra' AND hora= '$hora'");

        return view('factura',compact('total','venta','request'));
        }else{
           return redirect('/')->with('warning','Debes de iniciar sesion');
        }


    }

    public function facturapdf()
    {

        $user = Auth::user()->id;
        $total = DB::SELECT("SELECT SUM(total)AS total FROM venta WHERE id_user = '$user'");
        $venta = DB::SELECT("SELECT * FROM venta WHERE id_user = '$user'");

        $pdf = PDF::loadView('factura',compact('total','venta'));
        return $pdf->stream('Factura.pdf');
    }
    //metodo para regresar los valores que se agrregaran al carrio
    public function cargacarrito($id){
        return  DB::select("call contar('$id')");
    }

    public function obtenerid($id){
        return  DB::select("call carrito('$id')");
    }

    //metodo para mostrar cantidad de los productos en carrito 
    public function totalcarrito($id){
        return  DB::select("call total('$id')");
    }

    public function cargavalores($cod){
        return  DB::select("SELECT * FROM producto_barras WHERE codigo='$cod'");
    }

    public function cargavaloresnom($nom){
        return  DB::select("SELECT * FROM producto_barras WHERE nombre_pro='$nom'");
    }

    public function cargavaloressub($id){
        return  DB::select("SELECT s.id AS id_sub,c.id AS id_cat FROM subcategorias AS s
                            INNER JOIN categorias AS c ON c.id=s.id_categoria
                            WHERE s.id='$id'");
    }
}
