<?php

namespace App\Http\Controllers;

use App\modelo;
use Illuminate\Http\Request;
use DB;
use aler;
class ModeloController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modelo=DB::SELECT("SELECT * FROM modifica_modelo  WHERE activo=1");
        $marca=DB::SELECT("SELECT * FROM marca");
        $id_modelo=DB::SELECT("SELECT MAX(id)+1 as id_modelo FROM modelo");
        return view('modelos.index')
        ->with('marca',$marca)
        ->with('id_modelo',$id_modelo)
        ->with('modelo',$modelo);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

         \App\modelo::create([
        
        'id'=>($request['idmod']),
        'nombre_modelo'=>($request['nombre']),      
        'id_marca'=>($request['marca']),
        ]);
        return redirect('modelo')->with('success','Registro con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\modelo  $modelo
     * @return \Illuminate\Http\Response
     */
    public function show(modelo $modelo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\modelo  $modelo
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
     $modelo = modelo::find($id);
        
        return response()->json(
            $modelo->toArray()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\modelo  $modelo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $nombre_modelo = $request['nombre'];
        $id_marca = $request['marca'];
        $id = $request['id'];
       
        DB::SELECT("CALL modifica_modelo('$nombre_modelo','$id_marca','$id')");
        return redirect("modelo")->with('success','Registro Modificado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\modelo  $modelo
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $producto = modelo::findOrFail($id);

        if ( $request->ajax() ) {
            $producto = DB::SELECT("UPDATE modelo set modelo.activo=0 where $id=id");

            return response(['msg' => 'Producto Eliminado', 'status' => 'success']);
        }
         return redirect('productos')->with('errors','Opps ocurrio algun error');
    }
}
