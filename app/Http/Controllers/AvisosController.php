<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DataTables;
use Image;
use App\avisos;
use alert;

use Session;
use Redirect;
use Illuminate\Support\Facades\Storage;

class AvisosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $avi = avisos::all();
        return view ('avisos.index',compact('avi'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //valido si en el request se manda un archivo en el input llamado chooseFile    
       $file=$request->file('chooseFile');// chooseFile es el name que tiene mi input de tipo file
       //guardo el archivo en la carpeta imag con el nombre original del archivo
       $file->move('avisos',$file->getClientOriginalName());

        \App\avisos::create([
        'nombre'=>($request['nombre']),         
        'descripcion'=>($request['descripcion']),
        //el el campo imagen de la bd guardo el nombre original del archivo
        'img'=>$file->getClientOriginalName()
        ]);
        return redirect('aviso')->with('success','Registro con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\c  $c
     * @return \Illuminate\Http\Response
     */
    public function show(avisos $avisos)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\c  $c
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $aviso = avisos::find($id);
        return response()->json(
            $aviso->toArray()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\c  $c
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //dd($request);
        if ($file=$request->file('chooseFile')) {
        $file->move('avisos',$file->getClientOriginalName());

        $id             = $request['id'];
        $nombre         = $request['nombre'];
        $descripcion    = $request['descripcion'];
        $img            = $request['chooseFile']->getClientOriginalName();

        DB::SELECT("CALL modifica_avisos('$id','$nombre','$descripcion','$img')");
        return redirect('aviso')->with('success','Se ha modificado el aviso con exito.');
        }else{
        $id             = $request['id'];
        $nombre         = $request['nombre'];
        $descripcion    = $request['descripcion'];
        $img            = $request['img'];

        DB::SELECT("CALL modifica_avisos('$id','$nombre','$descripcion','$img')");
        return redirect('aviso')->with('success','Se ha modificado el aviso con exito.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\c  $c
     * @return \Illuminate\Http\Response
     */
    public function destroy(avisos $avisos)
    {
        //
    }
}
