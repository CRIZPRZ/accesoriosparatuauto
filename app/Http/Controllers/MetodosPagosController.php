<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Currency;
use App\PaymentPlatform;
use DB;
use Auth;

class MetodosPagosController extends Controller
{
    public function index()
    {
    	 if (Auth::check()==true) {
    	$currencies = Currency::all();
        $paymentPlatforms = PaymentPlatform::all();
        $categoria = DB::select("SELECT * FROM categorias");
        $user =  Auth::user();
	    $check= $user->id;
	    $carrito = DB::select("call contar('$check')");
        return view('metodos de pago.metodos',compact('currencies','paymentPlatforms','categoria','carrito'));
    	}
        else{
           return back()->with('warning','Debes de iniciar sesion'); 
       }
    }
}
