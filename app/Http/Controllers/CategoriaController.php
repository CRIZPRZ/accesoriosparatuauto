<?php
 
namespace App\Http\Controllers;

use App\categoria;
use App\marca;
use Illuminate\Http\Request;
use App\Http\Requests\CatStoreRequest;
use App\Http\Requests\CatUpdateRequest;
use DB;
use alert;
use DataTables;
use Session;
use Redirect;

class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categoria=DB::SELECT("SELECT * FROM categorias where activo=1");
        return view('categorias.index', compact('categoria'));
    }

    
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CatStoreRequest $request)
    {
         \App\categoria::create([
        'item'=>($request['item']),
        'nombre_categoria'=>($request['nombre']),
        'descripcion_cat'=>($request['desc']),
    
        ]);
        return redirect('categoria')->with('success','Registro con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function show(categoria $categoria)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       /*$check = $id;
        $categoria = DB::select("call categoria('$check')");
       
        return view('categorias.edit',['categoria'=>$categoria]);*/
         $categoria = categoria::find($id);
        
        return response()->json(
            $categoria->toArray()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function update(request $request)
    {
        //dd($request);
        $id = $request['id_cat'];
        $nombre_categoria = $request['nombre'];
        $descripcion_cat = $request['desc'];
        
       
        DB::SELECT("CALL modifica_categoria('$id','$nombre_categoria','$descripcion_cat')");
        return redirect("categoria")->with('success','Registro Modificado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $cat = categoria::findOrFail( $id );
        if ( $request->ajax() ) {
            $cat = DB::SELECT("UPDATE categorias set categorias.activo=0 where $id=id");
            return response(['msg' => 'Categoria Eliminada', 'status' => 'success']);
        }
         return redirect('categoria')->with('errors','Opps ocurrio algun error');
    }
}
