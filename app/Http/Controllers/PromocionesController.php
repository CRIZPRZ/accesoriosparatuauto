<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use DataTables;
use Image;
use App\Promociones;
use alert;

use Session;
use Redirect;
use Illuminate\Support\Facades\Storage;

class PromocionesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $promo = Promociones::all();
        return view ('promociones.index',compact('promo'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Promociones  $promociones
     * @return \Illuminate\Http\Response
     */
    public function show(Promociones $promociones)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Promociones  $promociones
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $promo = Promociones::find($id);
        return response()->json(
            $promo->toArray()
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Promociones  $promociones
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //dd($request);
        if ($file=$request->file('chooseFile')) {
        $file->move('promociones',$file->getClientOriginalName());

        $id             = $request['id'];
        $nombre         = $request['nombre'];
        $img            = $request['chooseFile']->getClientOriginalName();

        DB::SELECT("CALL modifica_promo('$id','$nombre','$img')");
        return redirect('promo')->with('success','Se ha modificado la promoción con exito.');
        }else{
            $id             = $request['id'];
        $nombre         = $request['nombre'];
        $img            = $request['img'];

        DB::SELECT("CALL modifica_promo('$id','$nombre','$img')");
        return redirect('promo')->with('success','Se ha modificado la promoción con exito.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Promociones  $promociones
     * @return \Illuminate\Http\Response
     */
    public function destroy(Promociones $promociones)
    {
        //
    }
}
