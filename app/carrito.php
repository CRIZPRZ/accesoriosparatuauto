<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class carrito extends Model
{
     protected $table = "carrito";
 	protected $fillable= ['id','nombre_producto','precio_venta','descripcion','imagen','id_user'];
}
