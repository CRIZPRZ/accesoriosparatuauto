<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class marcas extends Model
{
    protected $table = "marca";

    protected $fillable =['id','nombre_marca'];
}
