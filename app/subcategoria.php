<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class subcategoria extends Model
{
    protected $table = "subcategorias";
    protected $fillable = ['id','item','nombre_sub','descripcion_sub','id_categoria'];

    public static function subcategorias($id){
    	return subcategoria::where('id_categoria', '=',$id)
    	->get();

    }
}
