<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class modelo extends Model
{
    protected $table = "modelo";

    protected $fillable =['id','nombre_modelo','id_marca'];	

    public static function modelos($id){
    	return modelo::where('id_marca', '=',$id)
    	->get();

    }

    public static function modelos2($id){
    	return modelo::where('id_marca', '=',$id)
    	->get();

    }
}
 
 