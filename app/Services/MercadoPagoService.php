<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Traits\ConsumesExternalServices;
use DB;
use Alert;


class MercadoPagoService
{
    use ConsumesExternalServices;

    protected $baseUri;

    protected $key;

    protected $secret;

    protected $baseCurrency;

    protected $converter;

    public function __construct(CurrencyConversionService $converter)
    {
        $this->baseUri = config('services.mercadopago.base_uri');
        $this->key = config('services.mercadopago.key');
        $this->secret = config('services.mercadopago.secret');
        $this->baseCurrency = config('services.mercadopago.base_currency');

        $this->converter = $converter;
    }

    public function resolveAuthorization(&$queryParams, &$formParams, &$headers)
    {
       $queryParams['access_token'] = $this->resolveAccessToken();
    }

    public function decodeResponse($response)
    {
        return json_decode($response);
    }

    public function resolveAccessToken()
    {
        return $this->secret;
    }
 
    public function handlePayment(Request $request)
    {
        //dd($request);
        date_default_timezone_set('America/Mexico_City');
        setlocale(LC_TIME, 'es_MX.UTF-8');
        $fecha_actual=date('Y-m-d');
        $hora_actual=strftime("%H:%M:%S");
        $user = $request->user;
        $request->validate([
            'card_network' => 'required',
            'card_token' => 'required',
            'email' => 'required',
        ]);

        $payment = $this->createPayment(
            
            $request->value,
            $request->currency,
            $request->card_network,
            $request->card_token,
            $request->email,
        );

        if ($payment->status === "approved") {

            $id = $payment->id;
            //dd($id);
            DB::select("call tabla_venta('$user','$fecha_actual','$hora_actual','$id')");
            DB::table('carrito')->where('id_user','=',$user)->delete();
            $name = $payment->payer->first_name;
            $currency = strtoupper($payment->currency_id);
            $amount = $payment->transaction_amount;

            
  return redirect('carrito')->with('success','Gracias ' . $name . ' Recibimos tu pago por la cantidad de ' . $amount .' ' . $currency .'.');
        }

            return redirect('carrito')->with('errors','No podemos acreditar tu pago, por favor intenta mas tarde.');
    }


    public function createPayment($value, $currency, $cardNetwork, $cardToken, $email, $installments = 1)
    {

        
       return $this->makeRequest(
            'POST',
            '/v1/payments',
            [],
            [
                'payer' => [
                    'email' => $email,
                ],
                'binary_mode' => true,
                'transaction_amount' => round($value * $this->resolveFactor($currency)),
                'payment_method_id' => $cardNetwork,
                'token' => $cardToken,
                'installments' => $installments,
                'statement_descriptor' => config('app.name'),
            ],
            [],
            $isJsonRequest = true,
        );
    }

     public function resolveFactor($currency)
    {   
        
        return $this->converter
            ->convertCurrency($currency, $this->baseCurrency);
    }
}
