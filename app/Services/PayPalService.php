<?php

namespace App\Services;

use Illuminate\Http\Request;
use App\Traits\ConsumesExternalServices;
use DB;
use Alert;
use Auth;

class PayPalService
{
	use ConsumesExternalServices;

    protected $baseUri;

    protected $clientId;

    protected $clientSecret;

    public function __construct()
    {
        $this->baseUri = config('services.paypal.base_uri');
        $this->clientId = config('services.paypal.client_id');
        $this->clientSecret = config('services.paypal.client_secret');
    }

    public function resolveAuthorization(&$queryParams, &$formParams, &$headers)
    {
    	$headers['Authorization'] = $this->resolveAccessToken();
    }

    public function decodeResponse($response)
    {
    	return json_decode($response);
    }

    public function resolveAccessToken()
    {
        $credentials = base64_encode("{$this->clientId}:{$this->clientSecret}");

        return "Basic {$credentials}";
    }
 
    public function handlePayment(Request $request)
    {
    	

        $order = $this->createOrder($request->value, $request->currency);

        $orderLinks = collect($order->links);

        $approve = $orderLinks->where('rel', 'approve')->first();

        session()->put('approvalId', $order->id);

        

        return redirect($approve->href);
    }

    public function handleApproval()
    {
        if (session()->has('approvalId')) {
            $approvalId = session()->get('approvalId');


            $payment = $this->capturePayment($approvalId);
            //
            $user =  Auth::user()->id;
           
            date_default_timezone_set('America/Mexico_City');
            setlocale(LC_TIME, 'es_MX.UTF-8');
            $fecha_actual=date('Y-m-d');
            $hora_actual=strftime("%H:%M:%S");

            //obtenemos el id de la venta
            $id_venta = DB::select("SELECT COUNT(id)+1 AS id_venta FROM venta");
                 $id = $id_venta[0]->id_venta; 
            
            DB::select("call tabla_venta('$user','$fecha_actual','$hora_actual','$id')");
            DB::table('carrito')->where('id_user','=',$user)->delete();
            $name = $payment->payer->name->given_name;
            $payment = $payment->purchase_units[0]->payments->captures[0]->amount;
            $amount = $payment->value;
            $currency = $payment->currency_code;


            return redirect('carrito')->with('success','Gracias ' . $name . ' Recibimos tu pago por la cantidad de ' . $amount .' ' . $currency .'.');
        }

            return redirect('carrito')->with('errors','No podemos capturar el prago, por favor intenta mas tarde.');
    }


    public function createOrder($value, $currency)
    {
    	return $this->makeRequest(
            'POST',
            '/v2/checkout/orders',
            [],
            [
                'intent' => 'CAPTURE',
                'purchase_units' => [
                    0 => [
                        'amount' => [
                            'currency_code' => strtoupper($currency),
                            'value' => $value,
                        ]
                    ]
                ],
                'application_context' => [
                    'brand_name' => config('app.name'),
                    'shipping_preference' => 'NO_SHIPPING',
                    'user_action' => 'PAY_NOW',
                    'return_url' => route('approval'),
                    'cancel_url' => route('cancelled'),
                ]
            ],
            [],
            $isJsonRequest = true,
        );
    }

    public function capturePayment($approvalId)
    {
        return $this->makeRequest(
            'POST',
            "/v2/checkout/orders/{$approvalId}/capture",
            [],
            [],
            [
                'Content-Type' => 'application/json',
            ],
        );
    }
}
