<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class proveedor extends Model
{
    protected $table = "proveedors";
    protected $fillable = ['id','razon_social','correo','telefono','id_estado'];
}
