<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class tarjeta extends Model
{
 	protected $table = "tarjeta";
    protected $fillable = ['id','numero','titular','fecha','id_user','total_pagado'];
}
