<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class venta extends Model
{
    protected $table = "venta";
    protected $fillable = ['id','nombre_producto','descripcion','cantidad','imagen','id_user','total'];
}
