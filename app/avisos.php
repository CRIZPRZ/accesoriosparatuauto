<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class avisos extends Model
{
    protected $table = "avisos";
 	protected $fillable= ['id','nombre','descripcion','img'];
}
