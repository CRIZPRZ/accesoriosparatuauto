<?php
 
namespace App;

use Illuminate\Database\Eloquent\Model;

class producto extends Model
{
    protected $table = "productos";
    
    protected $fillable = ['id','no_publicacion','nombre_producto','cantidad', 'precio_compra', 'precio_venta', 'ano','modelo','descripcion','codigo_barras','imagen','id_categoria','id_sub','id_marca','id_modelo','id_prov','empleado'];

}
