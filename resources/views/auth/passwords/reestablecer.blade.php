<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Reestablecer contraseña</title>

        <link  href="estilos/css/strength.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        <style>
                       

#nivelseguridad {
  font-weight: bold;
  text-transform: uppercase;
}



legend {
  text-align: center;
  margin-bottom: 10px;
}



.nivelSeguridad > p {
  margin-right: 15px;
}

.nivelSeguridad > span#nivelseguridad {}

.nivelesColores {
  height: 10px;
  width: 188px;
  display: inline-block;
  position: relative;
  left: 25px;
}

.spanNivelesColores {
  //background: #fff;
  width: 0;
  height: 10px;
  display: inline-block;
  position: absolute;
  background: url(http://momstudio.es/img/img-elmaquetadorweb/password-level-47px.png) no-repeat;
 
  /*border: 1px solid #f00;
  right: 120px;*/
}

.bajo,
.medio,
.alto,
.muyAlto {
  height: 10px;
  
}

.spanNivelesColores.nulo {
 width:0px;
}

.spanNivelesColores.bajo {
  width: 47px;
}

.spanNivelesColores.medio {
  width: 94px;
}

.spanNivelesColores.alto {
  width: 141px;
}

.spanNivelesColores.muyAlto {
  width: 188px;
}

button {
  display: block;
  margin: 20px auto 5px;
}
.clavecorrecta {
  text-align: center;
  font-weight: bold;
}
.error {
  color: #e00;
  margin-top: 15px;
}

.oculto {
  display: none;
}
        </style>

        
</head>
<body>
    @extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Reset Password') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('cambiar') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input  id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" readonly="" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <div class="input-group">
                                  <input id="password" type="password" class="check-seguridad form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password" class="check-seguridad">
                                  <div class="input-group-append">
                                        <button id="show_password" class="btn btn-primary" type="button" onclick="mostrarPassword()"> <span class="fa fa-eye-slash icon"></span> </button>
                                      </div>
                                      @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                                
                                <span id="nivelseguridad">bajo</span>
                                <div class="nivelesColores">
                                  <div class="spanNivelesColores"></div>
                                </div>

                                
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="captcha" class="col-md-4 col-form-label text-md-right">{{ __('Captcha') }}</label>

                            <div class="col-md-6">
                                <div class="captcha">
                                    <span>{!! captcha_img() !!}</span>
                                    <button type="button" class="btn btn-success refresh">Refrescar</button>
                                </div>

                                <input id="captcha" type="text" class="form-control mt-2 @error('captcha') is-invalid @enderror" name="captcha" placeholder="Ingresa el captcha">

                                @error('captcha')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Reset Password') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>





<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

<script>
    $('.refresh').click(function () {
        $.ajax({
            type: 'GET',
            url: '{{ url('/refresh_captcha') }}',
            success: function (data){
                $('.captcha span').html(data);
            }
        });
    });
</script>
<script>
$(function() {
  var pruebas = $(".pruebas"),
    nivelesColores = $("#nivelesColores"),
    
    inputs = $("input"),
    inputClaveActual = $("#password"),
    inputRepetirClaveActual = $("#repetirClaveActual"),
    fieldset = $("fieldset"),
    nivel;

  function devuelveNivel(esteInput, evento) {
    var nivelBajo = 8,
      nivelMedio = 12,
      nivelAlto = 16,
      numCaracteres = esteInput.val().length,
      estaId = esteInput.attr("id"),
      espanNivelesColores = $(".spanNivelesColores");
    evento.stopPropagation();
    limpiarError();
    if (estaId === "password") {
      if (numCaracteres > 0 && numCaracteres < nivelBajo) {
        nivel = "bajo";
                espanNivelesColores.removeClass().addClass("spanNivelesColores bajo");        
      } 
      else if (numCaracteres >= nivelBajo && numCaracteres < nivelMedio) {
        nivel = "medio";
                espanNivelesColores.removeClass().addClass("spanNivelesColores medio"); 
      } 
      else if (numCaracteres >= nivelMedio && numCaracteres < nivelAlto) {
        nivel = "alto";
                espanNivelesColores.removeClass().addClass("spanNivelesColores alto"); 
      } 
      else if (numCaracteres >= nivelAlto) {
        nivel = "muy alto";
                espanNivelesColores.removeClass().addClass("spanNivelesColores muyAlto");
      }
      if (numCaracteres === 0) {
        espanNivelesColores.removeClass().addClass("spanNivelesColores");
      }
    }
  }

  function comprobarClave(e) {
    var divClaveCorrecta = $(".clavecorrecta"),
        espanNivelesColores = $(".spanNivelesColores"),
        nivelSeguridad = $("#nivelseguridad");
    e.preventDefault();
    e.stopPropagation();
    if (inputClaveActual.val() === inputRepetirClaveActual.val()) {
      divClaveCorrecta.removeClass("oculto");
      espanNivelesColores.removeClass().addClass("spanNivelesColores nulo");
      nivelSeguridad.html("");
      return true;
    } else {
      inputClaveActual.focus();
      mostrarError();
      inputs.val("");
    }
  }
  function mostrarError() {
    var divError = $(".error"),
        espanNivelesColores = $(".spanNivelesColores"),
        nivelSeg = $("#nivelseguridad");
    divError.removeClass("oculto", 600);
    nivel = "bajo";
    nivelSeg.html(nivel);
    espanNivelesColores.removeClass().addClass("spanNivelesColores nulo");
  }
  function limpiarError() {
    var divError = $(".error");
    if(!divError.hasClass("oculto")) {
      divError.addClass("oculto");
    }
  }
  $(document).on('keyup', 'input', function(e) {
    var nivelSeg = $("#nivelseguridad");
    devuelveNivel($(this), e);
    nivelSeg.html(nivel);
  });

  boton.click(comprobarClave);

  inputs.focus(limpiarError);
});  
</script>

<script type="text/javascript">
function mostrarPassword(){
        var cambio = document.getElementById("password");
        if(cambio.type == "password"){
            cambio.type = "text";
            $('.icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
        }else{
            cambio.type = "password";
            $('.icon').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
        }
    } 
    
    $(document).ready(function () {
    //CheckBox mostrar contraseña
    $('#ShowPassword').click(function () {
        $('#Password').attr('type', $(this).is(':checked') ? 'text' : 'password');
    });
});
</script>

@endsection

</body>
</html>