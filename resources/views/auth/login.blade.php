@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                    <a class="btn btn-primary" href="{{ route('social.auth', 'facebook') }}">
                            Facebook
                        </a>
        </div>
    </div>
</div>
@endsection
