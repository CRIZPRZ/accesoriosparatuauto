c<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Modificar Categorias</title>
	@extends('layouts.MDB')

    @section('MDB')
    @endsection
</head>
<body>
	     <!--Modal para editar registros-->
 <div class="container">
      <form action="{{ route('modificarcategoria') }}" method="POST">
       {{csrf_field()}}
      @foreach ($categoria as $categorias)
      <div class="modal-body mx-3">
      	<div class="md-form mb-5">
          <i class="fas fa-cog prefix grey-text"></i>
          <input name="id_cat" readonly="" type="text" id="defaultForm-email" value="{{ $categorias->id }}" class="form-control " >
          <label >Id</label>
        </div>
        <div class="md-form mb-5">
          <i class="fas fa-cog prefix grey-text"></i>
          <input name="nombre" type="text" id="defaultForm-email" value="{{ $categorias->nombre_categoria }}" class="form-control validate" required="">
          <label data-error="wrong" data-success="right" for="defaultForm-email">Nombre de categoria</label>
        </div>
        <div class="md-form mb-5">
          <i class="fas fa-cog prefix grey-text"></i>
          <input name="desc" type="text" id="defaultForm-email" value="{{ $categorias->descripcion_cat }}" class="form-control validate" required="">
          <label data-error="error" data-success="bien" for="defaultForm-email">Descripcion</label>
        </div>
        @endforeach

    
</div>
         


      </div>
      <div class="modal-footer d-flex justify-content-center">
        <button class="btn btn-default">Modificar</button>
      </div>
    </div>
  </div>
</div>
</form>
<center>
<a href="{{url('categoria')}}"><button class="btn btn-danger">Cancelar</button></a>
</center>
</body>
</html>