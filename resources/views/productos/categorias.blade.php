<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Accesorios Para tu auto</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/css/mdb.min.css" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="16x16" href="{{{ asset('estilos/imagenes/logo accesorios-03.png')}}}">

</head>
<body>
    <!--layout de nav-->
    @extends('layouts.nav')

    @section('nav')
    <!--termina layout de nav-->


<div class="container">
        <div class="row">
            <!--Empieza espacio de categorias-->
            <div style="padding-left:1px;" class="col-xs-12 col-m-4 col-md-2">
            
                        
        @foreach($subcategorias as $subcategoria )
       <b> <a href="" style="text-decoration: none; color: #000; text">*{{$subcategoria->nombre_sub}}</a></b>

        @endforeach
                   
            </div>
            @foreach($producto as $productos)
                <!-- Grid column -->
                <div class="col-xs-12 col-m-2 col-md-2">
                    <!-- Card -->
                    <div data-toggle="tooltip" title="{{$productos -> descri}}" class="card" style="height: 290px;">
                        <div class="card-title" style="background-color: #d42c3c;" align="center">
                            <p style="padding-top: 15px;">{{$productos -> codigo}}</p>
                        </div>
                        <!-- Card image -->

                        <div class="view overlay">
                            <img class="card-img-top" src="{{ asset('imag/'.$productos->img) }}" width="150" height="110">
                            <a href="#!">
                                <div class="mask rgba-white-slight"></div>
                            </a>
                        </div>
                        <!-- Card content -->
                        <div class="card-body" align="center" style="padding-top: 35px;">
                            <!-- Text -->
                            <strong class=""></strong>
                            <p class="card-text">${{$productos -> venta}} MXN</p>
                        </div>
                        <div class="item-info" style="border: 1px solid #EAEAEA; border-top: 0;  background-color: #D7D7D7; font-size: 13px; padding-top: 5px;" align="center">
                            <p class="card-text">{{$productos -> marca}} - {{$productos -> modelo}}<br>{{$productos -> ano}}</p>
                        </div>
                    </div>
                     <br>
                      <br>
            
            
                    <!-- Card -->
                </div>

            @endforeach

                   
</div><!-- Footer --> 

<script>

</script>
    @endsection

<center>
    <div>
        <a href="{{ url('/')}}"><img src="{{{ asset('estilos/imagenes/logo1.png')}}}" style="width: 1000px; height: auto;"></a>
    </div>
</center>
</body>
</html>