<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Accesorios Para tu auto</title>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{{ asset('estilos/sweetalert2/dist/sweetalert2.min.css')}}}">
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

  <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/css/mdb.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/css/mdb.min.css" rel="stylesheet">
    <link rel="stylesheet" href="{{{ asset('estilos/css/estilos.css')}}}">

 

      <link rel="stylesheet" type="text/css" href="estilos/css/sass.css">

     @extends('layouts.MDB')

    @section('MDB')
    @endsection
    <link rel="icon" type="image/png" sizes="16x16" href="{{{ asset('estilos/imagenes/logo accesorios-03.png')}}}">
    
      <style>
        .fa-circle {
  color: #DF0000; }

.red-cart {
  color: #DF0000;
  background: white; }

  .carritos{
    position: absolute;
  right: -6%;
  top: 0%;
  
  font-size: 40%;
  padding: .6em;
  border-radius: 999px;
  line-height: .75em;
  color: white;
  color: #DF0000;
  text-align: center;
  min-width: 2em;
  font-weight: bold;
  background: white;
  border-style: solid;"
  }
      .loginface {
  background-color: #2e4a88;
  box-shadow: 0 3px 0 #1B3D82;
  text-shadow: 0 -1px -1px #1B3D82;

  display: inline;
  position: relative;
  font-family: Arial;
  font-size: 13px;
  font-weight: bold;
  text-align: center;
  text-decoration: none;
  color: #fff;
  border-radius: 15px;
  padding: 10px 50px;
}
.loginface:hover {
  background-color: #354F84;
  color: aqua;
  text-decoration: none;
}
.loginface:active {
  top: 2px;
    box-shadow: 0 2px 0 #1B3D82;
}

      @media screen and (max-width: 992px) {
 .logo {
width: 100% !important;
height: auto !important;


  }

  #div1{
    display: inline-block !important;
    align:center !important;
  }
}
@media screen and (min-width: 992px) {
 .buscado{
      display: none;
    }
  }
@media screen and (max-width: 992px) {
  .buscador2{
    display: none;

    }
    .buscado{
     
      width: 80% !important;
    }
  }
    </style>
</head>
<body>
<main>
  <center>
    <div>
        <a href="{{ url('/')}}"><img class="logo" src="{{{ asset('estilos/imagenes/logo1.png')}}}" style="width: 1000px; height: auto;"></a>
    </div>
</center>



    <!--Navbar-->

<nav class="navbar navbar-expand-lg navbar-dark danger-color">

  <!-- Navbar brand -->


  <!-- Collapse button -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="buscado">
        <table>
          <tr>
            <td>
              {{ Form::open(['url' => 'busproduc', 'method' => 'GET', 'class' => 'form-inline pull-right']) }}

                <div class="md-form my-0">
                  <input required="" name="bus" class="form-control mr-sm-2" type="text" placeholder="Buscar" aria-label="Search">
                </div>
              {{ Form::close() }}
            </td>
            <td>
              @if(Auth::check()==true)
                  <div align="right">
                    <a href="{{url('carrito')}}">
                      @foreach($carrito as $cart)
                      <span class="fa-stack fa-2x has-badge" style="font-size: 27px;">
                        <span class="carritos" id="carts">{{$cart->car}}</span>
                      @endforeach

                        <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
                      </span>
                    </a>
                  </div>
              @else
              <div align="right">


                      <span class="fa-stack fa-2x has-badge"  style="font-size: 27px;">


                        <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
                      </span>

                  </div>
              @endif
            </td>
          </tr>
        </table>
      </div>
  <!-- Collapsible content -->
  <div  class="collapse navbar-collapse" id="basicExampleNav">

    <!-- Links -->
    <ul  class="navbar-nav mr-auto">

    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

      <li class="nav-item" >
        <a class="nav-link"href="{{ url('/')}}">Inicio</a>
      </li>

       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <!-- Basic dropdown -->
<!--<li class="nav-item" data-toggle="dropdown">
  <a class="nav-link" href="#">Categorias</a>
</li>-->

<div class="dropdown-menu" style="background: #000; border-color: red; width: 100%; height: auto; opacity: 0.9;">

  @foreach($categoria as $categorias)

    <a href="{{URL::action('menucontroller@busquedacat',['id'=>$categorias -> id])}}"
      onmouseover="window.status='Guiarte, sitio de turismo y arte';return true" onmouseout="window.status='';return true">

      <i class="fas fa-angle-right" style="color: red; padding-left: 14px;" aria-hidden="true"><li style="color: #fff; text-align:justify; display: inline-block; width: 310px; padding-left: 4px; font-size: 12px; line-height: 1px; "> {{$categorias -> nombre_categoria}}</li></i>
    </a>
  @endforeach
</div>
<!-- Basic dropdown -->

    <!--<li class="nav-item">
        <a class="nav-link" href="{{ url('register') }}">Registrate</a>
      </li>-->
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <!--<li class="nav-item">
        <a class="nav-link"href="{{ url('cuponera')}}">Promociones</a>
      </li>-->
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

      <li class="nav-item">
        <a class="nav-link" href="{{ url('nosotros') }}">Nosotros</a>
      </li>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

      <li class="nav-item">
        <a class="nav-link" href="{{ url('contacto') }}">Contactanos</a>
      </li>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

      <div class="buscador2">
        {{ Form::open(['url' => 'busproduc', 'method' => 'GET', 'class' => 'form-inline pull-right']) }}
        <div class="md-form my-0">
            <input required="" name="bus" class="form-control mr-sm-2" type="text" placeholder="Buscar" aria-label="Search">
        </div>
        {{ Form::close() }}
      </div>

    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
@if(Auth::check())

@else
     <li class="nav-item dropdown">
        <div class="dropdown">
  <a class="nav-link dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Iniciar Sesión
  </a>

  <div class="dropdown-menu">
    <form class="px-4 py-3" method="POST" action="{{ route('login') }}">
      @csrf


       @error('email')
      <span class="invalid-feedback" role="alert">
          <strong style="color: red;">{{ $message }}</strong>
      </span>
      @enderror
       <!--errores en el Contraseña-->
       @error('password')
         <span class="invalid-feedback" role="alert">
            <strong style="color: red;">{{ $message }}</strong>
          </span>
       @enderror

      <div class="form-group">
        <label for="exampleDropdownFormEmail1" style="color: black;">Correo electronico</label>
        <input  type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus id="exampleDropdownFormEmail1" placeholder="email@ejemplo.com">
      </div>


      <div class="form-group">
        <label for="exampleDropdownFormPassword1" style="color: black;">Contraseña</label>
        <input required="" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" id="exampleDropdownFormPassword1" placeholder="Contraseña">
      </div>
<!--obtener la hora y fecha actual -->
<?php
date_default_timezone_set('America/Mexico_City');
setlocale(LC_TIME, 'es_MX.UTF-8');
$fecha_actual=strftime("%Y-%m-%d");
$hora_actual=strftime("%H:%M:%S");
?>

<input name="fecha" hidden="" type="text" class="form-control"  value="<?php echo $fecha_actual ;?>"  >
<input name="hora" hidden="" type="text" class="form-control" value="<?php echo $hora_actual ;?>">
<input name="ip"  hidden=""type="text" class="form-control" value="<?php echo $_SERVER['REMOTE_ADDR'] ; ?>">



       @error('password')
           <span class="invalid-feedback" role="alert">
              <strong style="color: red;">{{ $message }}</strong>
            </span>
       @enderror
         <div >
            <div >
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="remember"
                        old('remember') ? 'checked' : '' }}>

                    <label style="font-size: 12px;"  class="dropdown-item" for="remember">
                    Recuerdame
                    </label>
                </div>
            </div>
        </div>

        <div >
            <div >
            <center>
            <button type="submit" class="btn btn-primary">Iniciar Sesiòn</button>
            </center>
                <br>

            </div>
        </div>

    </form>
    <center>
        <br>
       <p>deseas iniciar sesion con facebook</p>

      <a href="{{ url('/auth/facebook')}}"  class="fab fa-facebook" style="font-size: 30px; color: #fff;"></a>

      </center>
<!-- Button trigger modal -->


     <a data-toggle="modal" data-target=".bd-example-modal-xl">¿Nuevo por aqui? <b>Registrarse</b></a>

    <a href="{{url('password/reset')}}">¿Olvidaste tu contraseña?</a>
  </div>
</div>

      </li>





<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="body">

    </div>
  </div>
</div>


@endif

<!--validar que existe inicio de sesion-->
      @if(Auth::check())
      <!--si existe mostrar nombre de ususuario logueado si no existe sesion no mostrar nada-->
 <li class="nav-item dropdown">
      <a  href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="position: relative; padding-left: 50px;">
         @if(Auth::user()->image_facebook==0)
     <img src="./avatar/{{ Auth::user()->foto }}" style="width: 32px; height: 32px; position: absolute; left: 10px; border-radius: 50%;"/>

     @endif

      @if(Auth::user()->image_facebook==1)
      <img src="{{ Auth::user()->foto }}" style="width: 32px; height: 32px; position: absolute; left: 10px; border-radius: 50%;">
       @endif


          {{ Auth::user()->name }} <span class="caret"></span>
      </a>

      <ul class="dropdown-menu" role="menu">

          <li><a href="{{ url('perfiluser')}}"><i class="fas fa-id-card" style="color: red;"></i>  Perfil</a></li>

          <li>
            <a href="{{ route('logout') }}"  onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();">
        <i class="fa fa-power-off" style="color: red;"> </i> Cerrar Sesión
      </a>
       <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
      </form>
          </li>
      </ul>
  </li>
@endif

@if(Auth::check())

@if(Auth::user()->id_tipo!=4)
       <li class="nav-item">
        <a class="nav-link" href="{{ url('productos') }}">Sistema</a>
      </li>
 @endif
@endif

    </ul>
<div class="buscador2">
@if(Auth::check()==true)
    <div align="right">
      <a href="{{url('carrito')}}">
        @foreach($carrito as $cart)
        <span class="fa-stack fa-2x has-badge" style="font-size: 27px;">
          <span class="carritos" id="carts">{{$cart->car}}</span>
        @endforeach

          <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
        </span>
      </a>
    </div>
@else
<div align="right">


        <span class="fa-stack fa-2x has-badge"  style="font-size: 27px;">


          <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
        </span>

    </div>
@endif
</div>
    <!-- Links -->


  </div>
  <!-- Collapsible content -->

</nav>

<br>
<br>
<div id="div1" style="display: inline-flex;">

<section>
    <div class="row">
        <center>
        <div class="col-md-3" id="filtrado">
           @include('productos.filtrado')
        </div>


</section>
@if($producto == [])

<div class="row">

        <center>
        <div align="
        center" class="col-md-10" >
           <h3 align="center" style="text-align: center;" class=>No contamos con este producto en nuestro stock</h3>
        </div>

<br><br>
@else
<section>

  <form hidden action="{{url('relacion')}}" method="post" name="formulario1">
    {{ csrf_field() }}
    <input  type="text" name="categoria" value="" id="cat">
    <input  type="text" name="codigo" value="" id="codigo">

  </form>

  @if(Auth::check())
  <input id="id" hidden="" type="text" readonly="" value="{{Auth::user()->id}}">
  @endif
    <center>




        @foreach($producto as $productos)
        
        <div class=" col-md-2 productos" style="display: inline-block; padding-left: 45px;">

                <!-- Grid column -->

                    <!-- Card -->

                  <a data-id="{{$productos->codigo}}" class="detalles">
                    <div data-toggle="tooltip" title="{{$productos -> descri}}" class="card " style="height: 290px; ">
                        <div class="card-title" style="background-color: #d42c3c;" align="center">
                            <p style="padding-top: 15px;">{{$productos -> codigo}}</p>

                        </div>

                        <!-- Card image -->

                    <div class="view overlay">
                            <img class="card-img-top" src="{{ asset('imag/'.$productos->img) }}" width="150" height="115" >
                  </a>
                    </div>
                        <!-- Card content -->
                        <div class="card-body" align="center" style="padding-top: 0px;">
                            <!-- Text -->
                            <strong class=""></strong>
                            <p class="card-text">${{$productos -> venta}} MXN</p>

                        </div>
                        <div  class="item-info" style="border: 1px solid #EAEAEA; border-top: 0;  background-color: #D7D7D7; font-size: 13px; padding-top: 5px; overflow:auto; height:50px;" align="center">
                            <p class="card-text">{{$productos ->nombre_pro}}<br>{{$productos -> marca}} - {{$productos -> modelo}}<br>{{$productos -> ano}}
                                <br>{{$productos -> subcat}}</p>
                        </div>


                          <div style="padding-top: 10px; padding-bottom: 10px; ">
            <!--<form action="{{ route('carrito.store') }}" method="POST" name="formulario1" id="form-id">
            {{ csrf_field() }}-->

              <button style="color: blue; border: none; background: none" class="deleteProduct"  data-id="{{$productos->codigo}}">agregar<i class="fas fa-cart-plus"></i></button>
            <!--<a href="#" onclick="addcart()" id="registro">agregar<i class="fas fa-cart-plus"></i></a>-->

            </div>
          </div>

                    <br>
                      <br>


                    <!-- Card -->




        </div>

         @endforeach

         </center>

    </div>
</section>
@endif





   </main>
   <section style="margin-bottom: -100px">
<footer class=" font-small elegant-color pt-4">

  <!-- Footer Links -->
  <div class="container-fluid text-center text-md-left">

    <!-- Grid row -->
    <div class="row">

      <!-- Grid column -->
      <div class="col-md-3 mt-md-0 mt-3">

        <!-- Content -->

        <h5 style="color: #fff;" class="text-uppercase">Terminos Y Condiciones</h5>
        <li style="color: #fff;" class="list-unstyled">
          <a download href="estilos/pdf/proteccion.pdf"><i class="fas fa-file-pdf" style="font-size: 20px; color: red;"></i></a>&nbsp;&nbsp;
          <a style="color: #fff;" href="{{ url('proteccion')}}">Proteccion de datos personales</a>

        </li>
        <!--<li style="color: #fff;">
          <a style="color: #fff;" href="{{ url('formaspago')}}">Formas de Pago</a>
        </li>
        <li style="color: #fff;">
          <a style="color: #fff;" href="{{ url('envios')}}">Envios</a>
        </li>-->
        <li style="color: #fff;" class="list-unstyled">
          <a download href="estilos/pdf/devoluciones.pdf"><i class="fas fa-file-pdf" style="font-size: 20px; color: red;"></i></a>&nbsp;&nbsp;
          <a style="color: #fff;" href="{{ url('devoluciones')}}">Devoluciones</a>

        </li>
        <li style="color: #fff;" class="list-unstyled">
          <a download href="estilos/pdf/garantias.pdf"><i class="fas fa-file-pdf" style="font-size: 20px; color: red;"></i></a>&nbsp;&nbsp;
          <a style="color: #fff;" href="{{ url('garantias')}}">Garantias</a>

        </li>


      </div>
      <!-- Grid column -->

      <hr class="clearfix w-100 d-md-none pb-3">

      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3">


        <h5 style="color: #fff;" class="text-uppercase">Clientes</h5>

        <ul class="list-unstyled">
          <li>
            <a style="color: #fff;" href="{{ url('perfiluser')}}">Mi Cuenta</a>
          </li>
          <!--<li>
            <a style="color: #fff;" href="{{ url('cuponera')}}">Promociones</a>//dar de alta promociones
          </li>-->
          <li>
            <a style="color: #fff;" href="{{ url('contacto')}}">Te interesa comprar de mayoreo</a>
          </li>
          <!--<li>
            <a style="color: #fff;" href="{{ url('fidelidad')}}">Programa de fidelidad</a>
          </li>-->
        </ul>

      </div>

      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3">

        <!-- Links -->
        <h5 style="color: #fff;" class="text-uppercase">Informacion de Sitio</h5>

        <ul style="color: #fff;" class="list-unstyled">
          <li>
            Accesorios para tu Auto.com
          </li>
          <li>
            Av. de las palmas S/n Santa Maria Atarasquillo
          </li>
          <li>
            Lerma. Estado de Mexico. C.p 52044.
          </li>
          <li>
            Tels.: 7281338693
          </li>
          <li>
            Email: <a href="mailto:contacto@accesoriosparatuauto.com">contacto@accesoriosparatuauto.com</a>
          </li>
        </ul>

      </div>
      <hr class="clearfix w-100 d-md-none pb-3">

      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3">
<center>
        <!-- Links -->
        <h5 style="color: #fff;" class="text-uppercase">Contactanos</h5>
        <a href="https://www.facebook.com/acparatuauto/" target="_blank" class="fab fa-facebook"></a>
       <!-- <a href="#" class="fab fa-youtube"></a>-->
        <a href="https://api.whatsapp.com/send?phone=527221212454&text=Hola%2C%20deseo%20cotizar%20algunas%20piezas" target="_blank" class="fab fa-whatsapp"></a>



  </center>
    </div>


      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row -->

  </div>

  <!-- Footer Links -->

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3" style="color: #9c9c9c; background: #252525 ;">© <?php echo date("Y"); ?> Copyright:
    <a href="{{ url('inicio')}}" style="color: #fff;"> Accesorios para tu auto.com</a>
  </div>
  <!-- Copyright -->


</footer>
</section>
<script>
$('.deleteProduct').on('click', function(e) {


  var cod = $(this).attr('data-id');
  $.get('cargavalores/'+cod, function(data){
//se carga el valor que regresa la consulta al elemto span con id carts
     var nombre =  data[0].nombre_pro;
     var venta =  data[0].venta;
     var desc =  data[0].descri;
     var img =  data[0].img;
     var id_user = $("#id").val();
     var token = $("input[name=_token]").val();
     var route = "{{route('carrito.store')}}";
      $.ajax({
      url:route,
      headers:{'X-CSRF-TOKEN':token},
      type:'post',
      datatype:'json',
      data:{nombre_producto: nombre,
            precio_venta: venta,
            descripcion: desc,
            imagen: img,
            id_user: id_user},
      success:function(data)
      {
        if (data.success == 'true')
        {
         Swal.fire(
          '',
          'Producto agregado a tu carrito!',
          'success'
        )
         //obtenemos el id logeado
         var id = $("#id").val();
         //vamos a la ruta y mandamos el id que espera el metodo
         $.get('cargacarrito/'+id, function(data){
    //se carga el valor que regresa la consulta al elemto span con id carts
            $("#carts").html(data[0].car);
            });
        }

        else
        {
          Swal.fire(
          'Opps!',
          'Necesitas iniciar sesión!',
          'warning')
        }
      }
    });
  });
});
</script>

   <script>
   $('.detalles').on('click', function(e)
   {
     var cod = $(this).attr('data-id');

       $.get('cargavalores/'+cod, function(data){
     //se carga el valor que regresa la consulta al elemto span con id carts
          var codigo =  $("#codigo").val(data[0].codigo);
          var categoria =  $("#cat").val(data[0].id_cat);
          document.formulario1.submit();


   });
   });

</script>
</body>
</html>
