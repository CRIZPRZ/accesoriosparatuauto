<!DOCTYPE html>
<html lang="en">
<head>
     <meta charset="UTF-8">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Accesorios Para tu auto</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/css/mdb.min.css" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="16x16" href="{{{ asset('estilos/imagenes/logo accesorios-03.png')}}}">
    <style>
      @media screen and (max-width: 992px) {
 .logo {
width: 100% !important; 
height: auto !important;

    
  }

  #div1{
    display: inline-block !important; 
    align:center !important; 
  }
}
    </style> 
</head>
<body>
<main> 
    <!--layout de nav-->
    @extends('layouts.nav')

    @section('nav')
    <!--termina layout de nav-->

<div id="div1" style="display: inline-flex;">

<section>
    <div class="row">
        <center>
        <div class="col-md-3" id="filtrado">
           @include('productos.filtrado')
        </div>

    
</section>
@if($producto == [])

<div class="row">
    
        <center>
        <div align="
        center" class="col-md-10" >
           <h3 align="center" style="text-align: center;" class=>No contamos con este producto en nuestro stock</h3>
        </div>

<br><br>
@else
<section>
    <center>
        
        
      

        @foreach($producto as $productos)
        
        <div class=" col-md-2 productos" style="display: inline-block; padding-left: 45px;">
            
                <!-- Grid column -->
                
                    <!-- Card -->
                  <a style="text-decoration: none; color: #000;" href="#" onclick="document.getElementById('form-id2').submit();" id="your-id2">
                    <div data-toggle="tooltip" title="{{$productos -> descri}}" class="card " style="height: 290px; ">

                        <div class="card-title" style="background-color: #d42c3c;" align="center">
                            
                            <p style="padding-top: 15px;">{{$productos -> codigo}}</p>

                        </div>
                        
                        <!-- Card image -->

                        <div class="view overlay">
                            <img class="card-img-top" src="{{ asset('imag/'.$productos->img) }}" width="150" height="115" >
                             </a>
                        </div>
                        <!-- Card content -->
                        <div class="card-body" align="center" style="padding-top: 0px;">
                            <!-- Text -->
                            <strong class=""></strong>
                            <p class="card-text">${{$productos -> venta}} MXN</p>

                        </div>
                        <div  class="item-info" style="border: 1px solid #EAEAEA; border-top: 0;  background-color: #D7D7D7; font-size: 13px; padding-top: 5px; overflow:auto; height:50px;" align="center">
                            <p class="card-text">{{$productos ->nombre_pro}}<br>{{$productos -> marca}} - {{$productos -> modelo}}<br>{{$productos -> ano}}
                                <br>{{$productos -> subcat}}</p>
                        </div>
                            <form action="{{url('relacion')}}" method="GET" id="form-id2">
                            {{ csrf_field() }}
                                    <input hidden="" type="text" name="categoria" value="{{$productos->id_cat}}">
                                    <input hidden="" type="text" name="codigo" value="{{$productos->codigo}}">
                                
                          </form>
                          <div style="padding-top: 10px; padding-bottom: 10px; ">
            <form action="{{ route('carrito.store') }}" method="POST" name="formulario1" id="form-id">
            {{ csrf_field() }}


            <input hidden="" type="text" readonly="" name="nombre" value="{{ $productos->nombre_pro}}">
             <input hidden=""type="text" readonly="" name="venta" value="{{ $productos->venta}}">
            <input hidden=""type="text" readonly="" name="desc" value="{{ $productos->descri}}">
            <input hidden=""type="text" readonly="" name="img" value="{{ $productos->img}}">
            

            @if(Auth::check())
            <input hidden="" type="text" readonly="" name="user" value="{{Auth::user()->id}}">
            @endif

            <a href="#" onclick="document.getElementById('form-id').submit();" id="your-id">agregar<i class="fas fa-cart-plus"></i></a>
      

            </form>
            </div>
                    </div>
                   
                    <br>
                      <br>
            
            
                    <!-- Card -->
              

           

        </div>

         @endforeach
         
         </center>
    
    </div>
</section>
@endif

    @endsection

    <center>
    <div>
        <a href="{{ url('/')}}"><img class="logo" src="{{{ asset('estilos/imagenes/logo1.png')}}}" style="width: 1000px; height: auto;"></a>
    </div>
</center>

   </main> 
   <script>
var form = document.getElementById("form-id");

document.getElementById("your-id").addEventListener("click", function () {
  form.submit();
});

var form = document.getElementById("form-id2");

document.getElementById("your-id2").addEventListener("click", function () {
  form.submit();
});
</script>
</body>
</html>