<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
<style>
  #marcas{
    width: 160px;
  }
  #modelos{
    width: 160px;
  }
   @media screen and (max-width: 992px) {
 .categorias{
  display: flex !important;
  justify-content: center !important;
  align-items: center !important;
  
 }
 .tabla{
  padding-top: -100px !important; 
 }
 .label{

  font-size: 14px !important;
 }
 .marcas{
  border-radius: 4px !important;
  height: 30px !important;
  width: 122px !important;
   
  text-decoration: none !important;
  font-family: arial; font-size: 13px !important;
  opacity:0.6 !important;

}
.select2-selection__rendered{
  border-radius: 4px !important;
  height: 30px !important;
  width: 122px !important;
   
  text-decoration: none !important;
  font-family: arial; font-size: 13px !important;
  opacity:0.6 !important;

}
}
.marcas{
  border-radius: 4px;
  height: 30px;
  text-decoration: none;
  font-family: arial; font-size: 18px; 
  opacity:0.6;

}
.select2-selection__rendered {
  border-radius: 4px;
  height: 30px;
 
  text-decoration: none;
  font-family: arial; font-size: 18px; 
  opacity:0.6;
  text-align: left;
}
</style>
<section class="categorias">
<form action="{{ url('consulta') }}" method="GET" >
      @csrf
    
<table class="tabla">
  <tr>
    <td>
      <b> <label style="color: #000; font-size: 20px;"  data-error="wrong" data-success="right" for="defaultForm-email" class="label">Marca</label></b>
    </td>
  </tr>
  <tr>
    <td>
      {!! Form::select('marca',$marca,null,['id' => 'marcas','class'=>'marcas','required' => '','placeholder'=>'Marca']) !!}
    </td>
  </tr>
  <tr>
    <td>
      <b> <label class="label" style="color: #000; font-size: 20px;"  data-error="wrong" data-success="right" for="defaultForm-email">Modelo</label></b>
    </td>
  </tr>
  <tr>
    <td>
      {!! Form::select('modelos',['placeholder'=>'Modelo'],null,['id' => 'modelos','required' => '','class'=>'modelos']) !!}
    </td>
  </tr>
  <tr>
    <td>
      <b> <label class="label" style="color: #000; font-size: 20px;"  data-error="wrong" data-success="right" for="defaultForm-email">Año</label></b>
    </td>
  </tr>
  <tr>
    <td>
      <select required style="width: 100px;" class="form-control"  name="ano" id="ano">
        <option hidden="" value="" >Año</option>
        <option value="2002">2002</option>
        <option value="2003">2003</option>
        <option value="2004">2004</option>
        <option value="2005">2005</option>
        <option value="2006">2006</option>
        <option value="2007">2007</option>
        <option value="2008">2008</option>
        <option value="2009">2009</option>
        <option value="2010">2010</option>
        <option value="2012">2012</option>
        <option value="2013">2013</option>
        <option value="2014">2014</option>
        <option value="2015">2015</option>
      </select>
    </td>
  </tr>
  <tr>
    <td>
      <center>
        <br>
        <button  type="submit" style="border-radius: 9px; border: none; margin-left: 50px; color: #fff;" class="btn-sm peach-gradient">Buscar</button>
      </center>
    </td>
  </tr>
</table>
</form>
</section>
<br>
      <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

<script type="text/javascript">


$('#modelos').select2({
  selectOnClose: true

});


$('#ano').select2({
  selectOnClose: true
});

</script>