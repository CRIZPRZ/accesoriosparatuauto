<link rel="stylesheet" type="text/css" href="{{ asset('estilos/css/detalles_product.css') }}">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html lang="en">
  <head>
    
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Detalles del Producto</title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('estilos/css/carousel_detalles.css') }}">
    


  </head>

  <body>
  	<!--layout de nav-->
    @extends('layouts.nav')

    @section('nav')
    <!--termina layout de nav-->
  	<section>
	@foreach($producto as $productos)
	<div class="container">
		<div class="card2">
			<div class="container-fliud">
				<div class="wrapper row">
					<div class="preview col-md-6">
						
						<div class="preview-pic tab-content">
						  <div class="tab-pane active" id="pic-1"><a><img style="width: 600px;" src="{{ asset('imag/'.$productos->img) }}" data-toggle="modal" data-target=".bd-example-modal-lg" / ></a></div>
						  <!--<div class="tab-pane" id="pic-2"><img src="http://placekitten.com/400/252" /></div>
						  <div class="tab-pane" id="pic-3"><img src="http://placekitten.com/400/252" /></div>
						  <div class="tab-pane" id="pic-4"><img src="http://placekitten.com/400/252" /></div>
						  <div class="tab-pane" id="pic-5"><img src="http://placekitten.com/400/252" /></div>-->
						</div>
						<!--<ul class="preview-thumbnail nav nav-tabs">
						  <li class="active"><a data-target="#pic-1" data-toggle="tab"><img src="http://placekitten.com/200/126" /></a></li>
						  <li><a data-target="#pic-2" data-toggle="tab"><img src="http://placekitten.com/200/126" /></a></li>
						  <li><a data-target="#pic-3" data-toggle="tab"><img src="http://placekitten.com/200/126" /></a></li>
						  <li><a data-target="#pic-4" data-toggle="tab"><img src="http://placekitten.com/200/126" /></a></li>
						  <li><a data-target="#pic-5" data-toggle="tab"><img src="http://placekitten.com/200/126" /></a></li>
						</ul>-->
						
					</div>
					
					<div class="details col-md-6">
						<h3 class="product-title">{{$productos -> nombre_pro}}</h3>
						<!--<div class="rating">
							<div class="stars">
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
								<span class="fa fa-star checked"></span>
							</div>
						</div>-->
						<p class="product-description">{{$productos -> descri}}</p>
						<p>{{$productos -> marca}} {{$productos -> modelo}} {{$productos -> ano}}</p>
						<p>{{$productos -> codigo}}</p>
						<h4 class="price">Precio: <span>$ {{$productos -> venta}}</span></h4>
						<div class="action">
						<form action="{{ route('carrito.store') }}" method="POST" >
            				@csrf


            <input hidden="" type="text" readonly="" name="nombre" value="{{ $productos->nombre_pro}}">
             <input hidden=""type="text" readonly="" name="venta" value="{{ $productos->venta}}">
            <input hidden=""type="text" readonly="" name="desc" value="{{ $productos->descri}}">
            <input hidden=""type="text" readonly="" name="img" value="{{ $productos->img}}">
            

            @if(Auth::check())
            <input hidden="" type="text" readonly="" name="user" value="{{Auth::user()->id}}">
            @endif
            <div form-group>
			<button class="btn peach-gradient" type="submit">Añadir al carrito</button>
			
 
			
			</div>
      
 
            </form>
            @if($pdf == [])
			@else
			<a download href="archivos/{{$pdf1}}">
			<button class="btn peach-gradient">PDF</button>
			</a>	
			@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Large modal -->

	
<div style="background-color: rgba(0,0,0,.6); " class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <img style="width: 1200px;" src="{{ asset('imag/'.$productos->img) }}" / >
    </div>
  </div>
</div>
	@endforeach
	</section>
	<br><hr>
	<section>
    <center>
    	<form action="{{url('relacion')}}" method="POST">
            @csrf
    	<div class="container-fluid">
			<div class="row">
				<div class="col-xs-11 col-md-10 col-centered">
					<div id="carousel" class="carousel slide" data-ride="carousel" data-type="multi" data-interval="2500">
						<div class="carousel-inner">
							<div class="item active">
								 @foreach($related as $relacion)
								<div class="carousel-col">
									
						    		
					            		<!-- Grid column -->
					                
					                    <!-- Card -->
					                 
					                    
            
                <!-- Grid column -->
                
                    <!-- Card -->
                  <a style="text-decoration: none; color: #000;" href="#" onclick="document.getElementById('form-id2').submit();" id="your-id2">
                    <div data-toggle="tooltip" title="{{$relacion -> descri}}" class="card animated slideInUp" style="height: 400px; ">
                        <div class="card-title" style="background-color: #d42c3c;" align="center">
                            <p style="padding-top: 15px;">{{$relacion -> codigo}}</p>

                        </div>
                        
                        <!-- Card image -->

                        <div class="view overlay">
                            <img class="card-img-top" src="{{ asset('imag/'.$relacion->img) }}" width="150" height="115" >
                             </a>
                        </div>
                        <!-- Card content -->
                        <div class="card-body" align="center" style="padding-top: 0px;">
                            <!-- Text -->
                            <strong class=""></strong>
                            <p class="card-text">${{$relacion -> venta}} MXN</p>

                        </div>
                        <div  class="item-info" style="border: 1px solid #EAEAEA; border-top: 0;  background-color: #D7D7D7; font-size: 13px; padding-top: 5px; overflow:auto; height:50px;" align="center">
                            <p class="card-text">{{$relacion -> marca}} - {{$relacion -> modelo}}<br>{{$relacion -> ano}}
                                <br>{{$relacion -> subcat}}</p>
                        </div>
                            <form action="{{url('relacion')}}" method="GET" id="form-id2">
                                @csrf
                                    <input hidden="" type="text" name="categoria" value="{{$relacion->id_cat}}">
                                    <input hidden="" type="text" name="codigo" value="{{$relacion->codigo}}">
                                
                          </form>
                          <div style="padding-top: 10px; padding-bottom: 10px; ">
            <form action="{{ route('carrito.store') }}" method="POST" name="formulario1" id="form-id">
            @csrf


            <input hidden="" type="text" readonly="" name="nombre" value="{{ $relacion->nombre_pro}}">
             <input hidden=""type="text" readonly="" name="venta" value="{{ $relacion->venta}}">
            <input hidden=""type="text" readonly="" name="desc" value="{{ $relacion->descri}}">
            <input hidden=""type="text" readonly="" name="img" value="{{ $relacion->img}}">
            

            @if(Auth::check())
            <input hidden="" type="text" readonly="" name="user" value="{{Auth::user()->id}}">
            @endif

            <a href="#" onclick="document.getElementById('form-id').submit();" id="your-id">agregar<i class="fas fa-cart-plus"></i></a>
      

            </form>
            </div>
         </div>
     </div>
					                    <!-- Card -->
								
								@endforeach
							</div>
						</div>

						<!-- Controls -->
						<div class="left carousel-control">
							<a class="carousel-control-prev" href="#carouselExample" role="button" data-slide="prev">
								<span class="carousel-control-prev-icon" aria-hidden="true"></span>
								<span class="sr-only">Previous</span>
							</a>
						</div>
						<div class="right carousel-control">
							<a class="carousel-control-next text-faded" href="#carouselExample" role="button" data-slide="next">
								<span class="carousel-control-next-icon" aria-hidden="true"></span>
								<span class="sr-only">Next</span>
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>

	    	
         </center>
    </div>
</section>



	@endsection

	<center>
	    <div>
	        <a href="{{ url('/')}}"><img class="logo" src="{{{ asset('estilos/imagenes/logo1.png')}}}" style="width: 1000px; height: auto;"></a>
	    </div>
	</center>


<script type="text/javascript">
	const activeImage = document.querySelector(".product-image .active");
	const productImages = document.querySelectorAll(".image-list img");
	const navItem = document.querySelector('a.toggle-nav');

	function changeImage(e) {
	  activeImage.src = e.target.src;
	}

	function toggleNavigation(){
	  this.nextElementSibling.classList.toggle('active');
	}

	productImages.forEach(image => image.addEventListener("click", changeImage));
	navItem.addEventListener('click', toggleNavigation);

	$('.carousel[data-type="multi"] .item').each(function() {
	var next = $(this).next();
	if (!next.length) {
		next = $(this).siblings(':first');
	}
	next.children(':first-child').clone().appendTo($(this));

	for (var i = 0; i < 2; i++) {
		next = next.next();
		if (!next.length) {
			next = $(this).siblings(':first');
		}

		next.children(':first-child').clone().appendTo($(this));
	}
});
</script>
  </body>
</html>
