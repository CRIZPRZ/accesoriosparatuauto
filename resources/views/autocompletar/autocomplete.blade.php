<!DOCTYPE html>
<html>
<head>
  

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>    
<script src="estilos/js/autocompletar.js"></script>

</head>
<body>
   

   <input required="" name="bus" id="search" name="search" type="text" class="form-control mr-sm-2 typeahead" placeholder="Search" autocomplete="off"/>
    

   
<script type="text/javascript">
    var path = "{{ route('autocomplete') }}";
    $('input.typeahead').typeahead({
        
        source:  function (query, process) {
        return $.get(path, { query: query }, function (data) {
                return process(data);
            });
        }
    });
</script>
   
</body>
</html>