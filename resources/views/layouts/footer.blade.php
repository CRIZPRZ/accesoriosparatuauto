<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">

</head>
<body>
	<!-- Footer -->
<footer style="bottom: 0; border: none;" class=" font-small elegant-color pt-4">

  <!-- Footer Links -->
  <div class="container-fluid text-center text-md-left">

    <!-- Grid row -->
    <div class="row">

      <!-- Grid column -->
      <div class="col-md-3 mt-md-0 mt-3">

        <!-- Content -->

        <h5 style="color: #fff;" class="text-uppercase">Terminos Y Condiciones</h5>
        <li style="color: #fff;">
          <a style="color: #fff;" href="#!">Protección de datos personales</a>
        </li>
        <li style="color: #fff;">
          <a style="color: #fff;" href="#!">Formas de Pago</a>
        </li>
        <li style="color: #fff;">
          <a style="color: #fff;" href="#!">Envios</a>
        </li>
        <li style="color: #fff;">
          <a style="color: #fff;" href="#!">Devoluciones</a>
        </li>
        <li style="color: #fff;">
          <a style="color: #fff;" href="#!">Garantias</a>
        </li>
        <li style="color: #fff;">
          <a style="color: #fff;" href="#!">Ayuda</a>
        </li>

      </div>
      <!-- Grid column -->

      <hr class="clearfix w-100 d-md-none pb-3">

      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3">

        <!-- Links -->
        <h5 style="color: #fff;" class="text-uppercase">Clientes</h5>

        <ul class="list-unstyled">
          <li>
            <a style="color: #fff;" href="#!">Mi Cuenta</a>
          </li>
          <li>
            <a style="color: #fff;" href="#!">Cuponera</a>
          </li>
          <li>
            <a style="color: #fff;" href="#!">Descuento por tu compleaños</a>
          </li>
          <li>
            <a style="color: #fff;" href="#!">Comparte tu experiencia</a>
          </li><li>
            <a style="color: #fff;" href="#!">¿Te interesa comprar por mayoreo?</a>
          </li><li>
            <a style="color: #fff;" href="#!">Programa de fidelidad</a>
          </li>
        </ul>

      </div>

      <!-- Grid column -->

      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3">

        <!-- Links -->
        <h5 style="color: #fff;" class="text-uppercase">Información de Sitio</h5>

        <ul style="color: #fff;" class="list-unstyled">
          <li>
            <a>Accesorios para tu Auto.com</a>
          </li>
          <li>
            <a>Av. de las palmas S/n Santa Maria Atarasquillo</a>
          </li>
          <li>
            <a>Lerma. Estado de Mexico. C.p 52044.</a>
          </li>
          <li>
            <a>Tels: 7281338693</a>
          </li>
          <li>
            <a>Email: gedent_24@hotmail.com</a>
          </li>
        </ul>

      </div>
      <hr class="clearfix w-100 d-md-none pb-3">

      <!-- Grid column -->
      <div class="col-md-3 mb-md-0 mb-3">
<center>
        <!-- Links -->
        <h5 style="color: #fff;" class="text-uppercase">Contáctanos</h5>
        <a href="#" class="fab fa-facebook"></a>
        <a href="#" class="fab fa-twitter"></a>
        <a href="#" class="fab fa-youtube"></a>
        <a href="https://api.whatsapp.com/send?phone=527221212454&text=Hola%2C%20deseo%20cotizar%20algunas%20piezas" target="_blank" class="fab fa-whatsapp"></a>



  </center>
    </div>


      </div>
      <!-- Grid column -->

    </div>
    <!-- Grid row -->

  </div>

  <!-- Footer Links -->

  <!-- Copyright -->
  <div class="footer-copyright text-center py-3" style="color: #9c9c9c; background: #252525 ;">© <?php echo date("Y"); ?> Copyright:
    <a href="{{ url('inicio')}}" style="color: #fff;"> Accesorios para tu auto.com</a>
  </div>
  <!-- Copyright -->


</footer>
<!-- Footer -->
</body>
</html>
