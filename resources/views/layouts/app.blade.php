<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="estilos/css/menulateral.css"> 
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
 
    <title>Accesorios para tu auto</title>


    <!-- Scripts -->
    

    <!-- Fonts -->

    <!-- Styles -->
    <link href="{{ asset('estilos/css/app.css') }}" rel="stylesheet">
    
    
    
</head>
<body>

        <main class="py-4">
            @yield('content')
        </main>
    </div>

<script src="{{{ asset('estilos/js/menu.js')}}}"></script>
@stack('scripts')
</body>
</html>
