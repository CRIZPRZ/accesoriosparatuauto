<!doctype html>
<html class="no-js" lang="es-MX" data-site="MLM"
      data-country="MX"
      data-device="desktop">

<head prefix="">
	<title>Carrito</title>
	<meta charset="utf-8"/>

		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
		<link rel="stylesheet" type="text/css" href="https://http2.mlstatic.com/resources/frontend/cart-core_ui/ui-dist/styles/cart-core_ui__large-08e223adb0.css" />

		<link rel="stylesheet" type="text/css" href="https://http2.mlstatic.com/resources/frontend/web-cart/ui-dist/styles/cart__large-4db3053f21.css" />
    <link rel="stylesheet" href="{{{ asset('estilos/css/estilos.css')}}}">
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>

        <link rel="stylesheet" href="{{{ asset('estilos/sweetalert2/dist/sweetalert2.min.css')}}}">
        @extends('layouts.MDB')

    @section('MDB')
    @endsection
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
        <script src="{{{ asset('estilos/sweetalert2/dist/sweetalert2.all.min.js')}}}"></script>
        <script src="{{{ asset('estilos/sweetalert2/dist/sweetalert2.min.js')}}}"></script>


<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

</head>

<body>
<div id="pagina"></div>


</body>

</html>
