<?php
date_default_timezone_set('America/Mexico_City');
setlocale(LC_TIME, 'es_MX.UTF-8');
$fecha_actual=strftime("%d-%m-%y");
$hora_actual=strftime("%H:%M:%S");
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title> Factura</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <link rel="stylesheet" href="{{ asset('/estilos/css/factura.css')}}">
 
    </head>
  <body>
   <center>
    <div>
        <img src="{{ asset('estilos/imagenes/logo1.png') }}" style="width: 700px; height: auto;">
    </div>
</center>
<br>
    <header>
      <h1 style="background: red;">Factura</h1>

    
    </header>
    <article>
      <h1>Recipient</h1>
   <br>
   <br>
   <br>
   <br>
   <br>
   <br>
   
      <table class="meta">
        <tr>
          <span>N. Tarjeta</span>:
         <span>**************{{$request->n4}}</span>
        </tr>
        <tr>
          <th><span>Factura #</span></th>
          
        <td><span>00001</span></td>
          
          
        </tr>
        <tr>
          <th><span>Fecha</span></th>
          <td><span><?php echo $fecha_actual ;?></span></td>
        </tr>
       
        
        <tr>
          <th><span>Total Pagado</span></th>
          @foreach($total as $tot)
          <td><span>${{$tot->total}}.00</span></td>
          @endforeach
          
          
          
        </tr>
      </table>
      <table class="inventory">
        <thead>
          <tr>
            <th><span>imagen</span></th>
            <th><span>Producto</span></th>
            <th><span>Description</span></th>
            <th><span>Precio unitario</span></th>
            <th><span>Cantidad</span></th>
            <th><span>Total</span></th>
          </tr>
        </thead>
        <tbody>
          @foreach($venta as $ventas)
          <tr>
            
            <td><span><img style="width:85px; height:85px; " src="{{ asset('imag/'.$ventas->imagen) }}"></span></td>
            <td><span>{{$ventas->nombre_producto}}</span></td>
            <td><span>{{$ventas->descripcion}}</span></td>
            <td><span>$</span><span>{{$ventas->precio_venta}}.00</span></td>
            <td><span>{{$ventas->cantidad}}</span></td>
            <td><span>$</span><span>{{$ventas->total}}.00</span></td>
            
          </tr>
          @endforeach
        </tbody>
      </table>
            <table class="balance">
        <tr>
          <th><span>Total</span></th>
          @foreach($total as $tot)
          <td><span>${{$tot->total}}.00</span></td>
          @endforeach
        </tr>
        <!--<tr>
          <th><span>Amount Paid</span></th>
          <td><span>$</span><span>0.00</span></td>
        </tr>
        <tr>
          <th><span>Balance Due</span></th>
          <td><span>$</span><span>600.00</span></td>
        </tr>-->
      </table>
    </article>
    <script type="text/javascript">

     
 window.onload = calculos();
function calculos() {
  var valor = document.getElementById("total").value;
console.log(valor);


}
    </script>
  </body>
</html> 



