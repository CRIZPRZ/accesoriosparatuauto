<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Accesorios Para tu auto</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="estilos/css/inicio.css">
        <link rel="stylesheet" type="text/css" href="https://http2.mlstatic.com/resources/frontend/cart-core_ui/ui-dist/styles/cart-core_ui__large-08e223adb0.css" />

    <!-- Font Awesome -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/css/mdb.min.css" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="16x16" href="estilos/imagenes/logo accesorios-03.png">
<style>
  
</style>
</head>
<body style="background-repeat: no-repeat;">
    <!--layout de nav-->
    @extends('layouts.nav')

    @section('nav')
    <!--termina layout de nav-->
    <!--Carousel Wrapper-->
<div>
    <div class=" row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Total a Pagar: $<span id="total"></span></div>

                <div class="card-body">
                    <form action="{{ route('pay') }}" method="POST" id="paymentForm">
                        @csrf

                        <div>
                            <input hidden="" id="tot" type="text" name="value" >
                            <input hidden="" type="text" name="currency" value="mxn">
                            <input hidden="" type="text" readonly="" name="user" value="{{Auth::user()->id}}" id="user">

                        </div>
                       <center>
                            <div class="col">
                                <label>Select the desired payment platform:</label>
                                <div class="form-group" id="toggler">
                                    <div class="btn-group btn-group-toggle" data-toggle="buttons">
                                        @foreach ($paymentPlatforms as $paymentPlatform)
                                            <label
                                                class="btn btn-outline-danger rounded m-2 p-1"
                                                data-target="#{{ $paymentPlatform->name }}Collapse"
                                                data-toggle="collapse"
                                            >
                                                <input
                                                    type="radio"
                                                    name="payment_platform"
                                                    value="{{ $paymentPlatform->id }}"
                                                    required
                                                >
                                                <img class="img-thumbnail" src="{{ asset($paymentPlatform->image) }}">
                                            </label>
                                        @endforeach
                                    </div>
                                    @foreach ($paymentPlatforms as $paymentPlatform)
                                        <div
                                            id="{{ $paymentPlatform->name }}Collapse"
                                            class="collapse"
                                            data-parent="#toggler"
                                        >
                                            @includeIf('components.' . strtolower($paymentPlatform->name) . '-collapse')
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                       </center>
                        <div class="text-center mt-3">
                            <button type="submit" id="payButton" class="btn btn-primary btn-sm">Pagar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
    @endsection

<center>
    <div>
        <a href="{{ url('inicio')}}"><img src="estilos/imagenes/logo1.png" style="width: 1000px; height: auto;"></a>
    </div>
</center>

<script src="{{{ asset('estilos/sweetalert/sweetalert.min.js')}}}"></script>
@include('sweet::alert')
 <script>
     $( document ).ready(function() {
        var id = $("#user").val();
        $.get('totalcarrito/'+id, function(data){
    //se carga el valor que regresa la consulta al elemto span con id el id correspondiente
            $("#total").html(data[0].total);
            $("#tot").val(data[0].total);
            
            

            });
});
 </script>
</body>
</html>