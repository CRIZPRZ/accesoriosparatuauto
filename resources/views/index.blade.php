<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Accesorios Para tu auto</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="estilos/css/inicio.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <!-- Bootstrap core CSS -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/css/mdb.min.css" rel="stylesheet">
    <link rel="icon" type="image/png" sizes="16x16" href="estilos/imagenes/logo accesorios-03.png">
<style>
  
</style>
</head>
<body style="background-repeat: no-repeat;">
    <!--layout de nav-->
    @extends('layouts.nav')

    @section('nav')
    <!--termina layout de nav-->
    <!--Carousel Wrapper-->
    <div class="row">
       <div style="padding-left: 40px;" class="col-xs-12 col-m-2 col-md-2">
            @include('productos.filtrado')
        </div>
        
        <div class="col-xs-12 col-m-10 col-md-9 container">
            <div id="carousel-example-1z" class="carousel slide carousel-fade" data-ride="carousel">
              <!--Indicators-->
              <ol class="carousel-indicators">
                <li data-target="#carousel-example-1z" data-slide-to="0" class="active"></li>
                <li data-target="#carousel-example-1z" data-slide-to="1"></li>
                <li data-target="#carousel-example-1z" data-slide-to="2"></li>
              </ol>
              <!--/.Indicators-->
              <!--Slides-->
              <div class="carousel-inner" role="listbox">
                <!--First slide-->
                @foreach($promo as $promos)
                <div class="carousel-item active">
                  <img class="d-block w-100"  src="{{ asset('promociones/'.$promos->img) }}"
                    alt="First slide">                    
                </div>
                @endforeach
                <!--/First slide-->
                <!--Second slide-->
                @foreach($promo2 as $promos2)
                <div class="carousel-item">
                  <img class="d-block w-100" src="{{ asset('promociones/'.$promos2->img) }}"
                    alt="Second slide">
                </div> 
                @endforeach
                <!--/Second slide-->
                <!--Third slide-->
                @foreach($promo3 as $promos3)
                <div class="carousel-item">
                  <img class="d-block w-100" src="{{ asset('promociones/'.$promos3->img) }}"
                    alt="Third slide">
                </div>
                @endforeach
                <!--/Third slide-->
              </div>
              <!--/.Slides-->
              <!--Controls-->
              <a class="carousel-control-prev" href="#carousel-example-1z" role="button" data-slide="prev">
                <span style="color: #000;" class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span style="color: #000;" class="sr-only">Previous</span>
              </a>
              <a class="carousel-control-next" href="#carousel-example-1z" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
              </a>
              <!--/.Controls-->
            </div>
            <!--/.Carousel Wrapper-->
        </div>
    </div>
    <br>
    <br>
    <hr>
    
    <div class="row">
      @if($even ==[])
      @else
      <div class="col-xs-12 col-m-9 col-md-9" align="center">
        @foreach($even as $event)
        <h3 style="background-color: red; color: white;">{{$event->nombre}}</h3>
        <p>{{$event->decripcion}}</p>
        <img src="{{ asset('eventos/'.$event->img) }}" style="padding-bottom: 10px;">
        @endforeach
      </div>
       @endif
        @if($avi ==[])
        @else
      <div class="col-xs-12 col-m-3 col-md-3 container" align="center">
        @foreach($avi as $avis)
        <h3 style="background-color: red; color: white;">{{$avis->nombre}}</h3>
        <img src="{{ asset('avisos/'.$avis->img) }}" style="padding-right: 15px; border-radius: 10px; padding-bottom: 10px;">
        @endforeach
      </div>
       @endif
    </div>
   
    <br>
    <br>
    
    @endsection

<center>
    <div>
        <a href="{{ url('inicio')}}"><img src="estilos/imagenes/logo1.png" style="width: 1000px; height: auto;"></a>
    </div>
</center>
<script src="{{{ asset('estilos/sweetalert/sweetalert.min.js')}}}"></script>
@include('sweet::alert')
</body>
</html>