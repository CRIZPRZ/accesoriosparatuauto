
<!DOCTYPE html>
<html>
<head>
@extends('layouts.app')

@section('content')
@endsection
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="estilos/css/perfil.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">

    										
    <style type="text/css">
#chistes{
 position: relative;
}
 
}
.subir{
    padding: 5px 10px;
    background: #f55d3e;
    color:#fff;
    border:0px solid #fff;
}
 
.subir:hover{
    color:#fff;
    background: #f7cb15;
}

</style>
</head>
<body >
  @extends('layouts.nav')

    @section('nav')


  <div class="cabecera">

     <section style=" position: relative; top: 180px;" >
      <center>
       <figure class="snip1566">
         @if(Auth::user()->image_facebook==0)
     <img src="./avatar/{{ $user->foto }}" class="img-responsive img-rounded" alt="User picture"/>
       
     @endif

      @if(Auth::user()->image_facebook==1)
      <img class="img-responsive img-rounded" src="{{ Auth::user()->foto }}"
            alt="User picture">
       @endif
       
        
       </figure>
      </center>
     </section>

      
   </div>
   <section style=" position: relative; top: 80px;">
<center> <h2>{{ Auth::user()->name }}</h2></center>
<center> <h3>{{ Auth::user()->id_tipo }}</h3></center>
<center> <h5>Accesorios para tu auto.com</h5></center>
 <form  action="{{ url('actualizafoto') }}" method="POST" enctype="multipart/form-data">
      @csrf
           <center>
            <label style="font-size: 20px; color:#FFf;">Actualizar tu Foto de Perfil</label>
            <input  required="" type="file" name="avatar"  class="subir" class="btn btn-success">
            <input type="text" name="id" value="{{ Auth::user()->id }}" hidden>
          </center>
                
   
    <br>
    <center>
      <input type="submit"class=" btn btn-sm btn-primary"></center>

 </form>
</section>
<br>
<br>
<br>
<br>

  @endsection
  <center>
 <div>
        <a href="{{ url('inicio')}}"><img src="estilos/imagenes/logo1.png" style="width: 1000px; height: auto;"></a>
    </div>
</center>
<script src="estilos/sweetalert/sweetalert.min.js"></script>
@include('sweet::alert')

</body>
</html>


