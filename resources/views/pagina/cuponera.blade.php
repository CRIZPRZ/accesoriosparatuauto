<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Accesorios Para tu auto</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/inicio.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="icon" type="image/png" sizes="16x16" href="estilos/imagenes/logo accesorios-03.png">

</head>
<body>
    <!--layout de nav-->
    @extends('layouts.nav')

    @section('nav')
    <!--termina layout de nav-->
    <div class="container" style="width: 100%; height: auto;">
        <div class="row" >
            <div class="col-xs-12 col-m-10 col-md-12" align="justify">
                <h3 align="center">Cuponera</h3>
                <br>
                <p>Lanzo la vara frente a mí. La vara y un arcoíris prismático de luz estelar roza las coletas de Jinx antes de que regrese a mi mano. Una esfera de luz multicolor cubre a Jinx y a Poppy. Unas cuantas burbujas rebotan en la barrera y se revientan contra los columpios, dejando una espiral de niebla oscura, con negras formas ondeantes (¿son insectos, tal vez, o polillas?) y una risa larga y aguda, como la carcajada contenta de un niño.<br><br>

                ''Eso no está bien, ¿cierto?'', grita Jinx, pensando que susurra. ''¡Acabemos con estas burbujas rebeldes!''.<br><br>

                ''Justo lo que estaba pensando''. Un doble tiro de las pistolas gemelas de Sarah sale disparado antes de que ella termine su oración. Una ola de burbujas se revienta en una lluvia de neblina negra y mariposas retorcidas.<br><br>

                ''Lo que está adentro tampoco se ve tan bien'', dice Poppy.<br><br>

                ''No dejen que las toque''. Los ojos de Janna brillan con tonos lavanda. Una brisa se apodera del parque mientras ella comienza a elevarse del suelo. La corriente del aire concentra las hojas caídas a la vez que junta todas las burbujas. Janna las acorrala, junto con la oscuridad que contienen, y forma con ellas un gran grupo. Cada una de ellas se empuja contra la otra, como si les molestara ser contenidas.<br>
                La risa aguda se detiene de golpe y es reemplazada por un gruñido de molestia. El ruido retumba a nuestro alrededor y mis dientes rechinan. En el centro del grupo de burbujas tóxicas que recolectó Janna se forma un círculo estrecho. El círculo se abre como un portal, a través del cual brotan largos tentáculos provenientes de alguna dimensión sombría. Un desconcertante ojo de calamar se abre, y después otro. La masa gelatinosa y amorfa se despliega como una mezcla de un pulpo malvado y de una medusa endemoniada.</p>
            </div>
        </div>
    </div>
    <br>
    <br>

    @endsection

<center>
    <div>
        <a href="{{ url('inicio')}}"><img src="estilos/imagenes/logo1.png" style="width: 1000px; height: auto;"></a>
    </div>
</center>

</body>
</html>