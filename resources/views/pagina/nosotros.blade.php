<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Accesorios Para tu auto</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="estilos/css/inicio.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="icon" type="image/png" sizes="16x16" href="estilos/imagenes/logo accesorios-03.png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>

</head>
<body>
    <!--layout de nav-->
    @extends('layouts.nav')

    @section('nav')
    <!--termina layout de nav--> 

    <div class="container">
<br><br>
  <div id="accordion">
    <div class="card">
      <div class="card-header">
        <a class="card-link" data-toggle="collapse" href="#collapseOne">
        <h3 align="center"><strong>Objetivo</strong></h3>
        </a>
      </div>
      <div id="collapseOne" class="collapse show" data-parent="#accordion">
        <div class="card-body">
        <p>Somos un equipo profesional y honesto que busca ser un sitio web confiable y seguro para la comercialización de repuestos y accesorios automotrices generando una experiencia de compra única donde nuestra prioridad es la pasión por servir y satisfacer a nuestros clientes.</p>
        </div>
      </div>
    </div>
    <div class="card">
      <div class="card-header">
        <a class="collapsed card-link" data-toggle="collapse" href="#collapseTwo">
        <h3 align="center"><strong>Misión</strong></h3>
      </a>
      </div>
      <div id="collapseTwo" class="collapse" data-parent="#accordion">
        <div class="card-body">
        <p>Somos un equipo profesional y honesto que busca ser un sitio web confiable y seguro para la comercialización de repuestos y accesorios automotrices generando una experiencia de compra única donde nuestra prioridad es la pasión por servir y satisfacer a nuestros clientes.</p>
            </div>
      </div>
    </div>
    <div class="card">
      <div class="card-header">
        <a class="collapsed card-link" data-toggle="collapse" href="#collapseThree">
        <h3 align="center"><strong>Visión</strong></h3>
        </a>
      </div>
      <div id="collapseThree" class="collapse" data-parent="#accordion">
        <div class="card-body">
        <p>Contar con una extensa variedad de productos de excelente calidad con precios competitivos, diferenciarnos de otros por ofrecer los productos que brinden soluciones reales y adecuadas a las necesidades de todos y cada uno de nuestros clientes y así ser un sitio web de vanguardia con un alto índice de satisfacción y confiabilidad en México.</p>
            </div>
      </div>
    </div>
  </div>
</div>
<br>

<div class="row">
  <div class="col-sm-5"  style="right: -130px; border-color: black;">
    <div class="card">
      <div class="card-body" >
      <h3 align="center"><strong>Valores</strong></h3>
                <li>Compromiso</li>
                <li>Confianza</li>
                <li>Honestidad</li>
                <li>Integridad</li>
                <li>Lealtad</li>
                <li>Respeto</li>
                <li>Seguridad</li>
      </div>
    </div>
  </div>
  <div class="col-sm-5" style="right: -110px;">
    <div class="card">
      <div class="card-body">
      <h3><strong>Ventajas Competitivas</strong></h3>
                <li>Compromiso</li>
                <li>Confianza</li>
                <li>Honestidad</li>
                <li>Integridad</li>
                <li>Lealtad</li>
                <li>Respeto</li>
                <li>Seguridad</li>
      </div>
    </div>
  </div>
</div>

    @endsection

<center>
    <div>
        <a href="{{ url('inicio')}}"><img src="estilos/imagenes/logo1.png" style="width: 1000px; height: auto;"></a>
    </div>
</center>

</body>
</html>