<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Accesorios Para tu auto</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/inicio.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body style="background-image: url(imag/fondoclasico.png); background-repeat: no-repeat;">
    <!--layout de nav-->
    @extends('layouts.nav')

    @section('nav')
    <!--termina layout de nav-->
    <div class="container" style="width: 100%; height: auto;" align="justify">
        <div class="row" >
            <div class="col-xs-12 col-m-10 col-md-12" align="justify">
                <h3 align="center"><strong>Garantías</strong></h3>
                <p>Todos los productos de <img src="estilos/imagenes/logo-accesorios-03.png" style="width: 25px;"><strong> Accesorios para tu auto.com</strong>  cuentan con un <strong>periodo de garantía especificado al momento de la compra</strong> y es válida cuando el producto presenta fallas atribuibles a su fabricación, no es válida cuando el producto se utiliza para diferentes aplicaciones o mala instalación.<br><br>

                <h6><strong>Procedimiento de garantía.</strong></h6>
                &nbsp &nbsp1.- Ingresa a tu cuenta y ve a la sección garantías y da clik en <strong>reclamar garantía.</strong><br>
                &nbsp &nbsp2.- Cuéntanos del problema posteriormente ingresa los datos en el formulario correspondiente y adjunta mínimo 4 fotografías del artículo y da click en enviar.<br>
                &nbsp &nbsp3.- Se te notificara en un lapso de 24  horas el dictamen de tu devolución a través del medio que elegiste.<br>
                &nbsp &nbsp4.- Una vez el dictamen sea favorable se procederá con él envió del producto a reemplazar o el rembolso de tu compra, recuerda que tú decides.</p>
            </div>
        </div>
    </div>
    <br>
    <br>

    @endsection

<center>
    <div>
        <a href="{{ url('inicio')}}"><img src="estilos/imagenes/logo1.png" style="width: 1000px; height: auto;"></a>
    </div>
</center>

</body>
</html>