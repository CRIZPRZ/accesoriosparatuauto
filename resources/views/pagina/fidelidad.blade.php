<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Accesorios Para tu auto</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/inicio.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body>
    <!--layout de nav-->
    @extends('layouts.nav')

    @section('nav')
    <!--termina layout de nav-->
    <div class="container" style="width: 100%; height: auto;">
        <div class="row" >
            <div class="col-xs-12 col-m-10 col-md-12" align="justify">
                <h3 align="center">Programa de fidelidad</h3>
                <br>
                <p>''La pijamada''. Sarah me interrumpe, mirándome directo a los ojos. ''Esa noche. Ella debía venir. De último minuto, dijo que había algo de lo que tenía que encargarse. Algo con lo que no me dejaría ayudarla. Pensé que solo estaba siendo...''.<br><br>

                ''Ahri'', termino su frase, mientras ella asiente. ''¿No la has visto desde ese entonces?''.<br><br>

                Sarah niega con su cabeza, apretando el par de pistolas en su regazo. Un poco antes de que Sarah desvíe la mirada, puedo verlo: un destello de miedo. Siento cómo mi corazón late más fuerte.<br><br>

                Cien preguntas más inundan mi cerebro. Mi estómago se estruja.<br><br>

                ¿Qué cosa podría hacer entrar en pánico a Sarah de esta forma? ¿Dónde está Ahri? ¿Qué es eso que se avecina?<br><br>

                ¿Somos lo suficientemente fuertes como para enfrentarlo?<br><br>

                ¿Soy lo suficientemente fuerte?<br><br>

                Quiero preguntárselo, pero no puedo.</p>
            </div>
        </div>
    </div>
    <br>
    <br>

    @endsection

<center>
    <div>
        <a href="{{ url('inicio')}}"><img src="imagenes/logo1.png" style="width: 1000px; height: auto;"></a>
    </div>
</center>

</body>
</html>