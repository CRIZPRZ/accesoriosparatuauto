<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Accesorios Para tu auto</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/inicio.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body style="background-image: url(imag/fondoclasico.png); background-repeat: no-repeat;">
    <!--layout de nav-->
    @extends('layouts.nav')

    @section('nav')
    <!--termina layout de nav-->
    <div class="container" style="width: 100%; height: auto;" align="justify">
        <div class="row" >
            <div class="col-xs-12 col-m-10 col-md-10" align="justify">
                <h3 align="center"><strong>Pólitica de Devoluciones</strong></h3>
                <br>
                <h5><img src="estilos/imagenes/logo-accesorios-03.png" style="width: 25px;"><strong> Accesorios para tu auto.com</strong> pone a su disposición las siguientes políticas para devoluciones:</h5>
                <p><strong><h6>I.- Casos aplicables.</h6></strong>
                    <div style="padding-left: 25px;">
                        a) Si usted recibe el producto dañado, maltratado o roto.<br>
                        b) Si usted recibe el producto con defectos de fabricación.<br>
                        c) Si usted recibe otro producto diferente al que compro en nuestro sitio Web.<br>
                        d) Si usted recibió atención e información  personalizada en nuestro sitio web relacionada al producto y el asesor aseguro compatibilidad con su auto y no fue así.<br>
                        e) Si tuvo un error o confusión al comprar un producto y por ende hay compatibilidad con su auto.
                    </div>
                    <br>
                <h6><strong>II.- Procedimiento de devolución.</strong></h6>
                <div style="padding-left: 25px;">
                    1.- Ingresa a tu cuenta y ve a la sección de <strong>devolver artículo.</strong><br>
                    2.- Cuéntanos del problema posteriormente ingresa los datos en el formulario correspondiente y adjunta mínimo 4 fotografías del artículo y da click en enviar.<br>
                    3.- Se te notificara en un lapso de 48 horas el dictamen de tu devolución a través del medio que elegiste.<br>
                    4.- En cuanto tengamos de vuelta el producto considerando las condiciones de validación  liberaremos tu reembolso total de tu compra.
                    </div>
                    <br>
                <h6><strong>III.- Validación de devoluciones.</strong></h6>
                <div style="padding-left: 25px;">
                    A)  Si confundiste o elegiste un producto erróneo al realizar la compra y no interactuaste con nuestra asistencia personalizada, la devolución será válida siempre y cuando el producto sea el nuestro, lo devuelvas en su empaque original incluyendo accesorios, manuales y que no tenga señales de uso.<br>
                    B)  No será válidas las devoluciones por los casos de la sección I después de 48 hrs transcurridas así mismo se invalidaran si el producto presenta señales de uso.
                    IV.- Costos de devolución.<br>
                    a)  Si la devolución es atribuible a <img src="estilos/imagenes/logo-accesorios-03.png" style="width: 25px;"><strong> Accesorios para tu auto.com</strong> será cubierta por nosotros, solo tendrás que imprimir la guía que te hagamos llegar por correo, empacar el producto pegarla y esperar la recolección de tu paquete.<br>

                    b)  Si por algún motivo ajeno a <img src="estilos/imagenes/logo-accesorios-03.png" style="width: 25px;"><strong> Accesorios para tu auto.com</strong> quieres devolver el articulo a pesar que llego en buenas condiciones o tuviste una confusión al realizar tu compra y no utilizaste nuestro servicio personalizado de asesoría los gastos de devolución correrán por tu parte.
                </div>
                <br>

                <h6><strong>V.-Rembolsos.</strong></h6>
                <div style="padding-left: 25px;">
                    Considera que una vez que se culmina el proceso de devolución puedes ver tu saldo electrónico en tu cuenta si deseas retirarlo se verá reflejando en tu cuenta bancaria durante las 24 a 48 horas  lapso que banco  libera tu pago.</p>
                </div>
            </div>
        </div>
    </div>
    <br>
    <br>

    @endsection

<center>
    <div>
        <a href="{{ url('inicio')}}"><img src="estilos/imagenes/logo1.png" style="width: 1000px; height: auto;"></a>
    </div>
</center>

</body>
</html>