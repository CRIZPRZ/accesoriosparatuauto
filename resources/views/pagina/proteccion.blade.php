<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Accesorios Para tu auto</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/inicio.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

</head>
<body style="background-image: url(imag/fondoclasico.png); background-repeat: no-repeat;">
    <!--layout de nav-->
    @extends('layouts.nav')

    @section('nav')
    <!--termina layout de nav-->
    <div class="container" style="width: 100%; height: auto;">
        <div class="row" >
            <div class="col-xs-12 col-m-10 col-md-12" align="justify">
                <h3 align="center"><strong>Aviso de privacidad</strong></h3>
                <br>
                <p><img src="estilos/imagenes/logo-accesorios-03.png" style="width: 25px;"><strong> Accesorios para tu auto.com</strong>, con domicilio en Avenida de las Palmas SN, Santa María Atarasquillo, Lerma, Estado de México, C.P. 52044, es responsable del uso y protección de sus datos personales, y al respecto le informamos lo siguiente:<br><br>
                Los datos personales que recabamos de usted, los utilizaremos para las siguientes finalidades que son necesarias para la venta de productos que solicita:<br>
                1.-Ventas de nuestros productos.<br>
                2.-Logistica de envíos y entregas de sus pedidos.<br>
                3.-Campañas de publicidad.<br>
                4.-Pogramas de fidelidad.<br>
                5.- Actualización de la Base de Datos.<br>
                6.-Cualquier finalidad asociada con las anteriores.<br>
                7.-Verificación y compatibilidad de repuestos y accesorios automotrices específicos.<br><br>
                De manera adicional, utilizaremos su información personal para las siguientes finalidades que no son necesarias para la venta de nuestros productos, pero que nos permiten y facilitan brindarle una mejor atención:<br>
                1.- Campañas evaluación de satisfacción del cliente.<br>
                En caso de que no desee que sus datos personales sean tratados para estos fines adicionales, desde este momento usted nos puede comunicar lo anterior atreves de la siguiente dirección de correo electrónico <a href="mailto:contacto@accesoriosparatuauto.com">contacto@accesoriosparatuauto.com</a><br>
                La negativa para el uso de sus datos personales para estas finalidades no podrá ser un motivo para que le neguemos los servicios y productos que solicita o contrata con nosotros.<br>
                Para llevar a cabo las finalidades descritas en el presente aviso de privacidad, utilizaremos los siguientes datos personales:<br> 
                1. Nombre Completo.<br>
                2. Domicilio.<br>
                3. Registro Federal de Contribuyentes.<br>
                4. Teléfonos de Hogar, Oficina y móviles<br>
                5. Correo Electrónico.<br>
                6. Placa vehicular de cualquier entidad federativa.<br>
                7. NIV (Número de Identificación Vehicular).<br><br>
                Además de los datos personales mencionados anteriormente, para las finalidades informadas en el presente aviso de privacidad utilizaremos los siguientes datos personales considerados como sensibles, que requieren de especial protección:<br>
                1. Datos Personales de familiares y personas referidas.<br>
                2. Terceras personas (Nombre, Dirección, Teléfono, relación, etc.).<br><br>
                Le informamos que sus datos personales no son compartidos dentro y fuera del país con ninguna persona, empresa, organizaciones y autoridad distintas a nosotros.<br>
                Usted tiene derecho a conocer qué datos personales tenemos de usted, para qué los utilizamos y las condiciones del uso que les damos (Acceso). Asimismo, es su derecho solicitar la corrección de su información personal en caso de que esté desactualizada, sea inexacta o incompleta (Rectificación); que la eliminemos de nuestros registros o bases de datos cuando considere que la misma no está siendo utilizada conforme a los principios, deberes y obligaciones previstas en la normativa (Cancelación); así como oponerse al uso de sus datos personales para fines específicos (Oposición). Estos derechos se conocen como derechos ARCO.<br><br>

                Para el ejercicio de cualquiera de los derechos ARCO, usted deberá presentar la solicitud respectiva en <a href="mailto:contacto@accesoriosparatuauto.com">contacto@accesoriosparatuauto.com</a>. Para conocer el procedimiento y requisitos para el ejercicio de los derechos ARCO, usted podrá llamar al siguiente número telefónico 7282810192 ingresar a nuestro sitio de Internet <img src="estilos/imagenes/logo-accesorios-03.png" style="width: 25px;"> <strong>www.accesoriosparatuauto.com</strong> a la sección  privacidad.<br><br>
                Usted puede revocar el consentimiento que, en su caso, nos haya otorgado para el tratamiento de sus datos personales. Sin embargo, es importante que tenga en cuenta que no en todos los casos podremos atender su solicitud o concluir el uso de forma inmediata, ya que es posible que por alguna obligación legal requiramos seguir tratando sus datos personales. Asimismo, usted deberá considerar que para ciertos fines, la revocación de su consentimiento implicará que no le podamos seguir prestando el servicio que nos solicitó, o la conclusión de su relación con nosotros.<br><br>
                Para revocar su consentimiento deberá presentar su solicitud al email <a href="mailto:contacto@accesoriosparatuauto.com">contacto@accesoriosparatuauto.com</a><br>
                Su inscripción en el Registro Público para Evitar Publicidad, que está a cargo de la Procuraduría Federal del Consumidor, con la finalidad de que sus datos personales no sean utilizados para recibir publicidad o promociones de empresas de bienes o servicios. Para mayor información sobre este registro, usted puede consultar el portal de Internet de la PROFECO, o bien ponerse en contacto directo con ésta.<br><br>
                Le informamos que en nuestra página de Internet utilizamos cookies, web beacons y otras tecnologías a través de las cuales es posible monitorear su comportamiento como usuario de Internet, así como brindarle un mejor servicio y experiencia de usuario al navegar en nuestra página.<br><br>
                Los datos personales que obtenemos de estas tecnologías de rastreo son los siguientes: edad, género, intereses, residencia,  mismos que utilizamos para estrategias de  Marketing.<br>
                El presente aviso de privacidad puede sufrir modificaciones, cambios o actualizaciones derivadas de nuevos requerimientos legales; de nuestras propias necesidades por los productos o servicios que ofrecemos; de nuestras prácticas de privacidad; de cambios en nuestro modelo de negocio, o por otras causas.<br><br>                                                                                                 Nos comprometemos a mantenerlo informado sobre los cambios que pueda sufrir el presente aviso de privacidad, a través de nuestro boletín de nuestro sitio web.<br>
                Última actualización 27/09/2019</p>
            </div>
        </div>
    </div>
    <br><br>

    @endsection

<center>
    <div>
        <a href="{{ url('inicio')}}"><img src="estilos/imagenes/logo1.png" style="width: 1000px; height: auto;"></a>
    </div>
</center>

</body>
</html>