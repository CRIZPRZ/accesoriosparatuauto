<!DOCTYPE html>
<html lang="en">
<head>
  <title>Modelos</title>
     <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet" href="estilos/datatables/bootstrap.css">
<link rel="stylesheet" href="{{{ asset('estilos/datatables/bootstrap4.min.css')}}}">
<link rel="stylesheet" href="{{{ asset('estilos/datatables/css/jquery.dataTables.min.css')}}}">
<link rel="stylesheet" href="{{{ asset('estilos/datatables/css/estilo.css')}}}">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
<link rel="stylesheet" href="estilos/sweetalert2/dist/sweetalert2.min.css">
<link rel="stylesheet" href="{{{ asset('https://cdn.datatables.net/responsive/2.2.3/css/responsive.dataTables.min.css
')}}}">

        <!-- Bootstrap core CSS -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
        <!-- Material Design Bootstrap -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/css/mdb.min.css" rel="stylesheet">
                <link rel="stylesheet" href="{{asset('estilos/css/file.css')}}">
                <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
        <script src="estilos/sweetalert2/dist/sweetalert2.all.min.js"></script>

        <!-- Include a polyfill for ES6 Promises (optional) for IE11 -->
        <script src="https://cdn.jsdelivr.net/npm/promise-polyfill@8/dist/polyfill.js"></script>
        <script src="estilos/sweetalert2/dist/sweetalert2.min.js"></script>
        <link rel="icon" type="image/png" sizes="16x16" href="estilos/imagenes/logo accesorios-03.png">
        <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
    

</head>
<body >
@extends('layouts.app')

@section('content')
    
<div class="container">

              

   <!-- Modal para realizar alta de productos-->
        <div class="modal fade" id="modalLoginForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Alta Modelo</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ route('modelo.store') }}" method="POST">
      @csrf
      <div class="modal-body mx-3">

         

         <!-- obtener el id para proveedores-->  
         <div class="container">
        <div class="md-form mb-5">
          <i class="fas fa-cog prefix grey-text"></i>
          <input placeholder="Nombre de Modelo" name="nombre"  type="text" id="defaultForm-email" class="form-control validate" required="">
        </div>

        <div class=" mb-5">
          <label data-error="Error" data-success="Correcto" for="defaultForm-email">Nombre de Modelo</label>
          <select required=""  name="marca" class="form-control validate" style="width: 200px;">
            <option required="" hidden="" value="">Selecciona una Marca</option>
           @foreach($marca as $marcas)
            <option value="{{$marcas->id}}" required="">
                 {{$marcas->nombre_marca}} </option>
                 @endforeach
        </select>

         
        </div>
        </div>


  
      </div>
      <div class="modal-footer d-flex justify-content-center">
        
         <button class="btn btn-default">Registrar</button>
      </div>

    </div>
  </div>
</div>
</form>


                </div>
</div>

<!-- Modal para realizar edicion de productos-->
 <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
  aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header text-center">
        <h4 class="modal-title w-100 font-weight-bold">Actualiza Modelos</h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="{{ url('modificarmodelos') }}" method="POST">
      @csrf
      <div class="modal-body mx-3">

         
        <div class=" mb-5">
          <i class="fas fa-cog prefix grey-text"></i>
          
          <label data-error="Error" data-success="Correcto" for="defaultForm-email">id</label>
          <input name="id" readonly=""  type="text" id="id" class="form-control " required="">
        </div>
        <div class=" mb-5">
          <i class="fas fa-cog prefix grey-text"></i>
         
          <label data-error="Error" data-success="Correcto">Nombre de Marca</label>
           <input name="nombre"  type="text" id="nombre" class="form-control validate" required="">
        </div>
        <div >
        <label data-error="Error" data-success="Correcto" for="defaultForm-email">Nombre de Marca</label>
          <select id="marca" name="marca" class="form-control validate" style="width: 200px;">                                               
            <option  hidden="" value=" "> </option>
                  
                 </option>
   
           
           <option value="#">Selecciona una opcion</option>

           
               @foreach($marca as $marcas)
              <option value="{{$marcas->id}}">{{$marcas->nombre_marca}} </option>
                @endforeach
                   
           </select>
        </div>
          
          </div>
          <div class="modal-footer d-flex justify-content-center">
            
             <button class="btn btn-default">Actualizar</button>
          </div>

        </div>
  </div>
  </form>
</div>
<!--termina edicion -->

<div class="page-wrapper chiller-theme toggled">
  <a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
    <i class="fas fa-bars"></i>
  </a>
  <nav id="sidebar" class="sidebar-wrapper">
    <div class="sidebar-content">
      <div class="sidebar-brand">
        <a href="{{url('/')}}">Accesorios</a>
        <div id="close-sidebar">
          <i class="fas fa-times"></i>
        </div>
      </div>
      <div class="sidebar-header">
        <div class="user-pic">
           @if(Auth::user()->image_facebook==0)
     <img src="./avatar/{{ Auth::user()->foto }}" class="img-responsive img-rounded" alt="User picture"/>
       
     @endif

      @if(Auth::user()->image_facebook==1)
      <img class="img-responsive img-rounded" src="{{ Auth::user()->foto }}"
            alt="User picture">
       @endif
        </div>
        <div class="user-info">
          <span class="user-name">
          </span>
          <span class="user-role">{{ Auth::user()->name }}</span>
          <span class="user-status">
            <i class="fa fa-circle"></i>
            <span>Online</span>
          </span>
        </div>
      </div>
      <!-- sidebar-header  -->
      <div class="sidebar-search">
        <div>
          <!--<div class="input-group">
            <input type="text" class="form-control search-menu" placeholder="Search...">
            <div class="input-group-append">
              <span class="input-group-text">
                <i class="fa fa-search" aria-hidden="true"></i>
              </span>
            </div>
          </div>-->
        </div>
      </div>
      <!-- sidebar-search  -->
      <div class="sidebar-menu">
        <ul>
          <li class="header-menu">
            <span>General</span>
          </li>
          <li class="sidebar-dropdown">
            <a href="#">
              <i class="fas fa-bookmark"></i>
              <span>Catalogos</span>
             
            </a>
            <div class="sidebar-submenu">
              <ul>
                <li>
                  <a href="{{ url('productos') }}">Productos
                   <!--<span class="badge badge-pill badge-success">Pro</span>-->
                  </a>
                </li>
                @role('master|admin')
                <li>
                  <a href="{{ url('estado') }}">Estado</a>
                </li>
                
                <li>
                  <a href="{{ url('marca') }}">Marcas</a>
                </li>
                <li>
                  <a href="{{ url('modelo') }}">Modelo</a>
                </li>
                <li>
                  <a href="{{ url('categoria') }}">Categoria</a>
                </li>
                <li>
                  <a href="{{ url('subcat') }}">Subcategoria</a>
                </li>
                <li>
                  <a href="{{ url('proveedor') }}">Proveedores</a>
                </li>
                <li>
                  <a href="{{ url('usuarios') }}">Usuarios</a>
                </li>
                <li>
                  <a href="{{ url('promo') }}">Promociones</a>
                </li>
                <li>
                  <a href="{{ url('archivo') }}">Archivos PDF</a>
                </li>
                <li>
                  <a href="{{ url('evento') }}">Eventos</a>
                </li>
                <li>
                  <a href="{{ url('aviso') }}">Avisos</a>
                </li>
                @endrole
                <li>
                  <a href="{{ url('/') }}">inicio</a>
                </li>
              </ul>
            </div>
          </li>
          <!--<li class="sidebar-dropdown">
            <a href="#">
              <i class="fa fa-shopping-cart"></i>
              <span>E-commerce</span>
              <span class="badge badge-pill badge-danger">3</span>
            </a>
            <div class="sidebar-submenu">
              <ul>
                <li>
                  <a href="#">Products

                  </a>
                </li>
                <li>
                  <a href="#">Orders</a>
                </li>
                <li>
                  <a href="#">Credit cart</a>
                </li>
              </ul>
            </div>
          </li>
          <li class="sidebar-dropdown">
            <a href="#">
              <i class="far fa-gem"></i>
              <span>Components</span>
            </a>
            <div class="sidebar-submenu">
              <ul>
                <li>
                  <a href="#">General</a>
                </li>
                <li>
                  <a href="#">Panels</a>
                </li>
                <li>
                  <a href="#">Tables</a>
                </li>
                <li>
                  <a href="#">Icons</a>
                </li>
                <li>
                  <a href="#">Forms</a>
                </li>
              </ul>
            </div>
          </li>
          <li class="sidebar-dropdown">
            <a href="#">
              <i class="fa fa-chart-line"></i>
              <span>Charts</span>
            </a>
            <div class="sidebar-submenu">
              <ul>
                <li>
                  <a href="#">Pie chart</a>
                </li>
                <li>
                  <a href="#">Line chart</a>
                </li>
                <li>
                  <a href="#">Bar chart</a>
                </li>
                <li>
                  <a href="#">Histogram</a>
                </li>
              </ul>
            </div>
          </li>-->
          <li class="sidebar-dropdown">
            <a href="#">
              <i class="fa fa-globe"></i>
              <span>Tiendas Fisicas</span>
            </a>
            <div class="sidebar-submenu">
              <ul>
                <li>
                  <a href="https://goo.gl/maps/jJ57MaAQsn9wUVzn9" target="_blank">Google maps</a>
                </li>
                <!--<li>
                  <a href="#">Open street map</a>
                </li>-->
              </ul>
            </div>
          </li>
          <!--<li class="header-menu">
            <span>Extra</span>
          </li>
          <li>
            <a href="#">
              <i class="fa fa-book"></i>
              <span>Documentation</span>
              <span class="badge badge-pill badge-primary">Beta</span>
            </a>
          </li>
          <li>
            <a href="#">
              <i class="fa fa-calendar"></i>
              <span>Calendar</span>
            </a>
          </li>
          <li>
            <a href="#">
              <i class="fa fa-folder"></i>
              <span>Examples</span>
            </a>
          </li>-->
        </ul>
      </div>
      <!-- sidebar-menu  -->
    </div>
    <!-- sidebar-content  -->
    <div class="sidebar-footer">
     <!-- <a href="#">
        <i class="fa fa-bell"></i>
        <span class="badge badge-pill badge-warning notification">3</span>
      </a>
      <a href="#">
        <i class="fa fa-envelope"></i>
        <span class="badge badge-pill badge-success notification">7</span>
      </a>-->
      <a href="{{ url('perfiluser') }}">
        <i class="fas fa-users"></i>
        <span class="badge-sonar"></span>
      </a>
      <a href="{{ route('logout') }}"  onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();">
        <i class="fa fa-power-off"></i>
      </a>
       <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
      </form>
    </div>
  </nav>
  <!-- sidebar-wrapper  -->
  <main class="page-content">
    <div class="container-fluid">
      <div class="text-center">
         <div class="text-center">
          <h1 style="text-align: center;">Consulta de Modelos</h1>
            @can('create modelo')
              <a href="" class="btn btn-default btn-rounded mb-4" data-toggle="modal" data-target="#modalLoginForm">Alta de Modelo</a>
            @endcan
</div>

              <table id="mod"  class="table table-striped table-bordered display responsive nowrap" style="width:100%" margin="left">
                  <thead>
                      
                      @can('read modelo')
                        <th>Opciones</th>
                      @endcan
                      <th>ID</th>
                      <th>Nombre</th>
                      <th>Marca</th>
                     
                  </thead>
                          <tbody>
                          @foreach ($modelo as $modelos)

                           @can('read modelo')
                          <td>
                           <center>
                              <div style="display: inline-flex; ">

                                  {!! Form::open(['method' => 'DELETE', 'id' => 'formDeleteProduct', 'action' => ['ModeloController@destroy', $modelos->id]]) !!}

                                  {!! Form::button( '<i class="fa fa-trash "></i>', ['type' => 'submit', 'class' => 'btn btn-danger deleteProduct','id' =>  'btnDeleteProduct', 'data-id' => $modelos->id ] ) !!}
                                  
                                  {!! Form::close() !!} 
                                          &nbsp; &nbsp; &nbsp; &nbsp;
                                  <button data-toggle="modal" data-target="#edit" 
                                  class='fas fa-edit edit btn btn-info id' value="{{ $modelos->id }}" onclick='Mostrar(this);' style='font-size:14px;color:red; ' ></button>
                              </div>
                            </center>                          
                          </td>
                          @endcan
                          <td>{{$modelos->id}}
                          <td>{{$modelos->nombre_modelo}}</td>
                          <td>{{$modelos->marca}}</td>
                        </tr>
              @endforeach
              </tbody>
                </table>
    </div>

  </main>
  <!-- page-content" -->
</div>
<!-- page-wrapper -->

              
              
              <script src="{{{ asset('estilos/datatables/js/jquery-3.3.1.js')}}}"></script>
              <script src="{{{ asset('estilos/datatables/js/jquery.dataTables.min.js')}}}"></script>
              <script src="{{{ asset('estilos/datatables/js/dataTables.bootstrap4.min.js')}}}"></script>
              <script src="{{{ asset('estilos/datatables/js/dataTables.buttons.min.js')}}}"></script>
              <script src="{{{ asset('estilos/datatables/js/jszip.min.js')}}}"></script>
              <script src="{{{ asset('estilos/datatables/js/pdfmake.min.js')}}}"></script>
              <script src="{{{ asset('estilos/datatables/js/vfs_fonts.js')}}}"></script>
              <script src="{{{ asset('estilos/datatables/js/buttons.html5.min.js')}}}"></script>
              <script src="{{{ asset('estilos/datatables/js/buttons.print.min.js')}}}"></script>
              <script src="{{{ asset('estilos/datatables/js/buttons.print.min.js')}}}"></script>
              <script src="{{{ asset('estilos/datatables/js/buttons.colVis.min.js')}}}"></script>
              <script src="{{{ asset('estilos/datatables/js/pdf.js')}}}"></script>
              <script src="{{{ asset('estilos/js/validaciones.js')}}}"></script>
              <script src="{{{ asset('https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js')}}}"></script>
              
              
<script>          
 $('.deleteProduct').on('click', function(e) {
    var inputData = $('#formDeleteProduct').serialize();

    var id = $(this).attr('data-id');
    var parent = $(this).parent();

    Swal.fire({
  title: 'Estas seguro?',
  text: "¡No podrás revertir esto!",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Si, Eliminar!'
}).then((result) => {
  if (result.value) {

$.ajax({
        url: '{{ url('/eliminarmodelo') }}' + '/' + id,
        type: 'POST',
        data: inputData,
        success: function( msg ) {
            if ( msg.status === 'success' ) {
              Swal.fire(
              'Eliminado!',
              'Registro eliminado con exito.',
              'success'
              )
               
                parent.slideUp(300, function () {
                    parent.closest("tr").remove();
                });
                 setInterval(function() {
                    window.location.reload();
                 }, 5900);
            }
        },
        error: function( data ) {
            if ( data.status === 422 ) {
                toastr.error('Cannot delete the category');
            }
        }
    });
  }
})

    return false;
});
</script>            
             
<script> 
 

  function Mostrar(btn){
    console.log(btn.value);
    var route = '{{ url('/modificarmodelo') }}' + '/' + btn.value;

    $.get(route, function(res){
        $("#nombre").val(res.nombre_modelo);
        $("#id").val(res.id);
        $("#marca").val(res.id_marca);

    });
    
  } 
</script>

         

<!-- Bootstra p core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.8.10/js/mdb.min.js"></script>
              

<script src="estilos/sweetalert/sweetalert.min.js"></script>
@include('sweet::alert')

 @endsection 

</body>
</html>