
<!DOCTYPE html>
<html>
<head>
@extends('layouts.app')

@section('content')
@endsection
<meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="estilos/css/perfil.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script>


    <style type="text/css">
#chistes{
 position: relative;
}

}
.subir{
    padding: 5px 10px;
    background: #f55d3e;
    color:#fff;
    border:0px solid #fff;
}

.subir:hover{
    color:#fff;
    background: #f7cb15;
}

</style>
</head>
<body >
  @extends('layouts.nav')

    @section('nav')
	<div class="page-wrapper chiller-theme toggled">
  <a id="show-sidebar" class="btn btn-sm btn-dark" href="#">
    <i class="fas fa-bars"></i>
  </a>

  <br>
  <div class="container">
  <div class="card mb-3" style="max-width: 900px;">
  <div class="row no-gutters">
    <div class="col-md-4">
      <p class="card-text">(nombre del producto)</p>
      <img class="card-img-top" src="hover/dragon.jpg" alt="Card image" style="width:100%">

    </div>
    <div class="col-md-4">
      <div class="card-body">
        <p class="card-text">nombre del producto</p><br>
        <p>$precio por cantidad</p>


        <br>
        <p class="card-text"><small class="text-muted"></small></p>
      </div>
    </div>
    <div class="col-md-4">
      <div class="card-body">

        <button data-toggle="collapse" class="btn btn-danger" data-target="#demo">Detalles de la compra</button>

   <div id="demo" class="collapse">
     <p>Compraste un drago el día (fecha)</p>
     <textarea name="name" rows="8" cols="15" placeholder="descripcion detallada de o los articulos comprados"></textarea>
     <form role="form">

  </form>
   </div>
      </div>
    </div>
  </div>
</div>
</div>


      <!-- sidebar-header  -->
      <div class="sidebar-search">
        <div>
          <!--<div class="input-group">
            <input type="text" class="form-control search-menu" placeholder="Search...">
            <div class="input-group-append">
              <span class="input-group-text">
                <i class="fa fa-search" aria-hidden="true"></i>
              </span>
            </div>
          </div>-->
        </div>
      </div>
      <!-- sidebar-search  -->
      <div class="sidebar-menu">
        <ul>
          <li class="header-menu">
            <span>General</span>
          </li>

          @can('read productos')
          <li class="sidebar-dropdown">
            <a href="#">
              <i class="fas fa-bookmark"></i>
              <span>Catalogos</span>

            </a>
            <div class="sidebar-submenu">
              <ul>

                <li>
                  <a href="{{ url('productos') }}">Productos
                   <!--<span class="badge badge-pill badge-success">Pro</span>-->
                  </a>
                </li>

                @role('master|admin')
                <li>
                  <a href="{{ url('estado') }}">Estado</a>
                </li>

                <li>
                  <a href="{{ url('marca') }}">Marcas</a>
                </li>
                <li>
                  <a href="{{ url('modelo') }}">Modelo</a>
                </li>
                <li>
                  <a href="{{ url('categoria') }}">Categoria</a>
                </li>
                <li>
                  <a href="{{ url('subcat') }}">Subcategoria</a>
                </li>
                <li>
                  <a href="{{ url('proveedor') }}">Proveedores</a>
                </li>
                <li>
                  <a href="{{ url('usuarios') }}">Usuarios</a>
                </li>
                <li>
                  <a href="{{ url('usuarios') }}">Usuarios</a>
                </li>
                @endrole
                <li>
                  <a href="{{ url('/') }}">inicio</a>
                </li>
              </ul>
            </div>
          </li>
           @endcan
          <!--<li class="sidebar-dropdown">
            <a href="#">
              <i class="fa fa-shopping-cart"></i>
              <span>E-commerce</span>
              <span class="badge badge-pill badge-danger">3</span>
            </a>
            <div class="sidebar-submenu">
              <ul>
                <li>
                  <a href="#">Products

                  </a>
                </li>
                <li>
                  <a href="#">Orders</a>
                </li>
                <li>
                  <a href="#">Credit cart</a>
                </li>
              </ul>
            </div>
          </li>
          <li class="sidebar-dropdown">
            <a href="#">
              <i class="far fa-gem"></i>
              <span>Components</span>
            </a>
            <div class="sidebar-submenu">
              <ul>
                <li>
                  <a href="#">General</a>
                </li>
                <li>
                  <a href="#">Panels</a>
                </li>
                <li>
                  <a href="#">Tables</a>
                </li>
                <li>
                  <a href="#">Icons</a>
                </li>
                <li>
                  <a href="#">Forms</a>
                </li>
              </ul>
            </div>
          </li>
          <li class="sidebar-dropdown">
            <a href="#">
              <i class="fa fa-chart-line"></i>
              <span>Charts</span>
            </a>
            <div class="sidebar-submenu">
              <ul>
                <li>
                  <a href="#">Pie chart</a>
                </li>
                <li>
                  <a href="#">Line chart</a>
                </li>
                <li>
                  <a href="#">Bar chart</a>
                </li>
                <li>
                  <a href="#">Histogram</a>
                </li>
              </ul>
            </div>
          </li>-->
          <li class="sidebar-dropdown">
            <a href="#">
              <i class="fa fa-globe"></i>
              <span>Tiendas Fisicas</span>
            </a>
            <div class="sidebar-submenu">
              <ul>
                <li>
                  <a href="https://goo.gl/maps/jJ57MaAQsn9wUVzn9" target="_blank">Google maps</a>
                </li>
                <!--<li>
                  <a href="#">Open street map</a>
                </li>-->
              </ul>
            </div>
          </li>
          <!--<li class="header-menu">
            <span>Extra</span>
          </li>
          <li>
            <a href="#">
              <i class="fa fa-book"></i>
              <span>Documentation</span>
              <span class="badge badge-pill badge-primary">Beta</span>
            </a>
          </li>
          <li>
            <a href="#">
              <i class="fa fa-calendar"></i>
              <span>Calendar</span>
            </a>
          </li>
          <li>
            <a href="#">
              <i class="fa fa-folder"></i>
              <span>Examples</span>
            </a>
          </li>-->
        </ul>
      </div>
      <!-- sidebar-menu  -->
    </div>
    <!-- sidebar-content  -->
    <div class="sidebar-footer">
     <!-- <a href="#">
        <i class="fa fa-bell"></i>
        <span class="badge badge-pill badge-warning notification">3</span>
      </a>
      <a href="#">
        <i class="fa fa-envelope"></i>
        <span class="badge badge-pill badge-success notification">7</span>
      </a>-->
      <a href="{{ url('perfiluser') }}">
        <i class="fas fa-users"></i>
        <span class="badge-sonar"></span>
      </a>
      <a href="{{ route('logout') }}"  onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();">
        <i class="fa fa-power-off"></i>
      </a>
       <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
      </form>
    </div>
  </nav>

  <div class="cabecera">

     <section style=" position: relative; top: 180px;" >
      <center>
       <figure class="snip1566">
     @if(Auth::user()->image_facebook==0)
     <img style="border-style: solid;
      border-color: #000;"  src="./avatar/{{ $user->foto }}" height="150"  alt="sq-sample14" />

     @endif

      @if(Auth::user()->image_facebook==1)
      <img style="border-style: solid;
       border-color: #000;"  src="{{ Auth::user()->foto }}" height="150"  alt="sq-sample14" />
       @endif

         <figcaption>
          <i>
            <font class="letra" >Actualizar foto</font>
          </i>
         </figcaption>
        <a href="actualiza"></a>
       </figure>
      </center>
     </section>

   </div>
   <section style=" position: relative; top: 80px;">
<center> <h2>{{ Auth::user()->name }}</h2></center>
<center> <h3>{{ Auth::user()->id_tipo }}</h3></center>
<center> <h5>Accesorios para tu auto.com</h5></center>
</section>

  @endsection
  <center>
 <div>
        <a href="{{ url('inicio')}}"><img src="estilos/imagenes/logo1.png" style="width: 1000px; height: auto;"></a>
    </div>
</center>
<script src="{{{ asset('estilos/sweetalert/sweetalert.min.js')}}}"></script>
@include('sweet::alert')

</body>
</html>
