<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Accesorios Para tu auto</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <style>
        @media screen and (max-width: 992px) {
         .logo {
        width: 100% !important;
        height: auto !important;


          }
        }
    </style>
</head>
<body>
    <!--layout de nav-->
    @extends('layouts.nav')

    @section('nav')
    <!--termina layout de nav-->
<div class="row">
    <div class="col-xs-12 col-md-7">
        <!--Google map-->
        <div style="margin-top: 2%;" align="center">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d1079.3113499606457!2d-99.4676605710325!3d19.334717794578953!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e1!3m2!1ses!2smx!4v1569945065720!5m2!1ses!2smx" width="80%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>
    </div>
    <div class="col-xs-12 col-md-5">
    <h2>Contactanos</h2>
        <form action="{{route('contacto.store')}}" method="POST">
             @csrf
            <div class="form-group">
                <label style="color: black;">Nombre:</label>
                <input name="name" type="text" required="" style="width: 300px;">
            </div>
            <div class="form-group">
                <label style="color: black;">Tu correo electronico:</label>
                <input required="" name="correo" type="email" style="width: 187px;">
            </div>
            <div class="form-group">
                <label style="color: black;">Asunto:</label>
                <input name="subject" required="" type="text" style="width: 309px;">
            </div>
            <div class="form-group">
                <label style="color: black;">Mensaje:</label><br>
                <textarea name="msg" required="" type="text" style="width: 378px;" cols="40" rows="6"></textarea>
            </div>
            <div class="form-group" style="padding-left: 25%;">
                <button type="submit" id='btn-contact' class="btn peach-gradient" style="border-radius: 10px;">Enviar</button width>
            </div>
        </form>  


@endsection
<center>
    <div>
        <a href="{{ url('inicio')}}"><img class="logo" src="estilos/imagenes/logo1.png" style="width: 1000px; height: auto;"></a>
    </div>
</center>
<!--enviar el formulario con js-->


<script src="estilos/sweetalert/sweetalert.min.js"></script>
@include('sweet::alert')
</body>
</html>