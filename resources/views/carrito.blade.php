


<!doctype html>
<html class="no-js" lang="es-MX" data-site="MLM"
      data-country="MX"
      data-device="desktop">

<head prefix="">
  <title>Carrito</title>
  <meta charset="utf-8"/>

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <link rel="stylesheet" type="text/css" href="https://http2.mlstatic.com/resources/frontend/cart-core_ui/ui-dist/styles/cart-core_ui__large-08e223adb0.css" />

    <link rel="stylesheet" href="{{{ asset('estilos/css/estilos.css')}}}">
    <script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.4.1.min.js"></script>

        <link rel="stylesheet" href="{{{ asset('estilos/sweetalert2/dist/sweetalert2.min.css')}}}">
        @extends('layouts.MDB')

    @section('MDB')
    @endsection
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
        <script src="{{{ asset('estilos/sweetalert2/dist/sweetalert2.all.min.js')}}}"></script>
        <script src="{{{ asset('estilos/sweetalert2/dist/sweetalert2.min.js')}}}"></script>


<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

      <style>
        .fa-circle {
  color: #DF0000; }

.red-cart {
  color: #DF0000;
  background: white; }

  .carritos{
    position: absolute;
  right: -6%;
  top: 0%;
  
  font-size: 40%;
  padding: .6em;
  border-radius: 999px;
  line-height: .75em;
  color: white;
  color: #DF0000;
  text-align: center;
  min-width: 2em;
  font-weight: bold;
  background: white;
  border-style: solid;"
  }
      .loginface {
  background-color: #2e4a88;
  box-shadow: 0 3px 0 #1B3D82;
  text-shadow: 0 -1px -1px #1B3D82;

  display: inline;
  position: relative;
  font-family: Arial;
  font-size: 13px;
  font-weight: bold;
  text-align: center;
  text-decoration: none;
  color: #fff;
  border-radius: 15px;
  padding: 10px 50px;
}
.loginface:hover {
  background-color: #354F84;
  color: aqua;
  text-decoration: none;
}
.loginface:active {
  top: 2px;
    box-shadow: 0 2px 0 #1B3D82;
}

      @media screen and (max-width: 992px) {
 .logo {
width: 100% !important;
height: auto !important;


  }

  #div1{
    display: inline-block !important;
    align:center !important;
  }
}
@media screen and (min-width: 992px) {
 .buscado{
      display: none;
    }
  }
@media screen and (max-width: 992px) {
  .buscador2{
    display: none;

    }
    .buscado{
     
      width: 80% !important;
    }
  }
    </style>
</head>

<body>

  <center>
    <div>
        <a href="{{ url('/')}}"><img class="logo" src="{{{ asset('estilos/imagenes/logo1.png')}}}" style="width: 1000px; height: auto;"></a>
    </div>
</center>



    <!--Navbar-->

<nav class="navbar navbar-expand-lg navbar-dark danger-color">

  <!-- Navbar brand -->


  <!-- Collapse button -->
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#basicExampleNav"
    aria-controls="basicExampleNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
   <div class="buscado">
        <table>
          <tr>
            <td>
              {{ Form::open(['url' => 'busproduc', 'method' => 'GET', 'class' => 'form-inline pull-right']) }}

                <div class="md-form my-0">
                  <input required="" name="bus" class="form-control mr-sm-2" type="text" placeholder="Buscar" aria-label="Search">
                </div>
              {{ Form::close() }}
            </td>
            <td>
              @if(Auth::check()==true)
                <div align="right">
                  <a href="{{url('carrito')}}">
                    @foreach($carrito as $cart)
                    <span class="fa-stack fa-2x has-badge" data-count="{{$cart->car}}" style="font-size: 27px;">
                    @endforeach

                      <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
                    </span>
                  </a>
                </div>
              @else
                <div align="right">
                  <span class="fa-stack fa-2x has-badge"  style="font-size: 27px;">
                    <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
                  </span>
                </div>
              @endif
            </td>
          </tr>
        </table>
      </div>
  <!-- Collapsible content -->
  <div  class="collapse navbar-collapse" id="basicExampleNav">

    <!-- Links -->
    <ul  class="navbar-nav mr-auto">

    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

      <li class="nav-item" >
        <a class="nav-link"href="{{ url('/')}}">Inicio</a>
      </li>

       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <!-- Basic dropdown -->
<!--<li class="nav-item" data-toggle="dropdown">
  <a class="nav-link" href="#">Categorias</a>
</li>-->

<div class="dropdown-menu" style="background: #000; border-color: red; width: 100%; height: auto; opacity: 0.9;">

  @foreach($categoria as $categorias)

    <a href="{{URL::action('menucontroller@busquedacat',['id'=>$categorias -> id])}}"
      onmouseover="window.status='Guiarte, sitio de turismo y arte';return true" onmouseout="window.status='';return true">

      <i class="fas fa-angle-right" style="color: red; padding-left: 14px;" aria-hidden="true"><li style="color: #fff; text-align:justify; display: inline-block; width: 310px; padding-left: 4px; font-size: 12px; line-height: 1px; "> {{$categorias -> nombre_categoria}}</li></i>
    </a>
  @endforeach
</div>
<!-- Basic dropdown -->

    <!--<li class="nav-item">
        <a class="nav-link" href="{{ url('register') }}">Registrate</a>
      </li>-->
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
      <!--<li class="nav-item">
        <a class="nav-link"href="{{ url('cuponera')}}">Promociones</a>
      </li>-->
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

      <li class="nav-item">
        <a class="nav-link" href="{{ url('nosotros') }}">Nosotros</a>
      </li>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

      <li class="nav-item">
        <a class="nav-link" href="{{ url('contacto') }}">Contactanos</a>
      </li>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

<div class="buscador2">
      {{ Form::open(['url' => 'busproduc', 'method' => 'GET', 'class' => 'form-inline pull-right']) }}
       <div class="md-form my-0">
          <input required="" name="bus" class="form-control mr-sm-2" type="text" placeholder="Buscar" aria-label="Search">
      </div>
      {{ Form::close() }}
</div>

    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
@if(Auth::check())

@else
     <li class="nav-item dropdown">
        <div class="dropdown">
  <a class="nav-link dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
    data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Iniciar Sesión
  </a>

  <div class="dropdown-menu">
    <form class="px-4 py-3" method="POST" action="{{ route('login') }}">
      @csrf


       @error('email')
      <span class="invalid-feedback" role="alert">
          <strong style="color: red;">{{ $message }}</strong>
      </span>
      @enderror
       <!--errores en el Contraseña-->
       @error('password')
         <span class="invalid-feedback" role="alert">
            <strong style="color: red;">{{ $message }}</strong>
          </span>
       @enderror

      <div class="form-group">
        <label for="exampleDropdownFormEmail1" style="color: black;">Correo electronico</label>
        <input  type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus id="exampleDropdownFormEmail1" placeholder="email@ejemplo.com">
      </div>


      <div class="form-group">
        <label for="exampleDropdownFormPassword1" style="color: black;">Contraseña</label>
        <input required="" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" id="exampleDropdownFormPassword1" placeholder="Contraseña">
      </div>
<!--obtener la hora y fecha actual -->
<?php
date_default_timezone_set('America/Mexico_City');
setlocale(LC_TIME, 'es_MX.UTF-8');
$fecha_actual=strftime("%Y-%m-%d");
$hora_actual=strftime("%H:%M:%S");
?>

<input name="fecha" hidden="" type="text" class="form-control"  value="<?php echo $fecha_actual ;?>"  >
<input name="hora" hidden="" type="text" class="form-control" value="<?php echo $hora_actual ;?>">
<input name="ip"  hidden=""type="text" class="form-control" value="<?php echo $_SERVER['REMOTE_ADDR'] ; ?>">



       @error('password')
           <span class="invalid-feedback" role="alert">
              <strong style="color: red;">{{ $message }}</strong>
            </span>
       @enderror
         <div >
            <div >
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="remember"
                        old('remember') ? 'checked' : '' }}>

                    <label style="font-size: 12px;"  class="dropdown-item" for="remember">
                    Recuerdame
                    </label>
                </div>
            </div>
        </div>

        <div >
            <div >
            <center>
            <button type="submit" class="btn btn-primary">Iniciar Sesiòn</button>
            </center>
                <br>

            </div>
        </div>

    </form>
    <center>
        <br>
       <p>deseas iniciar sesion con facebook</p>

      <a href="{{ url('/auth/facebook')}}"  class="fab fa-facebook" style="font-size: 30px; color: #fff;"></a>

      </center>
<!-- Button trigger modal -->


     <a data-toggle="modal" data-target=".bd-example-modal-xl">¿Nuevo por aqui? <b>Registrarse</b></a>

    <a href="{{url('password/reset')}}">¿Olvidaste tu contraseña?</a>
  </div>
</div>

      </li>





<div class="modal fade bd-example-modal-xl" tabindex="-1" role="dialog" aria-labelledby="myExtraLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-xl" role="document">
    <div class="body">

    </div>
  </div>
</div>


@endif

<!--validar que existe inicio de sesion-->
      @if(Auth::check())
      <!--si existe mostrar nombre de ususuario logueado si no existe sesion no mostrar nada-->
 <li class="nav-item dropdown">
      <a  href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" style="position: relative; padding-left: 50px;">
         @if(Auth::user()->image_facebook==0)
     <img src="./avatar/{{ Auth::user()->foto }}" style="width: 32px; height: 32px; position: absolute; left: 10px; border-radius: 50%;"/>

     @endif

      @if(Auth::user()->image_facebook==1)
      <img src="{{ Auth::user()->foto }}" style="width: 32px; height: 32px; position: absolute; left: 10px; border-radius: 50%;">
       @endif


          {{ Auth::user()->name }} <span class="caret"></span>
      </a>

      <ul class="dropdown-menu" role="menu">

          <li><a href="{{ url('perfiluser')}}"><i class="fas fa-id-card" style="color: red;"></i>  Perfil</a></li>

          <li>
            <a href="{{ route('logout') }}"  onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();">
        <i class="fa fa-power-off" style="color: red;"> </i> Cerrar Sesión
      </a>
       <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          @csrf
      </form>
          </li>
      </ul>
  </li>
@endif

@if(Auth::check())

@if(Auth::user()->id_tipo!=4)
       <li class="nav-item">
        <a class="nav-link" href="{{ url('productos') }}">Sistema</a>
      </li>
 @endif
@endif


    </ul>
 <div class="buscador2"> 
  @if(Auth::check()==true)
      <div align="right">
        <a href="{{url('carrito')}}">
          @foreach($carrito as $cart)
          <span class="fa-stack fa-2x has-badge" style="font-size: 27px;">
            <span class="carritos" id="carts">{{$cart->car}}</span>
          @endforeach

            <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
          </span>
        </a>
      </div>
  @else
  <div align="right">


          <span class="fa-stack fa-2x has-badge"  style="font-size: 27px;">


            <i class="fa fa-shopping-cart fa-stack-1x fa-inverse"></i>
          </span>

      </div>
  @endif
</div>  
    <!-- Links -->


  </div>
  <!-- Collapsible content -->

</nav>
   
<br>
<br>

@if($carro == [])
<div data-region="items" class="cart__items-container" role="tabpanel" itemscope itemtype="http://schema.org/ItemList">


                <div class="cart__placeholder">
    <h3 class="cart__placeholder-title">Tu carrito está vacío</h3>
    <p class="cart__placeholder-subtitle">¿No sabes qué comprar? ¡Miles de productos te esperan!
 <a href="//www.mercadolibre.com.mx" title="Mercado Libre"></a></p>
</div>
<br><br>
@else


                <div id="vacio" style="display: none;" class="cart__placeholder">
    <h3 class="cart__placeholder-title">Tu carrito está vacío</h3>
    <p class="cart__placeholder-subtitle">¿No sabes qué comprar? ¡Miles de productos te esperan!
 <a href="//www.mercadolibre.com.mx" title="Mercado Libre"></a></p>
</div>



<div class="container">
<div id="eliminaritem" data-component="index" class="cart__list-items u-clearfix ">
    <div data-region="tabs" class="cart__tabs">
        <ul class="ui-tabs" role="tablist" data-tabs>
     @foreach($carrito as $cart)
    <li data-tab="cart" class="ui-tabs__item  ui-tabs__item--selected "><a  style="text-decoration: none; color: #000;" class="ui-tabs__item-link" href="/gz/cart" role="tab" id="canproductos">Carrito ({{$cart->car}})</a></li>
    @endforeach


</ul>

    </div>



@foreach($carro as $carritos)
 <form action="{{ route('carrito.store') }}" method="POST">
            @csrf



            <input hidden="" ype="text" readonly="" name="desc" value="{{ $carritos->descripcion}}">
            <input hidden="" type="text" readonly="" name="img" value="{{ $carritos->imagen}}">


            @if(Auth::check())


       <input hidden="" type="text" readonly="" name="user" value="{{Auth::user()->id}}" id="user">

@endif




    <div data-region="items" class="cart__items-container" role="tabpanel" itemscope itemtype="http://schema.org/ItemList">

       <div id="sect" data-component="item-cart" class="ui-panel" itemprop="itemListElement" itemscope itemtype="http://schema.org/Product">
    <div class="ui-panel__content">
        <article class="item  ">
            <div data-region="item-loading"></div>

                <figure class="item__image item__image--dimmer">
    <a itemprop="url" href="#">

         <img id="eimg" src="{{ asset('imag/'.$carritos->imagen) }}" width="48" height="48">
    </a>
</figure>

<div class="item__information">
    <div class="u-float-left item__description">
    <h2 class="item__title" itemprop="name">
    <a id="enom" class="item__title--link" href="#">{{$carritos->nombre_producto}}</a>
    <input hidden="" type="text" readonly="" name="nombre" value="{{ $carritos->nombre_producto}}">
</h2>



    </div>
   
    <a class="u-button-reset ui-quantity-selector__button edelete" data-id="{{$carritos->id}}">-</a>

        <input style="width: 10px;"  autocomplete="off" data-quantity="input" id="totalprod" value="{{$carritos->cantidad}}" class="u-button-reset ui-quantity-selector__input" name="quantity"  />


       <a id="eadd" data-id="{{$carritos->nombre_producto}}" value="+" class="u-button-reset ui-quantity-selector__button addtocart" name="push" />+</a>





        <div class="item__price">







        <span class="price-tag item__price-tag" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
    <meta itemprop="price" content="1,182.62"/>


    <span class="price-tag-symbol" itemprop="priceCurrency" >$</span>
    <span  >{{$carritos->precio_venta}}</span>
    <input id="venta" hidden="" value="{{$carritos->precio_venta}}" name="venta">

        <!--<span class="price-tag-decimal-separator">.</span>

        <span class="price-tag-cents">62</span>-->


</span>

        <span class="price-tag item__price-tag" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
    <meta itemprop="price" content="1,182.62"/>



    <!--<a href="{{URL::action('CarritoController@eliminar',['nombre_producto'=>$carritos->nombre_producto])}}" class='btn-sm btn-danger id' style='font-size:20px;color:#fff;'>Elimiar</a>-->
    <a data-id="{{$carritos->nombre_producto}}" style='font-size:15px;color:#fff;' class=" btn-sm btn-danger elimprduct">eliminar</a>
   


        <!--<span class="price-tag-decimal-separator">.</span>

        <span class="price-tag-cents">62</span>-->


</span>





</div>


</div>
</article>








    </div>
</div>




</form>

    </div>
    @endforeach
    <div data-region="summary" class="cart__summary">



    <div class="summary summary--static" aria-label="Resumen de tu carrito" data-summary="footer">
        <div class="summary__row-content">

                <div class="summary__row summary__row--shipping summary__row--subtotal">
                    @foreach($carrito as $cart)

                    <span class="summary__label">Productos  <span id="cant" class="bodge">({{$cart->car}})</span></span>
                    @endforeach
                    <span class="summary__price">
                        <span class="price-tag item__price-tag" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
    <meta itemprop="price" content="1,182.62"/>


    <span class="price-tag-symbol" itemprop="priceCurrency" >$</span>
    @foreach($total as $totales)
    <span class="price-tag-fraction" id="totproductos">{{$totales->total}}</span>

        <span class="price-tag-decimal-separator">.</span>

        <span class="price-tag-cents"></span>

    @endforeach
</span>

                    </span>
                 </div>



                <div class="summary__row summary__row--shipping summary__row--shipping-label">
                    <span class="summary__label ">

                            <div class="ui-dropdown">
                                <div class="cart-inline-loading__dropdown" data-region="dropdown-loading"></div>


                                        <a href="/navigation/addresses-hub?go=&context=cart" class="ui-dropdown__link" data-summary="dropdown-trigger">


                                   <!-- <span class="ui-dropdown__label">
                                        Envío a
                                        C.P.: 52044, Lerma
                                    </span>-->

                                        <span class="ui-dropdown__indicator">
                                            <svg viewBox="0 0 100 100" role="presentation" class="ui-icon ui-icon--chevron ">
    <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#ui-icon--chevron"></use>
</svg>

                                        </span>

                                </a>
                                <span data-region="dropdown-content"></span>
                            </div>

                    </span>




                        <!---<span class="summary__price summary__price--free">

                            <span class="summary__price--free-label">
                                Gratis
                            </span>
                        </span>-->


                </div>

        </div>

        <div class="summary__row summary__row--total">
            <span class="summary__label">Total</span>
            <span class="summary__price">
                <span class="price-tag item__price-tag" itemprop="offers" itemscope itemtype="http://schema.org/Offer">
    <meta itemprop="price" content="1,182.62"/>


    <span class="price-tag-symbol" itemprop="priceCurrency" >$</span>
    <span class="price-tag-fraction"  id="totproductos2">{{$totales->total}}</span>

        <span class="price-tag-decimal-separator">.</span>

        <!---<span class="price-tag-cents">62</span>-->


</span>

            </span>
        </div>
        <br><br>
        <a href="{{ url('metodospago') }}">
        <button class="btn btn-warning" style="border-radius: 5px;">Ir a pagar</button></a>

    </div>








 
</div>
@endif
<script>
$('.addtocart').on('click', function(e) {


  var nom = $(this).attr('data-id');

  $.get('cargavaloresprod/'+nom, function(data){
//se carga el valor que regresa la consulta al elemto span con id carts
     var nombre =  data[0].nombre_pro;
     var venta =  data[0].venta;
     var desc =  data[0].descri;
     var img =  data[0].img;
     var id_user = $("#user").val();
     var token = $("input[name=_token]").val();
     var route = "{{route('carrito.store')}}";
      $.ajax({
      url:route,
      headers:{'X-CSRF-TOKEN':token},
      type:'post',
      datatype:'json',
      data:{nombre_producto: nombre,
            precio_venta: venta,
            descripcion: desc,
            imagen: img,
            id_user: id_user},
      success:function(data)
      {
        if (data.success == 'true')
        {
         Swal.fire(
          '',
          'Producto agregado a tu carrito!',
          'success'
        )
        var id = $("#user").val();
          $.get('cargacarrito/'+id, function(data){
    //se carga el valor que regresa la consulta al elemto span con id carts
            var consulta= data[0].car
           
            
          if (consulta==0) {
            
            $('#eliminaritem').hide();
            $('#vacio').show(1000);

            //alert("functiona");
          }
          var id = $("#user").val();
         // alert(id);
         //traer la cantidad de de productos agregados al carrito
         $.get('cargacarrito/'+id, function(data){
    //se carga el valor que regresa la consulta al elemto span con id carts
            $("#carts").html(data[0].car);
            $("#cant").html(data[0].car);
           
            $("#canproductos").html("Carrito"+" "+"("+data[0].car+")");

            });
         //traer el precio total 
              $.get('totalcarrito/'+id, function(data){
    //se carga el valor que regresa la consulta al elemto span con id el id correspondiente
            $("#totproductos").html(data[0].total);
            $("#totproductos2").html(data[0].total);
            

            });
              var ideliminar = $("#user").val();
              $.get('obtenerid/'+ideliminar, function(data)
              {
                $("#totalprod").val(data[0].cantidad);
              });

        
          
                //window.location.reload();
                });
          $(".edelete").val(id);
        }

        else
        {
          Swal.fire(
          'Opps!',
          'Necesitas iniciar sesión!',
          'warning')
        }
      }
    });
  });
});
</script>

 <script>
   $('.elimprduct').on('click', function(e)
   {
    var token = $("input[name=_token]").val();
     var nom = $(this).attr('data-id');
     var parent = $(this).parent();


     

$.ajax({
        url: '{{ url('/eliminarc') }}' + '/' + nom,
        type: 'GET',
        data: token,
        
        /*beforeSend:function(){ 
             $(nom).html("procesando");
         },*/
        success:function(data)
      {
        if (data.success == 'true')
        {
          var id = $("#user").val();
          $.get('cargacarrito/'+id, function(data){
    //se carga el valor que regresa la consulta al elemto span con id carts
            var consulta= data[0].car
            
            
          if (consulta==0) {
            
            $('#eliminaritem').hide();
            $('#vacio').show(1000);

            //alert("functiona");
          }
          var id = $("#user").val();
         // alert(id);
         //traer la cantidad de de productos agregados al carrito
         $.get('cargacarrito/'+id, function(data){
    //se carga el valor que regresa la consulta al elemto span con id carts
            $("#carts").html(data[0].car);
            $("#cant").html(data[0].car);
            $("#canproductos").html("Carrito"+" "+"("+data[0].car+")");

            });
         //traer el precio total 
              $.get('totalcarrito/'+id, function(data){
    //se carga el valor que regresa la consulta al elemto span con id el id correspondiente
            $("#totproductos").html(data[0].total);
            $("#totproductos2").html(data[0].total);
            

            });

        
          parent.slideUp(300, function () {
                    //closest("#eee").remove();
                    $( "#eimg" ).remove();
                    $( "#enom" ).remove();
                    $( "#edelete" ).remove();
                    $( "#eadd" ).remove();
                    $( "#sect" ).remove();
                    $( "#ecant" ).remove();
                    //$( "#eprecio" ).remove();
                });
                //window.location.reload();
                });
        }

        else
        {
          
        }
      }
    });
   });

</script>


<script>
   $('.edelete').on('click', function(e)
   {
    var token = $("input[name=_token]").val();
     var idp = $(this).attr('data-id');
     
     
$.ajax({
        url: '{{ url('/eliminarcar') }}' + '/' + idp,
        type: 'GET',
        data: token,
        
        /*beforeSend:function(){ 
             $(nom).html("procesando");
         },*/
        success:function(data)
      {
        if (data.success == 'true')
        {
          var id = $("#user").val();
          $.get('cargacarrito/'+id, function(data){
    //se carga el valor que regresa la consulta al elemto span con id carts
            var consulta= data[0].car

            
                        
          if (consulta==0) {
            
            $('#eliminaritem').hide();
            $('#vacio').show(1000);

            //alert("functiona");
          }
          var id = $("#user").val();
         // alert(id);
         //traer la cantidad de de productos agregados al carrito
         $.get('cargacarrito/'+id, function(data){
    //se carga el valor que regresa la consulta al elemto span con id carts
            $("#carts").html(data[0].car);
            $("#cant").html(data[0].car);
            $('.edelete').attr("data-id",data[0].car);
            $("#canproductos").html("Carrito"+" "+"("+data[0].car+")");

            });
         //traer el precio total 
              $.get('totalcarrito/'+id, function(data){
    //se carga el valor que regresa la consulta al elemto span con id el id correspondiente
            $("#totproductos").html(data[0].total);
            $("#totproductos2").html(data[0].total);
            

            });
              var ideliminar = $("#user").val();
              $.get('obtenerid/'+ideliminar, function(data)
              {
                $("#totalprod").val(data[0].cantidad);
                $('.edelete').attr("data-id",data[0].id);
              });
              

                //window.location.reload();
                });
        }

        else
        {
          
        }
      }
    });
   });

</script>
<script src="{{{ asset('estilos/sweetalert/sweetalert.min.js')}}}"></script>
@include('sweet::alert')
</body>

</html>
