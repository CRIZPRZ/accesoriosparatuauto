<?php
use  App\Exports\ProductExportt;


Route::get('facturadescarga', 'CarritoController@facturapdf');



Route::get('factura',function(){
	return view('factura');
});


Route::resource('card','CardController');

//descargar exce
Route::get('excel',function(){
	return Excel::download(new ProductExportt, 'productos.xlsx');
});

Route::get('reset/{email}', function($email){
	return view('auth.passwords.reestablecer',compact('email'));
});

Route::POST('cambiar','UserController@updatepas')->name('cambiar');


Route::group(['middleware' => ['verified']], function() {
	//ruta para perfil de usuarios
	Route::get('cuenta','menucontroller@menu10');
	//ruta para carrito
	Route::resource('carrito','CarritoController');
	//ruta para actualizar foto de perfil
	Route::get('actualiza', 'UserController@actualiza');
	Route::post('actualizafoto', 'UserController@update_avatar');
	//ruta para perfil de usuarios
	Route::get('perfiluser','menucontroller@perfiluser');

});




Route::get('/product/{id}','ProductoController@consultaspr');
//ruta pagina principal

//rutas de login
Auth::routes(['verify' =>true]);
Route::post('registrar', 'UserController@registrar');
//fin

//ruta de home
Route::get('/home', 'HomeController@index')->name('home');
//fin

//middleware para roles de master admin y moderador
Route::group(['middleware' => ['role:master|admin|moderador']], function() {
	//crud productos
	Route::get('filtro2','ProductoController@filtro2');

   Route::resource('productos','ProductoController');
   Route::DELETE('/eliminarproduc/{id}','ProductoController@destroy');
   Route::get('/modificarproduc/{id}','ProductoController@edit');
   Route::POST('modificarproducs','ProductoController@update');
});


//middleware para roles de master y admin
Route::group(['middleware' => ['role:master|admin']], function() {

// crud para subir archivos pdf
Route::resource('archivo','ArchivoController');
Route::DELETE('/eliminarpdf/{id}','ArchivoController@destroy');

//crud para promiciones
Route::resource('promo','PromocionesController');
Route::get('/modificarpromos/{id}','PromocionesController@edit');
Route::POST('modificarpromo','PromocionesController@update')->name('modificarpromo');

//crud de eventos
Route::resource('evento','EventosController');
Route::get('/modificareventos/{id}','EventosController@edit');
Route::POST('modificareventos','EventosController@update')->name('modificareventos');
Route::DELETE('/eliminarevento/{id}','EventosController@destroy');

//crud de avisos
Route::resource('aviso','AvisosController');
Route::get('/modificaravisos/{id}','AvisosController@edit');
Route::POST('modificaravisos','AvisosController@update')->name('modificaravisos');
Route::DELETE('/eliminaraviso/{id}','AvisosController@destroy');

//crud de usuarios
Route::resource('usuarios','UserController');
Route::get('/eliminarmarca/{id}','MarcaController@destroy');
Route::get('/modificarmarca/{id}','MarcaController@edit');
Route::POST('modificarmarca','MarcaController@update')->name('modificarmarca');

	//crud estado
Route::resource('estado','EstadoController');
Route::DELETE('/eliminarestado/{id}','EstadoController@destroy');
//Route::get('/modificarestado/{id}','EstadoController@edit');
Route::POST('modificarestado','EstadoController@update')->name('modificarestado');
//fin

//crud Marcas
Route::resource('marca','MarcaController');
Route::get('/eliminarmarca/{id}','MarcaController@destroy');
Route::get('/modificarmarca/{id}','MarcaController@edit');
Route::POST('modificarmarca','MarcaController@update')->name('modificarmarca');
//fin

//crud Modelos
Route::resource('modelo','ModeloController');
Route::get('/eliminarmodelo/{id}','ModeloController@destroy');
Route::get('/modificarmodelo/{id}','ModeloController@edit');
Route::POST('modificarmodelos','ModeloController@update')->name('modificarmodelos');
//fin

//crud Categorias
Route::resource('categoria','CategoriaController');
Route::get('/eliminarcategoria/{id}','CategoriaController@destroy');
Route::get('/modificarcategoria/{id}','CategoriaController@edit');
Route::POST('modificarcategoria','CategoriaController@update')->name('modificarcategoria');
//fin

//crud subcategorias
Route::resource('subcat','SubcategoriaController');
Route::get('/eliminarsubcategoria/{id}','SubcategoriaController@destroy');
Route::get('/modificarsubcategoria/{id}','SubcategoriaController@edit');
Route::POST('modificarsubcategoria','SubcategoriaController@update')->name('modificarsubcategoria');
//fin

//crud proveedores
Route::resource('proveedor','ProveedorController');
Route::get('/eliminarproveedor/{id}','ProveedorController@destroy');
Route::get('/modificarproveedor/{id}','ProveedorController@edit');
Route::POST('modificarproveedor','ProveedorController@update')->name('modificarproveedor');
//fin
});


// rututas para select dinamico seleccionar marca y cargar todos los modelos de dicha marca
Route::get('modelos/{id}','ProductoController@getmodel');

Route::get('sub/{id}','ProductoController@getsub');

Route::get('model/{id}','menucontroller@getmod');
//fin

//rutas de web

Route::get('/','menucontroller@menu');

//pagina principal
Route::get('inicio','menucontroller@menu');
Route::get('nav','menucontroller@nav');
Route::get('consulta','menucontroller@filtrado');
Route::get('busproduc','menucontroller@bus');
Route::get('/buscategoria/{id}','menucontroller@busquedacat');
Route::post('/buscategorias','menucontroller@busquedasub');


Route::resource('menucontroller', 'menucontroller');

Route::get('nosotros','menucontroller@menu3');
Route::get('proteccion','menucontroller@menu4');
Route::get('formaspago','menucontroller@menu5');
Route::get('envios','menucontroller@menu6');
Route::get('devoluciones','menucontroller@menu7');
Route::get('garantias','menucontroller@menu8');
Route::get('ayuda','menucontroller@menu9');



Route::get('cuponera','menucontroller@menu11');
Route::get('descuento','menucontroller@menu12');
Route::get('experiencia','menucontroller@menu13');
Route::get('mayoreo','menucontroller@menu14');
Route::get('fidelidad','menucontroller@menu15');


//RUTAS AUTH WHIT FACEBOOK
Route::get('auth/{provider}', 'Auth\LoginController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\LoginController@handleProviderCallback');



//RUTAS ENVIO DE CORREO DE CONTACTO
Route::resource('contacto', 'ContactoController'); //Esta ruta la ponemos en la raiz para que nada mas ejecutar nuestra aplicación aparezca nuestro formulario




//ruta para detalle de producto
Route::post('producto/{codigo}', 'menucontroller@proddeta');
Route::post('relacion', 'menucontroller@relacion');

Route::get('prueba2',function(){
	return view('prueba');
});

//fin




//carrito
//
Route::resource('carrito','CarritoController');
Route::post('addtocart','CarritoController@store');

Route::get('/eliminarcar/{id}','CarritoController@destroy');
Route::get('/eliminarc/{nombre_producto}','CarritoController@eliminar');

//captcha
//
Route::get('/refresh_captcha', 'Auth\ResetPasswordController@refreshCaptcha')->name('refresh');



Route::post('tarjeta', 'CarritoController@tarjeta');

/***********Rutas para agregar al carrito en la vista de busqueda libre*/
Route::Get('cargacarrito/{id}', 'CarritoController@cargacarrito');
Route::Get('obtenerid/{id}', 'CarritoController@obtenerid');
Route::Get('totalcarrito/{id}', 'CarritoController@totalcarrito');
Route::Get('cargavalores/{cod}', 'CarritoController@cargavalores');
Route::Get('cargavaloresprod/{nom}', 'CarritoController@cargavaloresnom');
Route::Get('buscategoria/cargavaloressub/{id}', 'CarritoController@cargavaloressub');
Route::Get('cargasub/{id}', 'CarritoController@cargavaloressub');

/***********Rutas para agregar al carrito en la vista de categorias*/
Route::Get('buscategoria/cargavalores/{cod}', 'CarritoController@cargavalores');
Route::Get('buscat/cargavalores/{cod}', 'CarritoController@cargavalores');
Route::Get('buscategoria/cargacarrito/{id}', 'CarritoController@cargacarrito');
Route::get('/buscategoria/{id}','menucontroller@busquedacat');

/***********Rutas de prueba*/

Route::get('/search', 'menucontroller@search')->name('posts.search');