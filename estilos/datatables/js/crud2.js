$(document).ready(function() {
                $('#pdf').DataTable( {
                    
                    dom: 'Bfrtip',
                    
                       lengthMenu:[[5,10,20,50,100,500,-1],[5,10,20,50,100,500,'Todos']],

              processing: false,
              pageLength: 5,
              responsive: true,
             
                   buttons: [
                    

                    
                    ],
                      "language": {
                          "decimal": "",
                      "emptyTable": "No hay información",
                      "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
                      "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
                      "infoFiltered": "(Filtrado de _MAX_ total entradas)",
                      "infoPostFix": "",
                      "thousands": ",",
                      "lengthMenu": "Mostrar _MENU_ Entradas",
                      "loadingRecords": "Cargando...",
                      "processing": "Procesando...",
                      "search": "Buscar:",
                      "zeroRecords": "Sin resultados encontrados",
                      "paginate": {
                          "first": "Primero",
                          "last": "Ultimo",
                          "next": "Siguiente",
                          "previous": "Anterior"
                      },
                      "buttons":{
                          "copy" : 'Copiar',
                          "csv" : 'Exportar a CSV',
                          "print" : 'Imprimir',
                          copyTitle: 'Los datos fueron copiados',
                          copySuccess: {
                              _: '%d lineas copiadas',
                              1: '1 línea copiada'
                      },
                          "colvis" : 'Filtrar Columnas'
              
                      }
                          
                      }
                        } );

                    } );