
  $(document).ready(function() {
    $('.edit').on('click',function(){
    $tr = $(this).closest('tr');

    var data = $tr.children("td").map(function(){
          return $(this).text();
        }).get();

    console.log(data);

    $('#id').val(data[0]);
     $('#nombre').val(data[1]);
    });

});


   
  $('.deleteProduct').on('click', function(e) {
    var inputData = $('#formDeleteProduct').serialize();

    var id = $('#btnDeleteProduct').attr('data-id');
    var parent = $(this).parent();

    Swal.fire({
  title: 'Estas seguro?',
  text: "¡No podrás revertir esto!",
  type: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Si, Eliminar!'
}).then((result) => {
  if (result.value) {

$.ajax({
        url: '{{ url('/eliminarestado') }}' + '/' + id,
        type: 'POST',
        data: inputData,
        success: function( msg ) {
            if ( msg.status === 'success' ) {
              Swal.fire(
              'Eliminado!',
              'Registro eliminado con exito.',
              'success'
              )
               
                parent.slideUp(300, function () {
                    parent.closest("tr").remove();
                });
                 setInterval(function() {
                    window.location.reload();
                 }, 5900);
            }
        },
        error: function( data ) {
            if ( data.status === 422 ) {
                toastr.error('Cannot delete the category');
            }
        }
    });
  }
})

    return false;
});
 