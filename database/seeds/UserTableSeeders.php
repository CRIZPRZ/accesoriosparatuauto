<?php

use Illuminate\Database\Seeder;
use App\User;

class UserTableSeeders extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         // usuario con el rol master
        $master = User::create([
          'name' => 'cristian Lira',
          'email' => 'al221711754@gmail.com',
          'password' => bcrypt('Riderfnc'),
          'id_tipo' => ('1')
        ]);
        $master->assignRole('master');
    }
}
