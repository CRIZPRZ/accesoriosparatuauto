<?php

use Illuminate\Database\Seeder;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolesAnsPermissions extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
         // Reset cached roles and permissions
        app()['cache']->forget('spatie.permission.cache');

         // create permissions
        Permission::create(['name' => 'create usuarios']);
        Permission::create(['name' => 'read usuarios']);
        Permission::create(['name' => 'update usuarios']);
        Permission::create(['name' => 'delete usuarios']);


        Permission::create(['name' => 'create productos']);
        Permission::create(['name' => 'read productos']);
        Permission::create(['name' => 'update productos']);
        Permission::create(['name' => 'delete productos']);

        Permission::create(['name' => 'create estado']);
        Permission::create(['name' => 'read estado']);
        Permission::create(['name' => 'update estado']);
        Permission::create(['name' => 'delete estado']);

        Permission::create(['name' => 'create marca']);
        Permission::create(['name' => 'read marca']);
        Permission::create(['name' => 'update marca']);
        Permission::create(['name' => 'delete marca']);

        Permission::create(['name' => 'create modelo']);
        Permission::create(['name' => 'read modelo']);
        Permission::create(['name' => 'update modelo']);
        Permission::create(['name' => 'delete modelo']);

        Permission::create(['name' => 'create categorias']);
        Permission::create(['name' => 'read categorias']);
        Permission::create(['name' => 'update categorias']);
        Permission::create(['name' => 'delete categorias']);

        Permission::create(['name' => 'create subcategorias']);
        Permission::create(['name' => 'read subcategorias']);
        Permission::create(['name' => 'update subcategorias']);
        Permission::create(['name' => 'delete subcategorias']);

        Permission::create(['name' => 'create proveedores']);
        Permission::create(['name' => 'read proveedores']);
        Permission::create(['name' => 'update proveedores']);
        Permission::create(['name' => 'delete proveedores']);

        Permission::create(['name' => 'create role']);
        Permission::create(['name' => 'read roles']);
        Permission::create(['name' => 'update role']);
        Permission::create(['name' => 'delete role']);

        Permission::create(['name' => 'create permission']);
        Permission::create(['name' => 'read permissions']);
        Permission::create(['name' => 'update permission']);
        Permission::create(['name' => 'delete permission']);

        // create roles and assign created permissions
        $role = Role::create(['name' => 'master']);
        $role->givePermissionTo(Permission::all());

        $role = Role::create(['name' => 'admin']);
        $role->givePermissionTo('create usuarios');
        $role->givePermissionTo('read usuarios');
        $role->givePermissionTo('update usuarios');
        $role->givePermissionTo('delete usuarios');

        $role->givePermissionTo('create productos');
        $role->givePermissionTo('read productos');
        $role->givePermissionTo('update productos');
        $role->givePermissionTo('delete productos');

        $role->givePermissionTo('create estado');
        $role->givePermissionTo('read estado');
        $role->givePermissionTo('update estado');
        $role->givePermissionTo('delete estado');

        $role->givePermissionTo('create marca');
        $role->givePermissionTo('read marca');
        $role->givePermissionTo('update marca');
        $role->givePermissionTo('delete marca');

        $role->givePermissionTo('create modelo');
        $role->givePermissionTo('read modelo');
        $role->givePermissionTo('update modelo');
        $role->givePermissionTo('delete modelo');

        $role->givePermissionTo('create categorias');
        $role->givePermissionTo('read categorias');
        $role->givePermissionTo('update categorias');
        $role->givePermissionTo('delete categorias');

        $role->givePermissionTo('create subcategorias');
        $role->givePermissionTo('read subcategorias');
        $role->givePermissionTo('update subcategorias');
        $role->givePermissionTo('delete subcategorias');

        $role->givePermissionTo('create proveedores');
        $role->givePermissionTo('read proveedores');
        $role->givePermissionTo('update proveedores');
        $role->givePermissionTo('delete proveedores');



        $role = Role::create(['name' => 'moderador']);
        $role->givePermissionTo('create productos');
        $role->givePermissionTo('read productos');
        $role->givePermissionTo('update productos');
        $role->givePermissionTo('delete productos');

        $role = Role::create(['name' => 'usuario']);
        
    
    }
}
